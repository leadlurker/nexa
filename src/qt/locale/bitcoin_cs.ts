<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs">
<context>
    <name>AddressBookPage</name>
    <message>
        <location filename="../forms/addressbookpage.ui" line="+30"/>
        <source>Right-click to edit address or label</source>
        <translation>Pravým tlačítkem myši začneš upravovat označení adresy</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Create a new address</source>
        <translation>Vytvoř novou adresu</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;New</source>
        <translation>&amp;Nová</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Copy the currently selected address to the system clipboard</source>
        <translation>Zkopíruj aktuálně vybranou adresu do systémové schránky</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Copy</source>
        <translation>&amp;Kopíruj</translation>
    </message>
    <message>
        <location line="+67"/>
        <source>C&amp;lose</source>
        <translation>&amp;Zavřít</translation>
    </message>
    <message>
        <location filename="../addressbookpage.cpp" line="+90"/>
        <source>&amp;Copy Address</source>
        <translation>&amp;Kopíruj adresu</translation>
    </message>
    <message>
        <location filename="../forms/addressbookpage.ui" line="-53"/>
        <source>Delete the currently selected address from the list</source>
        <translation>Smaž zvolenou adresu ze seznamu</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Export the data in the current tab to a file</source>
        <translation>Exportuj data z tohoto panelu do souboru</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Export</source>
        <translation>&amp;Export</translation>
    </message>
    <message>
        <location line="-30"/>
        <source>&amp;Delete</source>
        <translation>S&amp;maž</translation>
    </message>
    <message>
        <location filename="../addressbookpage.cpp" line="-39"/>
        <source>Choose the address to send coins to</source>
        <translation>Zvol adresu, na kterou pošleš mince</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Choose the address to receive coins with</source>
        <translation>Zvol adres na příjem mincí</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>C&amp;hoose</source>
        <translation>&amp;Zvol</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Sending addresses</source>
        <translation>Odesílací adresy</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Receiving addresses</source>
        <translation>Přijímací adresy</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>These are your Nexa addresses for sending payments. Always check the amount and the receiving address before sending coins.</source>
        <translation>Tohle jsou tvé Nexaové adresy pro posílání plateb. Před odesláním mincí si vždy zkontroluj částku a cílovou adresu.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>These are your Nexa addresses for receiving payments. It is recommended to use a new receiving address for each transaction.</source>
        <translation>Tohle jsou tvé Nexaové adresy pro příjem plateb. Je doporučené používat pokaždé novou adresu pro každou transakci.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Copy &amp;Label</source>
        <translation>Kopíruj &amp;označení</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Edit</source>
        <translation>&amp;Uprav</translation>
    </message>
    <message>
        <location line="+173"/>
        <source>Export Address List</source>
        <translation>Exportuj seznam adres</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Comma separated file (*.csv)</source>
        <translation>CSV formát (*.csv)</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Exporting Failed</source>
        <translation>Exportování selhalo</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to save the address list to %1. Please try again.</source>
        <translation>Při ukládání seznamu adres do %1 se přihodila nějaká chyba. Zkus to prosím znovu.</translation>
    </message>
</context>
<context>
    <name>AddressTableModel</name>
    <message>
        <location filename="../addresstablemodel.cpp" line="+159"/>
        <source>Label</source>
        <translation>Označení</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Address</source>
        <translation>Adresa</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>(no label)</source>
        <translation>(bez označení)</translation>
    </message>
</context>
<context>
    <name>AskPassphraseDialog</name>
    <message>
        <location filename="../forms/askpassphrasedialog.ui" line="+26"/>
        <source>Passphrase Dialog</source>
        <translation>Změna hesla</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Enter passphrase</source>
        <translation>Zadej platné heslo</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>New passphrase</source>
        <translation>Zadej nové heslo</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Repeat new passphrase</source>
        <translation>Totéž heslo ještě jednou</translation>
    </message>
    <message>
        <location filename="../askpassphrasedialog.cpp" line="+47"/>
        <source>Encrypt wallet</source>
        <translation>Zašifruj peněženku</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>This operation needs your wallet passphrase to unlock the wallet.</source>
        <translation>K provedení této operace musíš zadat heslo k peněžence, aby se mohla odemknout.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unlock wallet</source>
        <translation>Odemkni peněženku</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>This operation needs your wallet passphrase to decrypt the wallet.</source>
        <translation>K provedení této operace musíš zadat heslo k peněžence, aby se mohla dešifrovat.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Decrypt wallet</source>
        <translation>Dešifruj peněženku</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Change passphrase</source>
        <translation>Změň heslo</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Confirm wallet encryption</source>
        <translation>Potvrď zašifrování peněženky</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Warning: If you encrypt your wallet and lose your passphrase, you will &lt;b&gt;LOSE ALL OF YOUR NEXA COINS&lt;/b&gt;!</source>
        <translation>Upozornění: Pokud si zašifruješ peněženku a ztratíš či zapomeneš heslo, &lt;b&gt;PŘIJDEŠ O VŠECHNY NEXA COINY&lt;/b&gt;!</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Are you sure you wish to encrypt your wallet?</source>
        <translation>Jsi si jistý, že chceš peněženku zašifrovat?</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>IMPORTANT: Any previous backups you have made of your wallet file should be replaced with the newly generated, encrypted wallet file. For security reasons, previous backups of the unencrypted wallet file will become useless as soon as you start using the new, encrypted wallet.</source>
        <translation>DŮLEŽITÉ: Všechny předchozí zálohy peněženky by měly být nahrazeny nově vygenerovanou, zašifrovanou peněženkou. Z bezpečnostních důvodů budou předchozí zálohy nešifrované peněženky nepoužitelné, jakmile začneš používat novou zašifrovanou peněženku.</translation>
    </message>
    <message>
        <location line="+104"/>
        <location line="+29"/>
        <source>Warning: The Caps Lock key is on!</source>
        <translation>Upozornění: Caps Lock je zapnutý!</translation>
    </message>
    <message>
        <location line="-140"/>
        <location line="+61"/>
        <source>Wallet encrypted</source>
        <translation>Peněženka je zašifrována</translation>
    </message>
    <message>
        <location line="-138"/>
        <source>Enter the new passphrase to the wallet.&lt;br/&gt;Please use a passphrase of &lt;b&gt;ten or more random characters&lt;/b&gt;, or &lt;b&gt;eight or more words&lt;/b&gt;.</source>
        <translation>Zadej nové heslo k peněžence.&lt;br/&gt;Použij &lt;b&gt;alespoň deset náhodných znaků&lt;/b&gt; nebo &lt;b&gt;alespoň osm slov&lt;/b&gt;.</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Enter the old passphrase and new passphrase to the wallet.</source>
        <translation>Zadej staré a nové heslo k peněžence.</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>%1 will close now to finish the encryption process. Remember that encrypting your wallet cannot fully protect your coins from being stolen by malware infecting your computer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+14"/>
        <location line="+8"/>
        <location line="+42"/>
        <location line="+6"/>
        <source>Wallet encryption failed</source>
        <translation>Zašifrování peněženky selhalo</translation>
    </message>
    <message>
        <location line="-55"/>
        <source>Wallet encryption failed due to an internal error. Your wallet was not encrypted.</source>
        <translation>Zašifrování peněženky selhalo kvůli vnitřní chybě. Tvá peněženka tedy nebyla zašifrována.</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+48"/>
        <source>The supplied passphrases do not match.</source>
        <translation>Zadaná hesla nejsou shodná.</translation>
    </message>
    <message>
        <location line="-36"/>
        <source>Wallet unlock failed</source>
        <translation>Nepodařilo se odemknout peněženku</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+11"/>
        <location line="+19"/>
        <source>The passphrase entered for the wallet decryption was incorrect.</source>
        <translation>Nezadal jsi správné heslo pro dešifrování peněženky.</translation>
    </message>
    <message>
        <location line="-20"/>
        <source>Wallet decryption failed</source>
        <translation>Nepodařilo se dešifrovat peněženku</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Wallet passphrase was successfully changed.</source>
        <translation>Heslo k peněžence bylo v pořádku změněno.</translation>
    </message>
</context>
<context>
    <name>BanTableModel</name>
    <message>
        <location filename="../bantablemodel.cpp" line="+86"/>
        <source>IP/Netmask</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>User Agent</source>
        <translation type="unfinished">Typ klienta</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Banned Until</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Ban Reason</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BitcoinGUI</name>
    <message>
        <location filename="../nexagui.cpp" line="+318"/>
        <source>Sign &amp;message...</source>
        <translation>Po&amp;depiš zprávu...</translation>
    </message>
    <message>
        <location line="+411"/>
        <source>Synchronizing with network...</source>
        <translation>Synchronizuji se se sítí...</translation>
    </message>
    <message>
        <location line="-500"/>
        <source>&amp;Overview</source>
        <translation>&amp;Přehled</translation>
    </message>
    <message>
        <location line="-136"/>
        <source>Node</source>
        <translation>Uzel</translation>
    </message>
    <message>
        <location line="+137"/>
        <source>Show general overview of wallet</source>
        <translation>Zobraz celkový přehled peněženky</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Request payments (generates QR codes and %1: URIs)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Transactions</source>
        <translation>&amp;Transakce</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Browse transaction history</source>
        <translation>Procházej historii transakcí</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>E&amp;xit</source>
        <translation>&amp;Konec</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Quit application</source>
        <translation>Ukonči aplikaci</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;About %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show information about %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>About &amp;Qt</source>
        <translation>O &amp;Qt</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show information about Qt</source>
        <translation>Zobraz informace o Qt</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Options...</source>
        <translation>&amp;Možnosti...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Modify configuration options for %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Unlimited...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Modify Bitcoin Unlimited Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Encrypt Wallet...</source>
        <translation>Zaši&amp;fruj peněženku...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Backup Wallet...</source>
        <translation>&amp;Zazálohuj peněženku...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Change Passphrase...</source>
        <translation>Změň &amp;heslo...</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Sending addresses...</source>
        <translation>Od&amp;esílací adresy...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Receiving addresses...</source>
        <translation>Př&amp;ijímací adresy...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Open &amp;URI...</source>
        <translation>Načíst &amp;URI...</translation>
    </message>
    <message>
        <location line="+397"/>
        <source>Importing blocks from disk...</source>
        <translation>Importuji bloky z disku...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Reindexing blocks on disk...</source>
        <translation>Vytvářím nový index bloků na disku...</translation>
    </message>
    <message>
        <location line="-498"/>
        <source>Send coins to a Nexa address</source>
        <translation>Pošli mince na Nexaovou adresu</translation>
    </message>
    <message>
        <location line="+77"/>
        <source>Backup wallet to another location</source>
        <translation>Zazálohuj peněženku na jiné místo</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Change the passphrase used for wallet encryption</source>
        <translation>Změň heslo k šifrování peněženky</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Debug window</source>
        <translation>&amp;Ladicí okno</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Open debugging and diagnostic console</source>
        <translation>Otevři ladicí a diagnostickou konzoli</translation>
    </message>
    <message>
        <location line="-4"/>
        <source>&amp;Verify message...</source>
        <translation>&amp;Ověř zprávu...</translation>
    </message>
    <message>
        <location line="+495"/>
        <source>Nexa</source>
        <translation>Nexa</translation>
    </message>
    <message>
        <location line="-726"/>
        <source>Wallet</source>
        <translation>Peněženka</translation>
    </message>
    <message>
        <location line="+147"/>
        <source>&amp;Send</source>
        <translation>P&amp;ošli</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>&amp;Receive</source>
        <translation>Při&amp;jmi</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>&amp;Show / Hide</source>
        <translation>&amp;Zobraz/Skryj</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show or hide the main Window</source>
        <translation>Zobraz nebo skryj hlavní okno</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Encrypt the private keys that belong to your wallet</source>
        <translation>Zašifruj soukromé klíče ve své peněžence</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Sign messages with your Nexa addresses to prove you own them</source>
        <translation>Podepiš zprávy svými Nexaovými adresami, čímž prokážeš, že jsi jejich vlastníkem</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Verify messages to ensure they were signed with specified Nexa addresses</source>
        <translation>Ověř zprávy, aby ses ujistil, že byly podepsány danými Nexaovými adresami</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>&amp;File</source>
        <translation>&amp;Soubor</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>&amp;Settings</source>
        <translation>&amp;Nastavení</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Help</source>
        <translation>Nápověd&amp;a</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Tabs toolbar</source>
        <translation>Panel s listy</translation>
    </message>
    <message>
        <location line="-93"/>
        <source>Show the list of used sending addresses and labels</source>
        <translation>Ukaž seznam použitých odesílacích adres a jejich označení</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show the list of used receiving addresses and labels</source>
        <translation>Ukaž seznam použitých přijímacích adres a jejich označení</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Command-line options</source>
        <translation>Ar&amp;gumenty z příkazové řádky</translation>
    </message>
    <message numerus="yes">
        <location line="+366"/>
        <source>%n active connection(s) to Nexa network</source>
        <translation>
            <numerusform>%n aktivní spojení do Nexa sítě</numerusform>
            <numerusform>%n aktivní spojení do Nexa sítě</numerusform>
            <numerusform>%n aktivních spojení do Nexa sítě</numerusform>
        </translation>
    </message>
    <message>
        <location line="+34"/>
        <source>No block source available...</source>
        <translation>Není dostupný žádný zdroj bloků...</translation>
    </message>
    <message numerus="yes">
        <location line="+9"/>
        <source>Processed %n block(s) of transaction history.</source>
        <translation>
            <numerusform>Zpracován %n blok transakční historie.</numerusform>
            <numerusform>Zpracovány %n bloky transakční historie.</numerusform>
            <numerusform>Zpracováno %n bloků transakční historie.</numerusform>
        </translation>
    </message>
    <message>
        <location line="+25"/>
        <source>%1 behind</source>
        <translation>Stahuji ještě bloky transakcí za poslední %1</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Last received block was generated %1 ago.</source>
        <translation>Poslední stažený blok byl vygenerován %1 zpátky.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Transactions after this will not yet be visible.</source>
        <translation>Následné transakce ještě nebudou vidět.</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Error</source>
        <translation>Chyba</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Warning</source>
        <translation>Upozornění</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Information</source>
        <translation>Informace</translation>
    </message>
    <message>
        <location line="-85"/>
        <source>Up to date</source>
        <translation>Aktuální</translation>
    </message>
    <message>
        <location line="-417"/>
        <source>Open a %1: URI or payment request</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Show the %1 help message to get a list with possible Nexa command-line options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+185"/>
        <source>%1 client</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+251"/>
        <source>Catching up...</source>
        <translation>Stahuji...</translation>
    </message>
    <message>
        <location line="+163"/>
        <source>Date: %1
</source>
        <translation>Datum: %1
</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Amount: %1
</source>
        <translation>Částka: %1
</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Type: %1
</source>
        <translation>Typ: %1
</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Label: %1
</source>
        <translation>Označení: %1
</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Address: %1
</source>
        <translation>Adresa: %1
</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sent transaction</source>
        <translation>Odeslané transakce</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Incoming transaction</source>
        <translation>Příchozí transakce</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>HD key generation is &lt;b&gt;enabled&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>HD key generation is &lt;b&gt;disabled&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Wallet is &lt;b&gt;encrypted&lt;/b&gt; and currently &lt;b&gt;unlocked&lt;/b&gt;</source>
        <translation>Peněženka je &lt;b&gt;zašifrovaná&lt;/b&gt; a momentálně &lt;b&gt;odemčená&lt;/b&gt;</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Wallet is &lt;b&gt;encrypted&lt;/b&gt; and currently &lt;b&gt;locked&lt;/b&gt;</source>
        <translation>Peněženka je &lt;b&gt;zašifrovaná&lt;/b&gt; a momentálně &lt;b&gt;zamčená&lt;/b&gt;</translation>
    </message>
</context>
<context>
    <name>CoinControlDialog</name>
    <message>
        <location filename="../forms/coincontroldialog.ui" line="+14"/>
        <source>Coin Selection</source>
        <translation>Výběr mincí</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Quantity:</source>
        <translation>Počet:</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Bytes:</source>
        <translation>Bajtů:</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Amount:</source>
        <translation>Částka:</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Priority:</source>
        <translation>Priorita:</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Fee:</source>
        <translation>Poplatek:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Dust:</source>
        <translation>Prach:</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>After Fee:</source>
        <translation>Čistá částka:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Change:</source>
        <translation>Drobné:</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>(un)select max inputs</source>
        <translation>(od)označit všechny</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Tree mode</source>
        <translation>Zobrazit jako strom</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>List mode</source>
        <translation>Vypsat jako seznam</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Amount</source>
        <translation>Částka</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Received with label</source>
        <translation>Příjem na označení</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Received with address</source>
        <translation>Příjem na adrese</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Confirmations</source>
        <translation>Potvrzení</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirmed</source>
        <translation>Potvrzeno</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Priority</source>
        <translation>Priorita</translation>
    </message>
    <message>
        <location filename="../coincontroldialog.cpp" line="+45"/>
        <source>Copy address</source>
        <translation>Kopíruj adresu</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy label</source>
        <translation>Kopíruj její označení</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+29"/>
        <source>Copy amount</source>
        <translation>Kopíruj částku</translation>
    </message>
    <message>
        <location line="-27"/>
        <source>Copy transaction ID</source>
        <translation>Kopíruj ID transakce</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Lock unspent</source>
        <translation>Zamkni neutracené</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unlock unspent</source>
        <translation>Odemkni k utracení</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Copy quantity</source>
        <translation>Kopíruj počet</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Copy fee</source>
        <translation>Kopíruj poplatek</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy after fee</source>
        <translation>Kopíruj čistou částku</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy bytes</source>
        <translation>Kopíruj bajty</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy priority</source>
        <translation>Kopíruj prioritu</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy dust</source>
        <translation>Kopíruj prach</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy change</source>
        <translation>Kopíruj drobné</translation>
    </message>
    <message>
        <location line="+352"/>
        <source>highest</source>
        <translation>nejvyšší</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>higher</source>
        <translation>vyšší</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>high</source>
        <translation>vysoká</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>medium-high</source>
        <translation>vyšší střední</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>medium</source>
        <translation>střední</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>low-medium</source>
        <translation>nižší střední</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>low</source>
        <translation>nízká</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>lower</source>
        <translation>nižší</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>lowest</source>
        <translation>nejnižší</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>(%1 locked)</source>
        <translation>(%1 zamčeno)</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>none</source>
        <translation>žádná</translation>
    </message>
    <message>
        <location line="+167"/>
        <source>This label turns red if the transaction size is greater than 1000 bytes.</source>
        <translation>Popisek zčervená, pokud je velikost transakce větší než 1000 bajtů.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>This label turns red if the priority is smaller than &quot;medium&quot;.</source>
        <translation>Popisek zčervená, pokud je priorita menší než „střední“.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>This label turns red if any recipient receives an amount smaller than %1.</source>
        <translation>Popisek zčervená, pokud má některý příjemce obdržet částku menší než %1.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Can vary +/- %1 satoshi(s) per input.</source>
        <translation>Může se lišit o +/– %1 satoshi na každý vstup.</translation>
    </message>
    <message>
        <location line="-41"/>
        <source>yes</source>
        <translation>ano</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>no</source>
        <translation>ne</translation>
    </message>
    <message>
        <location line="+17"/>
        <location line="+8"/>
        <source>This means a fee of at least %1 per kB is required.</source>
        <translation>To znamená, že je vyžadován poplatek alespoň %1 za kB.</translation>
    </message>
    <message>
        <location line="-5"/>
        <source>Can vary +/- 1 byte per input.</source>
        <translation>Může se lišit o +/– 1 bajt na každý vstup.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Transactions with higher priority are more likely to get included into a block.</source>
        <translation>Transakce s vyšší prioritou mají větší šanci na zařazení do bloku.</translation>
    </message>
    <message>
        <location line="+66"/>
        <location line="+64"/>
        <source>(no label)</source>
        <translation>(bez označení)</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>change from %1 (%2)</source>
        <translation>drobné z %1 (%2)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>(change)</source>
        <translation>(drobné)</translation>
    </message>
</context>
<context>
    <name>EditAddressDialog</name>
    <message>
        <location filename="../forms/editaddressdialog.ui" line="+14"/>
        <source>Edit Address</source>
        <translation>Uprav adresu</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Label</source>
        <translation>&amp;Označení</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>The label associated with this address list entry</source>
        <translation>Označení spojené s tímto záznamem v seznamu adres</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>The address associated with this address list entry. This can only be modified for sending addresses.</source>
        <translation>Adresa spojená s tímto záznamem v seznamu adres. Lze upravovat jen pro odesílací adresy.</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>&amp;Address</source>
        <translation>&amp;Adresa</translation>
    </message>
    <message>
        <location filename="../editaddressdialog.cpp" line="+25"/>
        <source>New receiving address</source>
        <translation>Nová přijímací adresa</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>New sending address</source>
        <translation>Nová odesílací adresa</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Edit receiving address</source>
        <translation>Uprav přijímací adresu</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Edit sending address</source>
        <translation>Uprav odesílací adresu</translation>
    </message>
    <message>
        <location line="+66"/>
        <source>The entered address &quot;%1&quot; is already in the address book.</source>
        <translation>Zadaná adresa &quot;%1&quot; už v adresáři je.</translation>
    </message>
    <message>
        <location line="-5"/>
        <source>The entered address &quot;%1&quot; is not a valid Nexa address.</source>
        <translation>Zadaná adresa &quot;%1&quot; není platná Nexa adresa.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Could not unlock wallet.</source>
        <translation>Nemohu odemknout peněženku.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>New key generation failed.</source>
        <translation>Nepodařilo se mi vygenerovat nový klíč.</translation>
    </message>
</context>
<context>
    <name>FreespaceChecker</name>
    <message>
        <location filename="../intro.cpp" line="+75"/>
        <source>A new data directory will be created.</source>
        <translation>Vytvoří se nový adresář pro data.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>name</source>
        <translation>název</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Directory already exists. Add %1 if you intend to create a new directory here.</source>
        <translation>Adresář už existuje. Přidej %1, pokud tady chceš vytvořit nový adresář.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Path already exists, and is not a directory.</source>
        <translation>Taková cesta už existuje, ale není adresářem.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Cannot create data directory here.</source>
        <translation>Tady nemůžu vytvořit adresář pro data.</translation>
    </message>
</context>
<context>
    <name>HelpMessageDialog</name>
    <message>
        <location filename="../utilitydialog.cpp" line="+39"/>
        <source>version</source>
        <translation>verze</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+2"/>
        <source>(%1-bit)</source>
        <translation>(%1-bit)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>About %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Command-line options</source>
        <translation>Argumenty z příkazové řádky</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Usage:</source>
        <translation>Užití:</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>command-line options</source>
        <translation>možnosti příkazové řádky</translation>
    </message>
</context>
<context>
    <name>Intro</name>
    <message>
        <location filename="../forms/intro.ui" line="+14"/>
        <source>Welcome</source>
        <translation>Vítej</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Welcome to %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+26"/>
        <source>As this is the first time the program is launched, you can choose where %1 will store its data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>%1 will download and store a copy of the Nexa block chain. At least %2GB of data will be stored in this directory, and it will grow over time. The wallet will also be stored in this directory.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Use the default data directory</source>
        <translation>Použij výchozí adresář pro data</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Use a custom data directory:</source>
        <translation>Použij tento adresář pro data:</translation>
    </message>
    <message>
        <location filename="../intro.cpp" line="+81"/>
        <source>Error: Specified data directory &quot;%1&quot; cannot be created.</source>
        <translation>Chyba: Nejde vytvořit požadovaný adresář pro data „%1“.</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Error</source>
        <translation>Chyba</translation>
    </message>
    <message numerus="yes">
        <location line="+11"/>
        <source>%n GB of free space available</source>
        <translation>
            <numerusform>%n GB volného místa</numerusform>
            <numerusform>%n GB volného místa</numerusform>
            <numerusform>%n GB volného místa</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>(of %n GB needed)</source>
        <translation>
            <numerusform>(z potřebného %n GB)</numerusform>
            <numerusform>(z potřebných %n GB)</numerusform>
            <numerusform>(z potřebných %n GB)</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>LessThanValidator</name>
    <message>
        <location filename="../unlimiteddialog.cpp" line="+486"/>
        <source>Upstream traffic shaping parameters can&apos;t be blank</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ModalOverlay</name>
    <message>
        <location filename="../forms/modaloverlay.ui" line="+14"/>
        <source>Form</source>
        <translation type="unfinished">Formulář</translation>
    </message>
    <message>
        <location line="+119"/>
        <source>The displayed information may be out of date. Your wallet automatically synchronizes with the Nexa network after a connection is established, but this process has not completed yet. This means that recent transactions will not be visible, and the balance will not be up-to-date until this process has completed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Spending coins may not be possible during that phase!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Amount of blocks left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+26"/>
        <source>unknown...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-13"/>
        <source>Last block time</source>
        <translation type="unfinished">Čas posledního bloku</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Progress</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>~</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Progress increase per Hour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+20"/>
        <source>calculating...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Estimated time left until synced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Hide</source>
        <translation type="unfinished">Skryj</translation>
    </message>
    <message>
        <location filename="../modaloverlay.cpp" line="+140"/>
        <source>Unknown. Reindexing (%1)...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unknown. Reindexing...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unknown. Syncing Headers (%1)...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unknown. Syncing Headers...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OpenURIDialog</name>
    <message>
        <location filename="../forms/openuridialog.ui" line="+14"/>
        <source>Open URI</source>
        <translation>Načíst URI</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Open payment request from URI or file</source>
        <translation>Načíst platební požadavek z URI nebo ze souboru</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>URI:</source>
        <translation>URI:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Select payment request file</source>
        <translation>Vyber soubor platebního požadavku</translation>
    </message>
    <message>
        <location filename="../openuridialog.cpp" line="+40"/>
        <source>Select payment request file to open</source>
        <translation>Vyber soubor platebního požadavku k načtení</translation>
    </message>
</context>
<context>
    <name>OptionsDialog</name>
    <message>
        <location filename="../forms/optionsdialog.ui" line="+14"/>
        <source>Options</source>
        <translation>Možnosti</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>&amp;Main</source>
        <translation>&amp;Hlavní</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Number of script &amp;verification threads</source>
        <translation>Počet vláken pro &amp;verifikaci skriptů</translation>
    </message>
    <message>
        <location line="+195"/>
        <source>Allow incoming connections</source>
        <translation>Přijímat příchozí spojení</translation>
    </message>
    <message>
        <location line="+44"/>
        <location line="+187"/>
        <source>IP address of the proxy (e.g. IPv4: 127.0.0.1 / IPv6: ::1)</source>
        <translation>IP adresa proxy (např. IPv4: 127.0.0.1/IPv6: ::1)</translation>
    </message>
    <message>
        <location line="+84"/>
        <source>Minimize instead of exit the application when the window is closed. When this option is enabled, the application will be closed only after selecting Exit in the menu.</source>
        <translation>Zavřením se aplikace minimalizuje. Pokud je tato volba zaškrtnuta, tak se aplikace ukončí pouze zvolením Konec v menu.</translation>
    </message>
    <message>
        <location line="+80"/>
        <location line="+13"/>
        <source>Third party URLs (e.g. a block explorer) that appear in the transactions tab as context menu items. %s in the URL is replaced by transaction hash. Multiple URLs are separated by vertical bar |.</source>
        <translation>URL třetích stran (např. block exploreru), které se zobrazí v kontextovém menu v záložce Transakce. %s v URL se nahradí hashem transakce. Více URL odděl svislítkem |.</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Third party transaction URLs</source>
        <translation>URL transakcí třetích stran</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Active command-line options that override above options:</source>
        <translation>Aktivní argumenty z příkazové řádky, které přetloukly tato nastavení:</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Reset all client options to default.</source>
        <translation>Vrátí všechny volby na výchozí hodnoty.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Reset Options</source>
        <translation>&amp;Obnovit nastavení</translation>
    </message>
    <message>
        <location line="-504"/>
        <source>&amp;Network</source>
        <translation>&amp;Síť</translation>
    </message>
    <message>
        <location line="-145"/>
        <source>(0 = auto, &lt;0 = leave that many cores free)</source>
        <translation>(0 = automaticky, &lt;0 = nechat daný počet jader volný, výchozí: 0)</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>W&amp;allet</source>
        <translation>P&amp;eněženka</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Expert</source>
        <translation>Odborník</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Enable coin &amp;control features</source>
        <translation>Povolit ruční správu &amp;mincí</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>If you disable the spending of unconfirmed change, the change from a transaction cannot be used until that transaction has at least one confirmation. This also affects how your balance is computed.</source>
        <translation>Pokud zakážeš utrácení ještě nepotvrzených drobných, nepůjde použít drobné z transakce, dokud nebude mít alespoň jedno potvrzení. Ovlivní to také výpočet stavu účtu.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Spend unconfirmed change</source>
        <translation>&amp;Utrácet i ještě nepotvrzené drobné</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When Instant Transactions is enabled you can spend unconfirmed transactions immediately.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Instant Transactions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When creating and sending transactions, auto consolidate will, if required, automatically create a chain of transactions which have inputs no greater than the consensus input limit.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Auto Consolidate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+54"/>
        <source>Automatically open the Nexa client port on the router. This only works when your router supports UPnP and it is enabled.</source>
        <translation>Automaticky otevře potřebný port na routeru. Tohle funguje jen za předpokladu, že tvůj router podporuje UPnP a že je UPnP povolené.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Map port using &amp;UPnP</source>
        <translation>Namapovat port přes &amp;UPnP</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Accept connections from outside.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Connect to the Nexa network through a SOCKS5 proxy.</source>
        <translation>Připojí se do Nexaové sítě přes SOCKS5 proxy.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Connect through SOCKS5 proxy (default proxy):</source>
        <translation>&amp;Připojit přes SOCKS5 proxy (výchozí proxy):</translation>
    </message>
    <message>
        <location line="+9"/>
        <location line="+187"/>
        <source>Proxy &amp;IP:</source>
        <translation>&amp;IP adresa proxy:</translation>
    </message>
    <message>
        <location line="-155"/>
        <location line="+187"/>
        <source>&amp;Port:</source>
        <translation>Por&amp;t:</translation>
    </message>
    <message>
        <location line="-162"/>
        <location line="+187"/>
        <source>Port of the proxy (e.g. 9050)</source>
        <translation>Port proxy (např. 9050)</translation>
    </message>
    <message>
        <location line="-163"/>
        <source>Used for reaching peers via:</source>
        <translation>Použije se k připojování k protějskům přes:</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>IPv4</source>
        <translation>IPv4</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>IPv6</source>
        <translation>IPv6</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Tor</source>
        <translation>Tor</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Connect to the Nexa network through a separate SOCKS5 proxy for Tor hidden services.</source>
        <translation>Připojí se do Nexaové sítě přes SOCKS5 proxy vyhrazenou pro skryté služby v Tor síti.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Use separate SOCKS5 proxy to reach peers via Tor hidden services:</source>
        <translation>Použít samostatnou SOCKS5 proxy ke spojení s protějšky přes skryté služby v Toru:</translation>
    </message>
    <message>
        <location line="+102"/>
        <source>&amp;Window</source>
        <translation>O&amp;kno</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Show only a tray icon after minimizing the window.</source>
        <translation>Po minimalizaci okna zobrazí pouze ikonu v panelu.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Minimize to the tray instead of the taskbar</source>
        <translation>&amp;Minimalizovávat do ikony v panelu</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>M&amp;inimize on close</source>
        <translation>Za&amp;vřením minimalizovat</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>&amp;Display</source>
        <translation>Zobr&amp;azení</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>User Interface &amp;language:</source>
        <translation>&amp;Jazyk uživatelského rozhraní:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>The user interface language can be set here. This setting will take effect after restarting %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Unit to show amounts in:</source>
        <translation>J&amp;ednotka pro částky:</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Choose the default subdivision unit to show in the interface and when sending coins.</source>
        <translation>Zvol výchozí podjednotku, která se bude zobrazovat v programu a při posílání mincí.</translation>
    </message>
    <message>
        <location line="-502"/>
        <source>Whether to show coin control features or not.</source>
        <translation>Zda ukazovat možnosti pro ruční správu mincí nebo ne.</translation>
    </message>
    <message>
        <location line="-121"/>
        <source>Automatically start %1 after logging in to the system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Start %1 on system login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Automatically initiate a, one time only, full database reindex on the next startup.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Reindex on startup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+333"/>
        <location line="+23"/>
        <location line="+23"/>
        <source>Shows if the supplied default SOCKS5 proxy is used to reach peers via this network type.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+387"/>
        <source>&amp;OK</source>
        <translation>&amp;Budiž</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Zrušit</translation>
    </message>
    <message>
        <location filename="../optionsdialog.cpp" line="+118"/>
        <source>default</source>
        <translation>výchozí</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>none</source>
        <translation>žádné</translation>
    </message>
    <message>
        <location line="+73"/>
        <source>Confirm options reset</source>
        <translation>Potvrzení obnovení nastavení</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+27"/>
        <source>Client restart required to activate changes.</source>
        <translation>K aktivaci změn je potřeba restartovat klienta.</translation>
    </message>
    <message>
        <location line="-26"/>
        <source>Client will be shut down. Do you want to proceed?</source>
        <translation>Klient se vypne, chceš pokračovat?</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>This change would require a client restart.</source>
        <translation>Tahle změna bude chtít restartovat klienta.</translation>
    </message>
    <message>
        <location line="+82"/>
        <source>The supplied proxy address is invalid.</source>
        <translation>Zadaná adresa proxy je neplatná.</translation>
    </message>
</context>
<context>
    <name>OverviewPage</name>
    <message>
        <location filename="../forms/overviewpage.ui" line="+14"/>
        <source>Form</source>
        <translation>Formulář</translation>
    </message>
    <message>
        <location line="+59"/>
        <location line="+386"/>
        <source>The displayed information may be out of date. Your wallet automatically synchronizes with the Nexa network after a connection is established, but this process has not completed yet.</source>
        <translation>Zobrazené informace nemusí být aktuální. Tvá peněženka se automaticky sesynchronizuje s Nexaovou sítí, jakmile se s ní spojí. Zatím ale ještě není synchronizace dokončena.</translation>
    </message>
    <message>
        <location line="-139"/>
        <source>Watch-only:</source>
        <translation>Sledované:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Available:</source>
        <translation>K dispozici:</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Your current spendable balance</source>
        <translation>Aktuální disponibilní stav tvého účtu</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Pending:</source>
        <translation>Očekáváno:</translation>
    </message>
    <message>
        <location line="-236"/>
        <source>Total of transactions that have yet to be confirmed, and do not yet count toward the spendable balance</source>
        <translation>Souhrn transakcí, které ještě nejsou potvrzené a které se ještě nezapočítávají do celkového disponibilního stavu účtu</translation>
    </message>
    <message>
        <location line="+112"/>
        <source>Immature:</source>
        <translation>Nedozráno:</translation>
    </message>
    <message>
        <location line="-29"/>
        <source>Mined balance that has not yet matured</source>
        <translation>Vytěžené mince, které ještě nejsou zralé</translation>
    </message>
    <message>
        <location line="-177"/>
        <source>Balances</source>
        <translation>Stavy účtů</translation>
    </message>
    <message>
        <location line="+161"/>
        <source>Total:</source>
        <translation>Celkem:</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>Your current total balance</source>
        <translation>Celkový stav tvého účtu</translation>
    </message>
    <message>
        <location line="+92"/>
        <source>Your current balance in watch-only addresses</source>
        <translation>Aktuální stav účtu sledovaných adres</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Spendable:</source>
        <translation>Běžné:</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Recent transactions</source>
        <translation>Poslední transakce</translation>
    </message>
    <message>
        <location line="-317"/>
        <source>Unconfirmed transactions to watch-only addresses</source>
        <translation>Nepotvrzené transakce sledovaných adres</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>Mined balance in watch-only addresses that has not yet matured</source>
        <translation>Vytěžené mince na sledovaných adresách, které ještě nejsou zralé</translation>
    </message>
    <message>
        <location line="+128"/>
        <source>Current total balance in watch-only addresses</source>
        <translation>Aktuální stav účtu sledovaných adres</translation>
    </message>
</context>
<context>
    <name>PaymentServer</name>
    <message>
        <location filename="../paymentserver.cpp" line="+470"/>
        <location line="+13"/>
        <location line="+10"/>
        <source>URI handling</source>
        <translation>Zpracování URI</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Invalid payment address %1</source>
        <translation>Neplatná platební adresa %1</translation>
    </message>
    <message>
        <location line="+113"/>
        <location line="+11"/>
        <location line="+35"/>
        <location line="+12"/>
        <location line="+21"/>
        <location line="+98"/>
        <source>Payment request rejected</source>
        <translation>Platební požadavek byl odmítnut</translation>
    </message>
    <message>
        <location line="-177"/>
        <source>Payment request network doesn&apos;t match client network.</source>
        <translation>Síť platebního požadavku neodpovídá síti klienta.</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Payment request is not initialized.</source>
        <translation>Platební požadavek není zahájený.</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Requested payment amount of %1 is too small (considered dust).</source>
        <translation>Požadovaná platební částka %1 je příliš malá (je považována za prach).</translation>
    </message>
    <message>
        <location line="-288"/>
        <location line="+240"/>
        <location line="+47"/>
        <location line="+126"/>
        <location line="+15"/>
        <location line="+15"/>
        <source>Payment request error</source>
        <translation>Chyba platebního požadavku</translation>
    </message>
    <message>
        <location line="-443"/>
        <source>Cannot start click-to-pay handler</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+95"/>
        <source>Payment request fetch URL is invalid: %1</source>
        <translation>Zdrojová URL platebního požadavku není platná: %1</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>URI cannot be parsed! This can be caused by an invalid Nexa address or malformed URI parameters.</source>
        <translation>Nepodařilo se analyzovat URI! Důvodem může být neplatná Nexaová adresa nebo poškozené parametry URI.</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Payment request file handling</source>
        <translation>Zpracování souboru platebního požadavku</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Payment request file cannot be read! This can be caused by an invalid payment request file.</source>
        <translation>Soubor platebního požadavku nejde přečíst nebo zpracovat! Příčinou může být špatný soubor platebního požadavku.</translation>
    </message>
    <message>
        <location line="+76"/>
        <source>Payment request expired.</source>
        <translation>Platební požadavek vypršel.</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Unverified payment requests to custom payment scripts are unsupported.</source>
        <translation>Neověřené platební požadavky k uživatelským platebním skriptům nejsou podporované.</translation>
    </message>
    <message>
        <location line="+11"/>
        <location line="+21"/>
        <source>Invalid payment request.</source>
        <translation>Neplatný platební požadavek.</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Refund from %1</source>
        <translation>Vrácení peněz od %1</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Payment request %1 is too large (%2 bytes, allowed %3 bytes).</source>
        <translation>Platební požadavek %1 je moc velký (%2 bajtů, povoleno %3 bajtů).</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Error communicating with %1: %2</source>
        <translation>Chyba při komunikaci s %1: %2</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Payment request cannot be parsed!</source>
        <translation>Platební požadavek je nečitelný!</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Bad response from server %1</source>
        <translation>Chybná odpověď ze serveru %1</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Payment acknowledged</source>
        <translation>Platba potvrzena</translation>
    </message>
    <message>
        <location line="-8"/>
        <source>Network request error</source>
        <translation>Chyba síťového požadavku</translation>
    </message>
</context>
<context>
    <name>PeerTableModel</name>
    <message>
        <location filename="../peertablemodel.cpp" line="+106"/>
        <source>User Agent</source>
        <translation>Typ klienta</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Node/Service</source>
        <translation>Uzel/Služba</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Ping Time</source>
        <translation>Odezva</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../nexaunits.cpp" line="+187"/>
        <source>Amount</source>
        <translation>Částka</translation>
    </message>
    <message>
        <location filename="../guiutil.cpp" line="+135"/>
        <source>Enter a NEXA address (e.g. %1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+832"/>
        <source>%1 d</source>
        <translation>%1 d</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 h</source>
        <translation>%1 h</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 m</source>
        <translation>%1 m</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+68"/>
        <source>%1 s</source>
        <translation>%1 s</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>None</source>
        <translation>Žádné</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>%1 ms</source>
        <translation>%1 ms</translation>
    </message>
    <message numerus="yes">
        <location line="+19"/>
        <source>%n seconds(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%n minutes(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%n hour(s)</source>
        <translation type="unfinished">
            <numerusform>%n hodinu</numerusform>
            <numerusform>%n hodiny</numerusform>
            <numerusform>%n hodin</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%n day(s)</source>
        <translation type="unfinished">
            <numerusform>%n den</numerusform>
            <numerusform>%n dny</numerusform>
            <numerusform>%n dnů</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <location line="+8"/>
        <source>%n week(s)</source>
        <translation type="unfinished">
            <numerusform>%n týden</numerusform>
            <numerusform>%n týdny</numerusform>
            <numerusform>%n týdnů</numerusform>
        </translation>
    </message>
    <message>
        <location line="-2"/>
        <source>%1 and %2</source>
        <translation type="unfinished">%1 a %2</translation>
    </message>
    <message numerus="yes">
        <location line="+1"/>
        <source>%n year(s)</source>
        <translation type="unfinished">
            <numerusform>%n rok</numerusform>
            <numerusform>%n roky</numerusform>
            <numerusform>%n roků</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>QRImageWidget</name>
    <message>
        <location filename="../receiverequestdialog.cpp" line="+36"/>
        <source>&amp;Save Image...</source>
        <translation>&amp;Ulož obrázek...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Copy Image</source>
        <translation>&amp;Kopíruj obrázek</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Save QR Code</source>
        <translation>Ulož QR kód</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>PNG Image (*.png)</source>
        <translation>PNG obrázek (*.png)</translation>
    </message>
</context>
<context>
    <name>RPCConsole</name>
    <message>
        <location filename="../forms/debugwindow.ui" line="+239"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+17"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+19"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+1753"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+26"/>
        <location line="+26"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+26"/>
        <location line="+23"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
    <message>
        <location line="-2715"/>
        <source>Client version</source>
        <translation>Verze klienta</translation>
    </message>
    <message>
        <location line="-6"/>
        <source>&amp;Information</source>
        <translation>&amp;Informace</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Debug window</source>
        <translation>Ladicí okno</translation>
    </message>
    <message>
        <location line="+620"/>
        <source>General</source>
        <translation>Obecné</translation>
    </message>
    <message>
        <location line="-587"/>
        <source>Using BerkeleyDB version</source>
        <translation>Používaná verze BerkeleyDB</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Startup time</source>
        <translation>Čas spuštění</translation>
    </message>
    <message>
        <location line="+474"/>
        <source>Network</source>
        <translation>Síť</translation>
    </message>
    <message>
        <location line="-467"/>
        <source>Name</source>
        <translation>Název</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Number of connections</source>
        <translation>Počet spojení</translation>
    </message>
    <message>
        <location line="+415"/>
        <source>Block chain</source>
        <translation>Řetězec bloků</translation>
    </message>
    <message>
        <location line="-408"/>
        <source>Current number of blocks</source>
        <translation>Aktuální počet bloků</translation>
    </message>
    <message>
        <location line="+884"/>
        <location line="+1694"/>
        <source>Received</source>
        <translation>Přijato</translation>
    </message>
    <message>
        <location line="-1614"/>
        <location line="+1591"/>
        <source>Sent</source>
        <translation>Odesláno</translation>
    </message>
    <message>
        <location line="-417"/>
        <source>&amp;Peers</source>
        <translation>&amp;Protějšky</translation>
    </message>
    <message>
        <location line="+107"/>
        <location filename="../rpcconsole.cpp" line="+311"/>
        <location line="+855"/>
        <source>Select a peer to view detailed information.</source>
        <translation>Vyber protějšek a uvidíš jeho detailní informace.</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Direction</source>
        <translation>Směr</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Version</source>
        <translation>Verze</translation>
    </message>
    <message>
        <location line="-2367"/>
        <location line="+2390"/>
        <source>User Agent</source>
        <translation>Typ klienta</translation>
    </message>
    <message>
        <location line="-2370"/>
        <source>Datadir</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Last block time (time since)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Last block size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Transactions in Tx pool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Transactions in Orphan pool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Messages in CAPD pool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Tx pool - usage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Tx pool - txns per second</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>XThin (Totals)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>XThin (24-Hour Averages)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Compact (Totals)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Compact (24-Hour Averages)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Graphene (Totals)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Graphene (24-Hour Averages)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Open the %1 debug log file from the current data directory. This can take a few seconds for large log files.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+130"/>
        <source>Block Propagation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+94"/>
        <source>Transaction Pools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+246"/>
        <source>Decrease font size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Increase font size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+380"/>
        <source>&amp;Transaction Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+58"/>
        <source>Instantaneous Rate (1s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+536"/>
        <source>Peak</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-463"/>
        <location line="+257"/>
        <location line="+279"/>
        <location line="+257"/>
        <source>Runtime</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-713"/>
        <location line="+257"/>
        <location line="+279"/>
        <location line="+257"/>
        <source>24-Hours</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-713"/>
        <location line="+257"/>
        <location line="+279"/>
        <location line="+257"/>
        <source>Displayed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-769"/>
        <location line="+536"/>
        <source>Average</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-265"/>
        <source>Smoothed Rate (60s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+589"/>
        <source>Banned peers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+85"/>
        <source>Whitelisted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+95"/>
        <source>Services</source>
        <translation>Služby</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Starting Block</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Synced Headers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Synced Blocks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Ban Score</source>
        <translation>Skóre pro klatbu</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Connection Time</source>
        <translation>Doba spojení</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Last Send</source>
        <translation>Poslední odeslání</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Last Receive</source>
        <translation>Poslední příjem</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Ping Time</source>
        <translation>Odezva</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>The duration of a currently outstanding ping.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Ping Wait</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Time Offset</source>
        <translation>Časový posun</translation>
    </message>
    <message>
        <location line="-2524"/>
        <source>&amp;Open</source>
        <translation>&amp;Otevřít</translation>
    </message>
    <message>
        <location line="+431"/>
        <source>&amp;Console</source>
        <translation>&amp;Konzole</translation>
    </message>
    <message>
        <location line="+195"/>
        <source>&amp;Network Traffic</source>
        <translation>&amp;Síťový provoz</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>&amp;Clear</source>
        <translation>&amp;Vyčistit</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Totals</source>
        <translation>Součty</translation>
    </message>
    <message>
        <location filename="../rpcconsole.cpp" line="-530"/>
        <source>In:</source>
        <translation>Sem:</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Out:</source>
        <translation>Ven:</translation>
    </message>
    <message>
        <location filename="../forms/debugwindow.ui" line="-704"/>
        <source>Debug log file</source>
        <translation>Soubor s ladicími záznamy</translation>
    </message>
    <message>
        <location line="+541"/>
        <source>Clear console</source>
        <translation>Vyčistit konzoli</translation>
    </message>
    <message>
        <location filename="../rpcconsole.cpp" line="-207"/>
        <source>&amp;Disconnect Node</source>
        <translation>&amp;Odpojit uzel</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+1"/>
        <location line="+1"/>
        <location line="+1"/>
        <source>Ban Node for</source>
        <translation>Uvalit na uzel klatbu na</translation>
    </message>
    <message>
        <location line="-3"/>
        <source>1 &amp;hour</source>
        <translation>1 &amp;hodinu</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>1 &amp;day</source>
        <translation>1 &amp;den</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>1 &amp;week</source>
        <translation>1 &amp;týden</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>1 &amp;year</source>
        <translation>1 &amp;rok</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>&amp;Unban Node</source>
        <translation>&amp;Zbavit uzel klatby</translation>
    </message>
    <message>
        <location line="+118"/>
        <source>Welcome to the %1 RPC console.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Use up and down arrows to navigate history, and &lt;b&gt;Ctrl-L&lt;/b&gt; to clear screen.</source>
        <translation>V historii se pohybuješ šipkami nahoru a dolů a pomocí &lt;b&gt;Ctrl-L&lt;/b&gt; čistíš obrazovku.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Type &lt;b&gt;help&lt;/b&gt; for an overview of available commands.</source>
        <translation>Napsáním &lt;b&gt;help&lt;/b&gt; si vypíšeš přehled dostupných příkazů.</translation>
    </message>
    <message>
        <location line="+125"/>
        <location line="+1"/>
        <location line="+28"/>
        <location line="+1"/>
        <location line="+28"/>
        <location line="+1"/>
        <source>Disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+110"/>
        <source>%1 B</source>
        <translation>%1 B</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 KB</source>
        <translation>%1 kB</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 MB</source>
        <translation>%1 MB</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 GB</source>
        <translation>%1 GB</translation>
    </message>
    <message>
        <location line="+117"/>
        <source>(node id: %1)</source>
        <translation>(id uzlu: %1)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>via %1</source>
        <translation>via %1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+2"/>
        <source>never</source>
        <translation>nikdy</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Inbound</source>
        <translation>Sem</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Outbound</source>
        <translation>Ven</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Yes</source>
        <translation>Ano</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>No</source>
        <translation>Ne</translation>
    </message>
    <message>
        <location line="+13"/>
        <location line="+6"/>
        <source>Unknown</source>
        <translation>Neznámá</translation>
    </message>
</context>
<context>
    <name>ReceiveCoinsDialog</name>
    <message>
        <location filename="../forms/receivecoinsdialog.ui" line="+110"/>
        <source>&amp;Amount:</source>
        <translation>Čás&amp;tka:</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>&amp;Label:</source>
        <translation>&amp;Označení:</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>&amp;Message:</source>
        <translation>&amp;Zpráva:</translation>
    </message>
    <message>
        <location line="-27"/>
        <source>Reuse one of the previously used receiving addresses. Reusing addresses has security and privacy issues. Do not use this unless re-generating a payment request made before.</source>
        <translation>Recyklovat již dříve použité adresy. Recyklace adres má bezpečnostní rizika a narušuje soukromí. Nezaškrtávejte to, pokud znovu nevytváříte již dříve vytvořený platební požadavek.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>R&amp;euse an existing receiving address (not recommended)</source>
        <translation>&amp;Recyklovat již existující adresy (nedoporučeno)</translation>
    </message>
    <message>
        <location line="+21"/>
        <location line="+23"/>
        <source>An optional message to attach to the payment request, which will be displayed when the request is opened. Note: The message will not be sent with the payment over the Nexa network.</source>
        <translation>Volitelná zpráva, která se připojí k platebnímu požadavku a která se zobrazí, když se požadavek otevře. Poznámka: Tahle zpráva se neposílá s platbou po Nexaové síti.</translation>
    </message>
    <message>
        <location line="-63"/>
        <location line="+56"/>
        <source>An optional label to associate with the new receiving address.</source>
        <translation>Volitelné označení, které se má přiřadit k nové adrese.</translation>
    </message>
    <message>
        <location line="-23"/>
        <source>Use this form to request payments. All fields are &lt;b&gt;optional&lt;/b&gt;.</source>
        <translation>Tímto formulář můžeš požadovat platby. Všechna pole jsou &lt;b&gt;volitelná&lt;/b&gt;.</translation>
    </message>
    <message>
        <location line="-56"/>
        <location line="+7"/>
        <source>An optional amount to request. Leave this empty or zero to not request a specific amount.</source>
        <translation>Volitelná částka, kterou požaduješ. Nech prázdné nebo nulové, pokud nepožaduješ konkrétní částku.</translation>
    </message>
    <message>
        <location line="-45"/>
        <source>Clear all fields of the form.</source>
        <translation>Promaž obsah ze všech formulářových políček.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Clear</source>
        <translation>Vyčistit</translation>
    </message>
    <message>
        <location line="+177"/>
        <source>Requested payments history</source>
        <translation>Historie vyžádaných plateb</translation>
    </message>
    <message>
        <location line="-197"/>
        <source>&amp;Request payment</source>
        <translation>&amp;Vyžádat platbu</translation>
    </message>
    <message>
        <location line="+148"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Coin freezing locks coins to make them temporarily unspendable. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Coin &amp;Freeze</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+71"/>
        <source>Show the selected request (does the same as double clicking an entry)</source>
        <translation>Zobraz zvolený požadavek (stejně tak můžeš přímo na něj dvakrát poklepat)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show</source>
        <translation>Zobrazit</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Remove the selected entries from the list</source>
        <translation>Smaž zvolené požadavky ze seznamu</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Remove</source>
        <translation>Smazat</translation>
    </message>
    <message>
        <location filename="../receivecoinsdialog.cpp" line="+53"/>
        <source>Copy URI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy label</source>
        <translation>Kopíruj její označení</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy message</source>
        <translation>Kopíruj zprávu</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation>Kopíruj částku</translation>
    </message>
</context>
<context>
    <name>ReceiveFreezeDialog</name>
    <message>
        <location filename="../forms/receivefreezedialog.ui" line="+14"/>
        <source>Coin Freeze</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+24"/>
        <source>WARNING! Freezing coins means they will be UNSPENDABLE until the release date or block specified below.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Freeze until block :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+19"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Specify the block when coins are released from the freeze. Coins are UNSPENDABLE until the freeze block.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+99"/>
        <source>-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-57"/>
        <source>OR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Freeze until date and time :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Specify the future date and time when coins are released from the freeze. Coins are UNSPENDABLE until after the freeze date and time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+98"/>
        <source>&amp;Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Alt+R</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+25"/>
        <source>&amp;OK</source>
        <translation type="unfinished">&amp;Budiž</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Alt+O</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ReceiveRequestDialog</name>
    <message>
        <location filename="../forms/receiverequestdialog.ui" line="+29"/>
        <source>QR Code</source>
        <translation>QR kód</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Copy &amp;URI</source>
        <translation>&amp;Kopíruj URI</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Copy &amp;Address</source>
        <translation>Kopíruj &amp;adresu</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Save Image...</source>
        <translation>&amp;Ulož obrázek...</translation>
    </message>
    <message>
        <location filename="../receiverequestdialog.cpp" line="+78"/>
        <source>Request payment to %1</source>
        <translation>Platební požadavek: %1</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Payment information</source>
        <translation>Informace o platbě</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>URI</source>
        <translation>URI</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Address</source>
        <translation>Adresa</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Amount</source>
        <translation>Částka</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Label</source>
        <translation>Označení</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Message</source>
        <translation>Zpráva</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Freeze until</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Resulting URI too long, try to reduce the text for label / message.</source>
        <translation>Výsledná URI je příliš dlouhá, zkus zkrátit text označení/zprávy.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Error encoding URI into QR Code.</source>
        <translation>Chyba při kódování URI do QR kódu.</translation>
    </message>
</context>
<context>
    <name>RecentRequestsTableModel</name>
    <message>
        <location filename="../recentrequeststablemodel.cpp" line="+30"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Label</source>
        <translation>Označení</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Message</source>
        <translation>Zpráva</translation>
    </message>
    <message>
        <location line="+97"/>
        <source>Amount</source>
        <translation>Částka</translation>
    </message>
    <message>
        <location line="-57"/>
        <source>(no label)</source>
        <translation>(bez označení)</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>(no message)</source>
        <translation>(bez zprávy)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>(no amount)</source>
        <translation>(bez částky)</translation>
    </message>
</context>
<context>
    <name>SendCoinsDialog</name>
    <message>
        <location filename="../forms/sendcoinsdialog.ui" line="+14"/>
        <location filename="../sendcoinsdialog.cpp" line="+627"/>
        <source>Send Coins</source>
        <translation>Pošli mince</translation>
    </message>
    <message>
        <location line="+76"/>
        <source>Coin Control Features</source>
        <translation>Možnosti ruční správy mincí</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Inputs...</source>
        <translation>Vstupy...</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>automatically selected</source>
        <translation>automaticky vybrané</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Insufficient funds!</source>
        <translation>Nedostatek prostředků!</translation>
    </message>
    <message>
        <location line="+89"/>
        <source>Quantity:</source>
        <translation>Počet:</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Bytes:</source>
        <translation>Bajtů:</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Amount:</source>
        <translation>Částka:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Priority:</source>
        <translation>Priorita:</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Fee:</source>
        <translation>Poplatek:</translation>
    </message>
    <message>
        <location line="+80"/>
        <source>After Fee:</source>
        <translation>Čistá částka:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Change:</source>
        <translation>Drobné:</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>If this is activated, but the change address is empty or invalid, change will be sent to a newly generated address.</source>
        <translation>Pokud aktivováno, ale adresa pro drobné je prázdná nebo neplatná, tak se drobné pošlou na nově vygenerovanou adresu.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Custom change address</source>
        <translation>Vlastní adresa pro drobné</translation>
    </message>
    <message>
        <location line="+206"/>
        <source>Transaction Fee:</source>
        <translation>Transakční poplatek:</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Choose...</source>
        <translation>Zvol...</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>collapse fee-settings</source>
        <translation>sbal nastavení poplatků</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>per kilobyte</source>
        <translation>za kilobajt</translation>
    </message>
    <message>
        <location line="-3"/>
        <location line="+16"/>
        <source>If the custom fee is set to 1000 satoshis and the transaction is only 250 bytes, then &quot;per kilobyte&quot; only pays 250 satoshis in fee, while &quot;total at least&quot; pays 1000 satoshis. For transactions bigger than a kilobyte both pay by kilobyte.</source>
        <translation>Pokud je vlastní poplatek nastavený na 1000 satoshi a transakce má pouze 250 bajtů, tak „za kilobajt“ zaplatí poplatek jen 250 satoshi, zatímco „přinejmenším“ zaplatí 1000 satoshi. Pro transakce větší než kilobajt obě možnosti platí za kilobajt.</translation>
    </message>
    <message>
        <location line="-64"/>
        <source>Hide</source>
        <translation>Skryj</translation>
    </message>
    <message>
        <location line="+67"/>
        <source>total at least</source>
        <translation>přinejmenším</translation>
    </message>
    <message>
        <location line="+30"/>
        <location line="+13"/>
        <source>Paying only the minimum fee is just fine as long as there is less transaction volume than space in the blocks. But be aware that this can end up in a never confirming transaction once there is more demand for nexa transactions than the network can process.</source>
        <translation>Platit jen minimální poplatek je v pořádku, pokud je zrovna méně transakcí než místa v blocích. Ale počítej s tím, že to také může skončit transakcí, která nikdy nebude potvrzena, pokud je větší poptávka po nexaových transakcích, než síť zvládne zpracovat.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>(read the tooltip)</source>
        <translation>(viz bublina)</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Recommended:</source>
        <translation>Doporučený:</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Custom:</source>
        <translation>Vlastní:</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>(Smart fee not initialized yet. This usually takes a few blocks...)</source>
        <translation>(Inteligentní poplatek ještě není inicializovaný. Obvykle mu to tak pár bloků trvá...)</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Confirmation time:</source>
        <translation>Rychlost potvrzení:</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>normal</source>
        <translation>normální</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>fast</source>
        <translation>rychlá</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Send as zero-fee transaction if possible</source>
        <translation>Pošli transakci pokud možno bez poplatku</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>(confirmation may take longer)</source>
        <translation>(potvrzení může trvat déle)</translation>
    </message>
    <message>
        <location line="+110"/>
        <source>Send to multiple recipients at once</source>
        <translation>Pošli více příjemcům naráz</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Add &amp;Recipient</source>
        <translation>Při&amp;dej příjemce</translation>
    </message>
    <message>
        <location line="-20"/>
        <source>Clear all fields of the form.</source>
        <translation>Promaž obsah ze všech formulářových políček.</translation>
    </message>
    <message>
        <location line="-858"/>
        <source>Dust:</source>
        <translation>Prach:</translation>
    </message>
    <message>
        <location line="+861"/>
        <source>Clear &amp;All</source>
        <translation>Všechno s&amp;maž</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>Balance:</source>
        <translation>Stav účtu:</translation>
    </message>
    <message>
        <location line="-84"/>
        <source>Confirm the send action</source>
        <translation>Potvrď odeslání</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>S&amp;end</source>
        <translation>Pošl&amp;i</translation>
    </message>
    <message>
        <location filename="../sendcoinsdialog.cpp" line="-231"/>
        <source>Confirm send coins</source>
        <translation>Potvrď odeslání mincí</translation>
    </message>
    <message>
        <location line="-59"/>
        <location line="+5"/>
        <location line="+5"/>
        <location line="+4"/>
        <source>%1 to %2</source>
        <translation>%1 pro %2</translation>
    </message>
    <message>
        <location line="-283"/>
        <source>Copy quantity</source>
        <translation>Kopíruj počet</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation>Kopíruj částku</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy fee</source>
        <translation>Kopíruj poplatek</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy after fee</source>
        <translation>Kopíruj čistou částku</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy bytes</source>
        <translation>Kopíruj bajty</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy priority</source>
        <translation>Kopíruj prioritu</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Copy change</source>
        <translation>Kopíruj drobné</translation>
    </message>
    <message>
        <location line="+247"/>
        <source>&lt;b&gt;Public label:&lt;/b&gt; %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+33"/>
        <source>&lt;br&gt;&lt;br&gt;&lt;b&gt;WARNING!!! DESTINATION IS A FREEZE ADDRESS&lt;br&gt;UNSPENDABLE UNTIL&lt;/b&gt; %1 &lt;br&gt;*************************************************&lt;br&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Total Amount %1</source>
        <translation>Celková částka %1</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>or</source>
        <translation>nebo</translation>
    </message>
    <message>
        <location line="+190"/>
        <source>The amount to pay must be larger than 0.</source>
        <translation>Odesílaná částka musí být větší než 0.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The amount exceeds your balance.</source>
        <translation>Částka překračuje stav účtu.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The total exceeds your balance when the %1 transaction fee is included.</source>
        <translation>Celková částka při připočítání poplatku %1 překročí stav účtu.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Transaction creation failed!</source>
        <translation>Vytvoření transakce selhalo!</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>The transaction was rejected! This might happen if some of the coins in your wallet were already spent, such as if you used a copy of wallet.dat and coins were spent in the copy but not marked as spent here.</source>
        <translation>Transakce byla odmítnuta! Tohle může nastat, pokud nějaké mince z tvé peněženky už jednou byly utraceny, například pokud používáš kopii souboru wallet.dat a mince byly utraceny v druhé kopii, ale nebyly označeny jako utracené v této.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>A fee higher than %1 is considered an absurdly high fee.</source>
        <translation>Poplatek vyšší než %1 je považován za absurdně vysoký.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Payment request expired.</source>
        <translation>Platební požadavek vypršel.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Public Label exeeds limit of </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+105"/>
        <source>Pay only the required fee of %1</source>
        <translation>Zaplatit pouze vyžadovaný poplatek %1</translation>
    </message>
    <message>
        <location line="-139"/>
        <source>The recipient address is not valid. Please recheck.</source>
        <translation>Adresa příjemce je neplatná – překontroluj ji prosím.</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Duplicate address found: addresses should only be used once each.</source>
        <translation>Zaznamenána duplicitní adresa: každá adresa by ale měla být použita vždy jen jednou.</translation>
    </message>
    <message>
        <location line="+255"/>
        <source>Warning: Invalid Nexa address</source>
        <translation>Upozornění: Neplatná Nexaová adresa</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Confirm custom change address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The address you selected for change is not part of this wallet. Any or all funds in your wallet may be sent to this address. Are you sure?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+24"/>
        <source>(no label)</source>
        <translation>(bez označení)</translation>
    </message>
    <message>
        <location line="-28"/>
        <source>Warning: Unknown change address</source>
        <translation>Upozornění: Neznámá adresa pro drobné</translation>
    </message>
    <message>
        <location line="-781"/>
        <source>Copy dust</source>
        <translation>Kopíruj prach</translation>
    </message>
    <message>
        <location line="+292"/>
        <source>Are you sure you want to send?</source>
        <translation>Jsi si jistý, že to chceš poslat?</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>added as transaction fee</source>
        <translation>přidán jako transakční poplatek</translation>
    </message>
</context>
<context>
    <name>SendCoinsEntry</name>
    <message>
        <location filename="../forms/sendcoinsentry.ui" line="+82"/>
        <location line="+650"/>
        <location line="+533"/>
        <source>A&amp;mount:</source>
        <translation>Čás&amp;tka:</translation>
    </message>
    <message>
        <location line="-1170"/>
        <source>Pay &amp;To:</source>
        <translation>&amp;Komu:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Choose previously used address</source>
        <translation>Vyber již použitou adresu</translation>
    </message>
    <message>
        <location line="-106"/>
        <source>This is a normal payment.</source>
        <translation>Tohle je normální platba.</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Private Description:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Private &amp;Label:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+51"/>
        <source>The Nexa address to send the payment to</source>
        <translation>Nexaová adresa příjemce</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Paste address from clipboard</source>
        <translation>Vlož adresu ze schránky</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Alt+P</source>
        <translation>Alt+P</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+526"/>
        <location line="+533"/>
        <source>Remove this entry</source>
        <translation>Smaž tento záznam</translation>
    </message>
    <message>
        <location line="-1037"/>
        <source>A message that was attached to the coin: URI which will be stored with the transaction for your reference. Note: This message will not be sent over the Nexa network.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Enter a private label for this address to add it to the list of used addresses</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <location filename="../sendcoinsentry.cpp" line="+43"/>
        <source>Enter a public label for this transaction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Public Label:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-180"/>
        <source>The fee will be deducted from the amount being sent. The recipient will receive less coins than you enter in the amount field. If multiple recipients are selected, the fee is split equally.</source>
        <translation>Poplatek se odečte od posílané částky. Příjemce tak dostane méně coinů, než zadáš do pole Částka. Pokud vybereš více příjemců, tak se poplatek rovnoměrně rozloží.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>S&amp;ubtract fee from amount</source>
        <translation>Od&amp;ečíst poplatek od částky</translation>
    </message>
    <message>
        <location line="+616"/>
        <source>This is an unauthenticated payment request.</source>
        <translation>Tohle je neověřený platební požadavek.</translation>
    </message>
    <message>
        <location line="+529"/>
        <source>This is an authenticated payment request.</source>
        <translation>Tohle je ověřený platební požadavek.</translation>
    </message>
    <message>
        <location filename="../sendcoinsentry.cpp" line="-12"/>
        <source>A message that was attached to the %1 URI which will be stored with the transaction for your reference. Note: This message will not be sent over the Nexa network.</source>
        <translation>Zpráva, která byla připojena k %1 URI a která se ti pro přehled uloží k transakci. Poznámka: Tahle zpráva se neposílá s platbou po Nexaové síti.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Enter a private label for this address to add it to your address book</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/sendcoinsentry.ui" line="-514"/>
        <location line="+529"/>
        <source>Pay To:</source>
        <translation>Komu:</translation>
    </message>
    <message>
        <location line="-495"/>
        <location line="+533"/>
        <source>Memo:</source>
        <translation>Poznámka:</translation>
    </message>
</context>
<context>
    <name>ShutdownWindow</name>
    <message>
        <location filename="../utilitydialog.cpp" line="+76"/>
        <source>%1 is shutting down...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Do not shut down the computer until this window disappears.</source>
        <translation>Nevypínej počítač, dokud toto okno nezmizí.</translation>
    </message>
</context>
<context>
    <name>SignVerifyMessageDialog</name>
    <message>
        <location filename="../forms/signverifymessagedialog.ui" line="+14"/>
        <source>Signatures - Sign / Verify a Message</source>
        <translation>Podpisy - podepsat/ověřit zprávu</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Sign Message</source>
        <translation>&amp;Podepiš zprávu</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>You can sign messages/agreements with your addresses to prove you can receive coins sent to them. Be careful not to sign anything vague or random, as phishing attacks may try to trick you into signing your identity over to them. Only sign fully-detailed statements you agree to.</source>
        <translation>Podepsáním zprávy/smlouvy svými adresami můžeš prokázat, že jsi na ně schopen přijmout coiny. Buď opatrný a nepodepisuj nic vágního nebo náhodného; například při phishingových útocích můžeš být lákán, abys něco takového podepsal. Podepisuj pouze naprosto úplná a detailní prohlášení, se kterými souhlasíš.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>The Nexa address to sign the message with</source>
        <translation>Nexaová adresa, kterou se zpráva podepíše</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+210"/>
        <source>Choose previously used address</source>
        <translation>Vyber již použitou adresu</translation>
    </message>
    <message>
        <location line="-200"/>
        <location line="+210"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location line="-200"/>
        <source>Paste address from clipboard</source>
        <translation>Vlož adresu ze schránky</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Alt+P</source>
        <translation>Alt+P</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Enter the message you want to sign here</source>
        <translation>Sem vepiš zprávu, kterou chceš podepsat</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Signature</source>
        <translation>Podpis</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Copy the current signature to the system clipboard</source>
        <translation>Zkopíruj aktuálně vybraný podpis do systémové schránky</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Sign the message to prove you own this Nexa address</source>
        <translation>Podepiš zprávu, čímž prokážeš, že jsi vlastníkem této Nexaové adresy</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Sign &amp;Message</source>
        <translation>Po&amp;depiš zprávu</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Reset all sign message fields</source>
        <translation>Vymaž všechna pole formuláře pro podepsání zrávy</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+143"/>
        <source>Clear &amp;All</source>
        <translation>Všechno &amp;smaž</translation>
    </message>
    <message>
        <location line="-84"/>
        <source>&amp;Verify Message</source>
        <translation>&amp;Ověř zprávu</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Enter the receiver&apos;s address, message (ensure you copy line breaks, spaces, tabs, etc. exactly) and signature below to verify the message. Be careful not to read more into the signature than what is in the signed message itself, to avoid being tricked by a man-in-the-middle attack. Note that this only proves the signing party receives with the address, it cannot prove sendership of any transaction!</source>
        <translation>K ověření podpisu zprávy zadej adresu příjemce, zprávu (ověř si, že správně kopíruješ zalomení řádků, mezery, tabulátory apod.) a podpis. Dávej pozor na to, abys nezkopíroval do podpisu víc, než co je v samotné podepsané zprávě, abys nebyl napálen man-in-the-middle útokem. Poznamenejme však, že takto lze pouze prokázat, že podepisující je schopný na dané adrese přijmout platbu, ale není možnéprokázat, že odeslal jakoukoli transakci!</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>The Nexa address the message was signed with</source>
        <translation>Nexaová adresa, kterou je zpráva podepsána</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Verify the message to ensure it was signed with the specified Nexa address</source>
        <translation>Ověř zprávu, aby ses ujistil, že byla podepsána danou Nexaovou adresou</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Verify &amp;Message</source>
        <translation>O&amp;věř zprávu</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Reset all verify message fields</source>
        <translation>Vymaž všechna pole formuláře pro ověření zrávy</translation>
    </message>
    <message>
        <location filename="../signverifymessagedialog.cpp" line="+40"/>
        <source>Click &quot;Sign Message&quot; to generate signature</source>
        <translation>Kliknutím na &quot;Podepiš zprávu&quot; vygeneruješ podpis</translation>
    </message>
    <message>
        <location line="+68"/>
        <source>Wallet unlock was cancelled.</source>
        <translation>Odemčení peněženky bylo zrušeno.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Message signed.</source>
        <translation>Zpráv podepsána.</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Message verified.</source>
        <translation>Zpráva ověřena.</translation>
    </message>
</context>
<context>
    <name>SplashScreen</name>
    <message>
        <location filename="../networkstyle.cpp" line="+19"/>
        <source>[testnet]</source>
        <translation>[testnet]</translation>
    </message>
</context>
<context>
    <name>TrafficGraphWidget</name>
    <message>
        <location filename="../trafficgraphwidget.cpp" line="+74"/>
        <source>KB/s</source>
        <translation>kB/s</translation>
    </message>
</context>
<context>
    <name>TransactionDesc</name>
    <message>
        <location filename="../transactiondesc.cpp" line="+34"/>
        <source>Open until %1</source>
        <translation>Otřevřeno dokud %1</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>conflicted</source>
        <translation>kolidující</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1/offline</source>
        <translation>%1/offline</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1/unconfirmed</source>
        <translation>%1/nepotvrzeno</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 confirmations</source>
        <translation>%1 potvrzení</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Status</source>
        <translation>Stav</translation>
    </message>
    <message numerus="yes">
        <location line="+7"/>
        <source>, broadcast through %n node(s)</source>
        <translation>
            <numerusform>, rozesláno přes %n uzel</numerusform>
            <numerusform>, rozesláno přes %n uzly</numerusform>
            <numerusform>, rozesláno přes %n uzlů</numerusform>
        </translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Source</source>
        <translation>Zdroj</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Generated</source>
        <translation>Vygenerováno</translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+13"/>
        <location line="+128"/>
        <source>From</source>
        <translation>Od</translation>
    </message>
    <message>
        <location line="-148"/>
        <location line="+21"/>
        <location line="+35"/>
        <location line="+114"/>
        <source>To</source>
        <translation>Pro</translation>
    </message>
    <message>
        <location line="-215"/>
        <source>double spent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>abandoned</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+70"/>
        <source>change address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>own address</source>
        <translation>vlastní adresa</translation>
    </message>
    <message>
        <location line="+0"/>
        <location line="+119"/>
        <source>watch-only</source>
        <translation>sledovaná</translation>
    </message>
    <message>
        <location line="-116"/>
        <location line="+28"/>
        <location line="+113"/>
        <source>label</source>
        <translation>označení</translation>
    </message>
    <message>
        <location line="-122"/>
        <location line="+63"/>
        <location line="+48"/>
        <location line="+71"/>
        <source>Public label:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-167"/>
        <source>Freeze until</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+18"/>
        <location line="+41"/>
        <location line="+63"/>
        <location line="+47"/>
        <location line="+74"/>
        <source>Credit</source>
        <translation>Příjem</translation>
    </message>
    <message numerus="yes">
        <location line="-222"/>
        <source>matures in %n more block(s)</source>
        <translation>
            <numerusform>dozraje po %n bloku</numerusform>
            <numerusform>dozraje po %n blocích</numerusform>
            <numerusform>dozraje po %n blocích</numerusform>
        </translation>
    </message>
    <message>
        <location line="+2"/>
        <source>not accepted</source>
        <translation>neakceptováno</translation>
    </message>
    <message>
        <location line="+102"/>
        <location line="+31"/>
        <location line="+79"/>
        <source>Debit</source>
        <translation>Výdaj</translation>
    </message>
    <message>
        <location line="-98"/>
        <source>Total debit</source>
        <translation>Celkové výdaje</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Total credit</source>
        <translation>Celkové příjmy</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Transaction fee</source>
        <translation>Transakční poplatek</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Net amount</source>
        <translation>Čistá částka</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+12"/>
        <source>Message</source>
        <translation>Zpráva</translation>
    </message>
    <message>
        <location line="-9"/>
        <source>Comment</source>
        <translation>Komentář</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Transaction Idem</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Merchant</source>
        <translation>Obchodník</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Generated coins must mature %1 blocks before they can be spent. When you generated this block, it was broadcast to the network to be added to the block chain. If it fails to get into the chain, its state will change to &quot;not accepted&quot; and it won&apos;t be spendable. This may occasionally happen if another node generates a block within a few seconds of yours.</source>
        <translation>Vygenerované mince musí čekat %1 bloků, než mohou být utraceny. Když jsi vygeneroval tenhle blok, tak byl rozposlán do sítě, aby byl přidán do řetězce bloků. Pokud se mu nepodaří dostat se do řetězce, změní se na &quot;neakceptovaný&quot; a nepůjde utratit. To se občas může stát, pokud jiný uzel vygeneruje blok zhruba ve stejném okamžiku jako ty.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Debug information</source>
        <translation>Ladicí informace</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Transaction</source>
        <translation>Transakce</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inputs</source>
        <translation>Vstupy</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Amount</source>
        <translation>Částka</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+3"/>
        <source>true</source>
        <translation>true</translation>
    </message>
    <message>
        <location line="-3"/>
        <location line="+3"/>
        <source>false</source>
        <translation>false</translation>
    </message>
    <message>
        <location line="-357"/>
        <source>, has not been successfully broadcast yet</source>
        <translation>, ještě nebylo rozesláno</translation>
    </message>
    <message numerus="yes">
        <location line="-40"/>
        <source>Open for %n more block(s)</source>
        <translation>
            <numerusform>Otevřeno pro %n další blok</numerusform>
            <numerusform>Otevřeno pro %n další bloky</numerusform>
            <numerusform>Otevřeno pro %n dalších bloků</numerusform>
        </translation>
    </message>
    <message>
        <location line="+73"/>
        <source>unknown</source>
        <translation>neznámo</translation>
    </message>
</context>
<context>
    <name>TransactionDescDialog</name>
    <message>
        <location filename="../forms/transactiondescdialog.ui" line="+14"/>
        <source>Transaction details</source>
        <translation>Detaily transakce</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>This pane shows a detailed description of the transaction</source>
        <translation>Toto okno zobrazuje detailní popis transakce</translation>
    </message>
</context>
<context>
    <name>TransactionGraphWidget</name>
    <message>
        <location filename="../transactiongraphwidget.cpp" line="+148"/>
        <source>tps</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TransactionTableModel</name>
    <message>
        <location filename="../transactiontablemodel.cpp" line="+239"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location line="+89"/>
        <source>Immature (%1 confirmations, will be available after %2)</source>
        <translation>Nedozráno (%1 potvrzení, bude k dispozici za %2)</translation>
    </message>
    <message numerus="yes">
        <location line="-29"/>
        <source>Open for %n more block(s)</source>
        <translation>
            <numerusform>Otevřeno pro %n další blok</numerusform>
            <numerusform>Otevřeno pro %n další bloky</numerusform>
            <numerusform>Otevřeno pro %n dalších bloků</numerusform>
        </translation>
    </message>
    <message>
        <location line="-60"/>
        <source>Address or Label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Open until %1</source>
        <translation>Otřevřeno dokud %1</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Confirmed (%1 confirmations)</source>
        <translation>Potvrzeno (%1 potvrzení)</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>This block was not received by any other nodes and will probably not be accepted!</source>
        <translation>Tento blok nedostal žádný jiný uzel a pravděpodobně nebude akceptován!</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Generated but not accepted</source>
        <translation>Vygenerováno, ale neakceptováno</translation>
    </message>
    <message>
        <location line="-31"/>
        <source>Offline</source>
        <translation>Offline</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unconfirmed</source>
        <translation>Nepotvrzeno</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirming (%1 of %2 recommended confirmations)</source>
        <translation>Potvrzuje se (%1 z %2 doporučených potvrzení)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Conflicted</source>
        <translation>V kolizi</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Double Spent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Abandoned</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Received with</source>
        <translation>Přijato do</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Received from</source>
        <translation>Přijato od</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Sent to</source>
        <translation>Posláno na</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Payment to yourself</source>
        <translation>Platba sama sobě</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Mined</source>
        <translation>Vytěženo</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Public label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Other</source>
        <translation type="unfinished">Ostatní</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>watch-only</source>
        <translation>sledovací</translation>
    </message>
    <message>
        <location line="+276"/>
        <source>Transaction status. Hover over this field to show number of confirmations.</source>
        <translation>Stav transakce. Najetím myši na toto políčko si zobrazíš počet potvrzení.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Date and time that the transaction was received.</source>
        <translation>Datum a čas přijetí transakce.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Type of transaction.</source>
        <translation>Druh transakce.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Whether or not a watch-only address is involved in this transaction.</source>
        <translation>Zda tato transakce zahrnuje i některou sledovanou adresu.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>User-defined intent/purpose of the transaction.</source>
        <translation>Uživatelsky určený účel transakce.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Amount removed from or added to balance.</source>
        <translation>Částka odečtená z nebo přičtená k účtu.</translation>
    </message>
</context>
<context>
    <name>TransactionView</name>
    <message>
        <location filename="../transactionview.cpp" line="+77"/>
        <location line="+19"/>
        <source>All</source>
        <translation>Vše</translation>
    </message>
    <message>
        <location line="-18"/>
        <source>Today</source>
        <translation>Dnes</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This week</source>
        <translation>Tento týden</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This month</source>
        <translation>Tento měsíc</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Last month</source>
        <translation>Minulý měsíc</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This year</source>
        <translation>Letos</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Range...</source>
        <translation>Rozsah...</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Received with</source>
        <translation>Přijato</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sent to</source>
        <translation>Posláno</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>To yourself</source>
        <translation>Sám sobě</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mined</source>
        <translation>Vytěženo</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Other</source>
        <translation>Ostatní</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Enter address or label to search</source>
        <translation>Zadej adresu nebo označení pro její vyhledání</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Min amount</source>
        <translation>Minimální částka</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Copy address</source>
        <translation>Kopíruj adresu</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy label</source>
        <translation>Kopíruj její označení</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation>Kopíruj částku</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Copy raw transaction</source>
        <translation>Kopíruj surovou transakci</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Edit label</source>
        <translation>Uprav označení</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show transaction details</source>
        <translation>Zobraz detaily transakce</translation>
    </message>
    <message>
        <location line="+172"/>
        <source>Export Transaction History</source>
        <translation>Exportuj transakční historii</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Watch-only</source>
        <translation>Sledovaná</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Exporting Failed</source>
        <translation>Exportování selhalo</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to save the transaction history to %1.</source>
        <translation>Při ukládání transakční historie do %1 se přihodila nějaká chyba.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Exporting Successful</source>
        <translation>Úspěšně vyexportováno</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The transaction history was successfully saved to %1.</source>
        <translation>Transakční historie byla v pořádku uložena do %1.</translation>
    </message>
    <message>
        <location line="-29"/>
        <source>Comma separated file (*.csv)</source>
        <translation>CSV formát (*.csv)</translation>
    </message>
    <message>
        <location line="-175"/>
        <source>Copy transaction idem</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+184"/>
        <source>Confirmed</source>
        <translation>Potvrzeno</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Label</source>
        <translation>Označení</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Address</source>
        <translation>Adresa</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location line="+108"/>
        <source>Range:</source>
        <translation>Rozsah:</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>to</source>
        <translation>až</translation>
    </message>
</context>
<context>
    <name>UnitDisplayStatusBarControl</name>
    <message>
        <location filename="../nexagui.cpp" line="+117"/>
        <source>Unit to show amounts in. Click to select another unit.</source>
        <translation>Jednotka pro částky. Klikni pro výběr nějaké jiné.</translation>
    </message>
</context>
<context>
    <name>UnlimitedDialog</name>
    <message>
        <location filename="../forms/unlimited.ui" line="+14"/>
        <source>Unlimited</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Mining</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+10"/>
        <source>The largest block that will be mined</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Maximum Generated Block Size (bytes) </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+30"/>
        <source>&amp;Network</source>
        <translation type="unfinished">&amp;Síť</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Bandwidth Restrictions in KBytes/sec (check to enable):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Send</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+71"/>
        <source>Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-35"/>
        <location line="+59"/>
        <source>Average</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-31"/>
        <source>Receive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+86"/>
        <source>Active command-line options that override above options:</source>
        <translation type="unfinished">Aktivní argumenty z příkazové řádky, které přetloukly tato nastavení:</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Reset all client options to default.</source>
        <translation type="unfinished">Vrátí všechny volby na výchozí hodnoty.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Reset Options</source>
        <translation type="unfinished">&amp;Obnovit nastavení</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>&amp;OK</source>
        <translation type="unfinished">&amp;Budiž</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished">&amp;Zrušit</translation>
    </message>
    <message>
        <location filename="../unlimiteddialog.cpp" line="-345"/>
        <source>Confirm options reset</source>
        <translation type="unfinished">Potvrzení obnovení nastavení</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This is a global reset of all settings!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Client restart required to activate changes.</source>
        <translation type="unfinished">K aktivaci změn je potřeba restartovat klienta.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Client will be shut down. Do you want to proceed?</source>
        <translation type="unfinished">Klient se vypne, chceš pokračovat?</translation>
    </message>
    <message>
        <location line="+60"/>
        <location line="+39"/>
        <location line="+42"/>
        <location line="+42"/>
        <source>Upstream traffic shaping parameters can&apos;t be blank</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-108"/>
        <location line="+36"/>
        <location line="+43"/>
        <location line="+41"/>
        <source>Traffic shaping parameters have to be greater than 0.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WalletFrame</name>
    <message>
        <location filename="../walletframe.cpp" line="+26"/>
        <source>No wallet has been loaded.</source>
        <translation>Žádná peněženka se nenačetla.</translation>
    </message>
</context>
<context>
    <name>WalletModel</name>
    <message>
        <location filename="../walletmodel.cpp" line="+287"/>
        <source>Send Coins</source>
        <translation>Pošli mince</translation>
    </message>
</context>
<context>
    <name>WalletView</name>
    <message>
        <location filename="../walletview.cpp" line="+44"/>
        <source>&amp;Export</source>
        <translation>&amp;Export</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Export the data in the current tab to a file</source>
        <translation>Exportuj data z tohoto panelu do souboru</translation>
    </message>
    <message>
        <location line="+187"/>
        <source>Backup Wallet</source>
        <translation>Záloha peněženky</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Wallet Data (*.dat)</source>
        <translation>Data peněženky (*.dat)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Backup Failed</source>
        <translation>Zálohování selhalo</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to save the wallet data to %1.</source>
        <translation>Při ukládání peněženky do %1 se přihodila nějaká chyba.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>The wallet data was successfully saved to %1.</source>
        <translation>Data z peněženky byla v pořádku uložena do %1.</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Backup Successful</source>
        <translation>Úspěšně zazálohováno</translation>
    </message>
</context>
<context>
    <name>nexa</name>
    <message>
        <location filename="../nexastrings.cpp" line="+56"/>
        <source>Prune configured below the minimum of %d MiB.  Please use a higher number.</source>
        <translation>Prořezávání je nastaveno pod minimum %d MiB.  Použij, prosím, nějaké vyšší číslo.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Prune: last wallet synchronisation goes beyond pruned data. You need to -reindex (download the whole blockchain again in case of pruned node)</source>
        <translation>Prořezávání: poslední synchronizace peněženky proběhla před už prořezanými daty. Je třeba provést -reindex (tedy v případě prořezávacího režimu stáhnout znovu celý řetězec bloků)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Rescans are not possible in pruned mode. You will need to use -reindex which will download the whole blockchain again.</source>
        <translation>V prořezávacím režimu není možné přeskenovávat řetězec bloků. Musíš provést -reindex, což znovu stáhne celý řetězec bloků.</translation>
    </message>
    <message>
        <location line="+90"/>
        <source>Error: A fatal internal error occurred, see debug.log for details</source>
        <translation>Chyba: Přihodila se závažná vnitřní chyba, podrobnosti viz v debug.log</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Pruning blockstore...</source>
        <translation>Prořezávám úložiště bloků...</translation>
    </message>
    <message>
        <location line="-155"/>
        <source>Distributed under the MIT software license, see the accompanying file COPYING or &lt;http://www.opensource.org/licenses/mit-license.php&gt;.</source>
        <translation>Šířen pod softwarovou licencí MIT, viz přiložený soubor COPYING nebo &lt;http://www.opensource.org/licenses/mit-license.php&gt;.</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>The block database contains a block which appears to be from the future. This may be due to your computer&apos;s date and time being set incorrectly. Only rebuild the block database if you are sure that your computer&apos;s date and time are correct</source>
        <translation>Databáze bloků obsahuje blok, který vypadá jako z budoucnosti, což může být kvůli špatně nastavenému datu a času na tvém počítači. Nech databázi bloků přestavět pouze v případě, že si jsi jistý, že máš na počítači správný datum a čas</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>This is a pre-release test build - use at your own risk - do not use for mining or merchant applications</source>
        <translation>Tohle je testovací verze – používej ji jen na vlastní riziko, ale rozhodně ji nepoužívej k těžbě nebo pro obchodní aplikace</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>WARNING: abnormally high number of blocks generated, %d blocks received in the last %d hours (%d expected)</source>
        <translation>UPOZORNĚNÍ: vygenerováno nezvykle mnoho bloků – přijato %d bloků jen za posledních %d hodin (očekáváno %d)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>WARNING: check your network connection, %d blocks received in the last %d hours (%d expected)</source>
        <translation>UPOZORNĚNÍ: zkontroluj své spojení do sítě – bylo přijato %d bloků za posledních %d hodin (očekáváno %d)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Warning: The network does not appear to fully agree! Some miners appear to be experiencing issues.</source>
        <translation>Upozornění: Síť podle všeho není v konzistentním stavu. Někteří těžaři jsou zřejmě v potížích.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Warning: We do not appear to fully agree with our peers! You may need to upgrade, or other nodes may need to upgrade.</source>
        <translation>Upozornění: Nesouhlasím zcela se svými protějšky! Možná potřebuji aktualizovat nebo ostatní uzly potřebují aktualizovat.</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Corrupted block database detected</source>
        <translation>Bylo zjištěno poškození databáze bloků</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Do you want to rebuild the block database now?</source>
        <translation>Chceš přestavět databázi bloků hned teď?</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error initializing block database</source>
        <translation>Chyba při zakládání databáze bloků</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error initializing wallet database environment %s!</source>
        <translation>Chyba při vytváření databázového prostředí %s pro peněženku!</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error opening block database</source>
        <translation>Chyba při otevírání databáze bloků</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Error: Disk space is low!</source>
        <translation>Problém: Na disku je málo místa!</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Failed to listen on any port. Use -listen=0 if you want this.</source>
        <translation>Nepodařilo se naslouchat na žádném portu. Použij -listen=0, pokud to byl tvůj záměr.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Importing...</source>
        <translation>Importuji...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Incorrect or no genesis block found. Wrong datadir for network?</source>
        <translation>Nemám žádný nebo jen špatný genesis blok. Není špatně nastavený datadir?</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Invalid -onion address: &apos;%s&apos;</source>
        <translation>Neplatná -onion adresa: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Not enough file descriptors available.</source>
        <translation>Je nedostatek deskriptorů souborů.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Prune cannot be configured with a negative value.</source>
        <translation>Prořezávání nemůže být zkonfigurováno s negativní hodnotou.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Prune mode is incompatible with -txindex.</source>
        <translation>Prořezávací režim není kompatibilní s -txindex.</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>User Agent comment (%s) contains unsafe characters.</source>
        <translation>Komentář u typu klienta (%s) obsahuje riskantní znaky.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Verifying blocks...</source>
        <translation>Ověřuji bloky...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Verifying wallet...</source>
        <translation>Kontroluji peněženku...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Waiting for Genesis Block...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Wallet %s resides outside data directory %s</source>
        <translation>Peněženka %s se nachází mimo datový adresář %s</translation>
    </message>
    <message>
        <location line="-166"/>
        <source>Error: Listening for incoming connections failed (listen returned error %s)</source>
        <translation>Chyba: Nelze naslouchat příchozí spojení (listen vrátil chybu %s)</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>The transaction amount is too small to send after the fee has been deducted</source>
        <translation>Částka v transakci po odečtení poplatku je příliš malá na odeslání</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>This product includes software developed by the OpenSSL Project for use in the OpenSSL Toolkit &lt;https://www.openssl.org/&gt; and cryptographic software written by Eric Young and UPnP software written by Thomas Bernard.</source>
        <translation>Tento produkt zahrnuje programy vyvinuté OpenSSL Projektem pro použití v OpenSSL Toolkitu &lt;https://www.openssl.org/&gt; a kryptografický program od Erika Younga a program UPnP od Thomase Bernarda.</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>You need to rebuild the database using -reindex to go back to unpruned mode.  This will redownload the entire blockchain</source>
        <translation>K návratu k neprořezávacímu režimu je potřeba přestavět databázi použitím -reindex.  Také se znovu stáhne celý řetězec bloků</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Activating best chain...</source>
        <translation>Aktivuji nejlepší řetězec...</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Cannot resolve -whitebind address: &apos;%s&apos;</source>
        <translation>Nemohu přeložit -whitebind adresu: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Information</source>
        <translation>Informace</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Invalid netmask specified in -whitelist: &apos;%s&apos;</source>
        <translation>Ve -whitelist byla zadána neplatná podsíť: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Need to specify a port with -whitebind: &apos;%s&apos;</source>
        <translation>V rámci -whitebind je třeba specifikovat i port: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Signing transaction failed</source>
        <translation>Nepodařilo se podepsat transakci</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Starting txindex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The transaction amount is too small to pay the fee</source>
        <translation>Částka v transakci je příliš malá na pokrytí poplatku</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This is experimental software.</source>
        <translation>Tohle je experimentální program.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction amount too small</source>
        <translation>Částka v transakci je příliš malá</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction amounts must be positive</source>
        <translation>Částky v transakci musí být kladné</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction has %d outputs. Maximum outputs allowed is %d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction of %d bytes is too large. Maximum allowed is %d bytes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction too large for fee policy</source>
        <translation>Transakce je na poplatkovou politiku příliš velká</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unable to bind to %s on this computer (bind returned error %s)</source>
        <translation>Nedaří se mi připojit na %s na tomhle počítači (operace bind vrátila chybu %s)</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Warning</source>
        <translation>Upozornění</translation>
    </message>
    <message>
        <location line="-39"/>
        <source>Loading addresses...</source>
        <translation>Načítám adresy...</translation>
    </message>
    <message>
        <location line="-5"/>
        <source>Invalid -proxy address: &apos;%s&apos;</source>
        <translation>Neplatná -proxy adresa: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Unknown network specified in -onlynet: &apos;%s&apos;</source>
        <translation>V -onlynet byla uvedena neznámá síť: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="-61"/>
        <source>Cannot resolve -bind address: &apos;%s&apos;</source>
        <translation>Nemohu přeložit -bind adresu: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cannot resolve -externalip address: &apos;%s&apos;</source>
        <translation>Nemohu přeložit -externalip adresu: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Loading block index...</source>
        <translation>Načítám index bloků...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading wallet...</source>
        <translation>Načítám peněženku...</translation>
    </message>
    <message>
        <location line="-36"/>
        <source>Cannot downgrade wallet</source>
        <translation>Nemohu převést peněženku do staršího formátu</translation>
    </message>
    <message>
        <location line="-125"/>
        <source>Nexa</source>
        <translation type="unfinished">Nexa</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The %s developers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bitcoin Bitcoin XT and Bitcoin Unlimited</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>-wallet.maxTxFee is set very high! Fees this large could be paid on a single transaction.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>-wallet.payTxFee is set very high! This is the transaction fee you will pay if you send a transaction.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cannot obtain a lock on data directory %s. %s is probably already running.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Could not locate RPC credentials. No authentication cookie could be found, and no rpcpassword is set in the configuration file (%s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Deployment configuration file &apos;%s&apos; contained invalid data - see debug.log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error loading %s: You can&apos;t enable HD on a already existing non-HD wallet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error reading %s! All keys read correctly, but transaction data or address book entries might be missing or incorrect.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Error reading from the coin database.
Details: %s

Do you want to reindex on the next restart?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Failed to listen on all P2P ports. Failing as requested by -bindallorfail.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Fee: %ld is larger than configured maximum allowed fee of : %ld.  To change, set &apos;wallet.maxTxFee&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Invalid amount for -wallet.maxTxFee=&lt;amount&gt;: &apos;%u&apos; (must be at least the minrelay fee of %s to prevent stuck transactions)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Invalid amount for -wallet.payTxFee=&lt;amount&gt;: &apos;%u&apos; (must be at least %s)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Please check that your computer&apos;s date and time are correct! If your clock is wrong, %s will not work properly.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Reducing -maxconnections from %d to %d because of file descriptor limitations (unix) or winsocket fd_set limitations (windows). If you are a windows user there is a hard upper limit of 1024 which cannot be changed by adjusting the node&apos;s configuration.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Total length of network version string with uacomments added exceeded the maximum length (%i) and have been truncated.  Reduce the number or size of uacomments to avoid truncation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Transaction has %d inputs and %d outputs. Maximum inputs allowed are %d and maximum outputs are %d</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Transaction has %d inputs. Maximum inputs allowed is %d. Try reducing inputs by transferring a smaller amount.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Wallet is not password protected. Your funds may be at risk! Goto &quot;Settings&quot; and then select &quot;Encrypt Wallet&quot; to create a password.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Warning: Could not open deployment configuration CSV file &apos;%s&apos; for reading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Warning: Unknown block versions being mined! It&apos;s possible unknown rules are in effect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Warning: Wallet file corrupt, data salvaged! Original %s saved as %s in %s; if your balance or transactions are incorrect you should restore from a backup.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>You are trying to use -wallet.auto but neither -spendzeroconfchange nor -wallet.instant is turned on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>You are trying to use wallet.fallbackFee which has been deprecated an no longer in  use - use wallet.payTxFee instead </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>You are trying to use wallet.minTxFee which has been deprecated an no longer in  use - use wallet.payTxFee instead </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>You can not send free transactions if you have configured a -relay.limitFreeRelay of zero</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>%s </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>%s corrupt, salvage failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>-maxtxpool must be at least %d MB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>-xthinbloomfiltersize must be at least %d Bytes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Cannot write default address</source>
        <translation>Nemohu napsat výchozí adresu</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>CommitTransaction failed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copyright (C) 2015-%i The Bitcoin Unlimited Developers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Deployment configuration file &apos;%s&apos; not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error loading %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error loading %s: Wallet corrupted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error loading %s: Wallet requires newer version of %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error loading %s: You can&apos;t disable HD on a already existing HD wallet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Initialization sanity check failed. %s is shutting down.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Insufficient funds or funds not confirmed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Keypool ran out, please call keypoolrefill first</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading Orphanpool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading TxPool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Loading banlist...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Opening Block database...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Opening Coins Cache database...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Opening UTXO database...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Portions Copyright (C) 2009-%i The Bitcoin Core Developers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Portions Copyright (C) 2014-%i The Bitcoin XT Developers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Reaccepting Wallet Transactions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Rescanning...</source>
        <translation>Přeskenovávám...</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Turn off auto consolidate and try sending again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unable to bind to %s on this computer. %s is probably already running.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unable to start RPC services. See debug log for details.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Upgrading block database...This could take a while.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Upgrading txindex database </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Upgrading txindex database...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Wallet needed to be rewritten: restart %s to complete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-61"/>
        <source>Done loading</source>
        <translation>Načítání dokončeno</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Error</source>
        <translation>Chyba</translation>
    </message>
</context>
</TS>
