<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>AddressBookPage</name>
    <message>
        <location filename="../forms/addressbookpage.ui" line="+30"/>
        <source>Right-click to edit address or label</source>
        <translation>鼠标右击编辑地址或标签</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Create a new address</source>
        <translation>创建新地址</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;New</source>
        <translation>新建(&amp;N)</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Copy the currently selected address to the system clipboard</source>
        <translation>复制当前选中的地址到系统剪贴板</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Copy</source>
        <translation>复制(&amp;C)</translation>
    </message>
    <message>
        <location line="+67"/>
        <source>C&amp;lose</source>
        <translation>关闭(&amp;l)</translation>
    </message>
    <message>
        <location filename="../addressbookpage.cpp" line="+90"/>
        <source>&amp;Copy Address</source>
        <translation>复制地址(&amp;C)</translation>
    </message>
    <message>
        <location filename="../forms/addressbookpage.ui" line="-53"/>
        <source>Delete the currently selected address from the list</source>
        <translation>从列表中删除选中的地址</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Export the data in the current tab to a file</source>
        <translation>导出当前分页里的数据到文件</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Export</source>
        <translation>导出(&amp;E)</translation>
    </message>
    <message>
        <location line="-30"/>
        <source>&amp;Delete</source>
        <translation>删除(&amp;D)</translation>
    </message>
    <message>
        <location filename="../addressbookpage.cpp" line="-39"/>
        <source>Choose the address to send coins to</source>
        <translation>选择发币地址</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Choose the address to receive coins with</source>
        <translation>选择收币地址</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>C&amp;hoose</source>
        <translation>选择(&amp;H)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Sending addresses</source>
        <translation>正在发送地址</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Receiving addresses</source>
        <translation>正在接收地址</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>These are your Nexa addresses for sending payments. Always check the amount and the receiving address before sending coins.</source>
        <translation>这是您用来付款的比特币地址。在付款前，请仔细核实付款金额和收款地址。</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>These are your Nexa addresses for receiving payments. It is recommended to use a new receiving address for each transaction.</source>
        <translation>这些都是您的比特币地址，可用于收款。建议对每笔交易都使用一个新的地址。</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Copy &amp;Label</source>
        <translation>复制标签(&amp;L)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Edit</source>
        <translation>编辑(&amp;E)</translation>
    </message>
    <message>
        <location line="+173"/>
        <source>Export Address List</source>
        <translation>导出地址列表</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Comma separated file (*.csv)</source>
        <translation>逗号分隔文件 (*.csv)</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Exporting Failed</source>
        <translation>导出失败</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to save the address list to %1. Please try again.</source>
        <translation>保存地址列表出现 %1错误。请重试。</translation>
    </message>
</context>
<context>
    <name>AddressTableModel</name>
    <message>
        <location filename="../addresstablemodel.cpp" line="+159"/>
        <source>Label</source>
        <translation>标签</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Address</source>
        <translation>地址</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>(no label)</source>
        <translation>(没有标签)</translation>
    </message>
</context>
<context>
    <name>AskPassphraseDialog</name>
    <message>
        <location filename="../forms/askpassphrasedialog.ui" line="+26"/>
        <source>Passphrase Dialog</source>
        <translation>密码对话框</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Enter passphrase</source>
        <translation>输入密码</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>New passphrase</source>
        <translation>新密码</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Repeat new passphrase</source>
        <translation>重复新密码</translation>
    </message>
    <message>
        <location filename="../askpassphrasedialog.cpp" line="+47"/>
        <source>Encrypt wallet</source>
        <translation>加密钱包</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>This operation needs your wallet passphrase to unlock the wallet.</source>
        <translation>此操作需要您首先使用密码解锁该钱包。</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unlock wallet</source>
        <translation>解锁钱包</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>This operation needs your wallet passphrase to decrypt the wallet.</source>
        <translation>该操作需要您首先使用密码解密钱包。</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Decrypt wallet</source>
        <translation>解密钱包</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Change passphrase</source>
        <translation>更改密码</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Confirm wallet encryption</source>
        <translation>确认加密钱包</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Warning: If you encrypt your wallet and lose your passphrase, you will &lt;b&gt;LOSE ALL OF YOUR NEXA COINS&lt;/b&gt;!</source>
        <translation>警告：如果您加密了您的钱包，但是忘记了密码，你将会&lt;b&gt;丢失所有的比特币&lt;/b&gt;！</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Are you sure you wish to encrypt your wallet?</source>
        <translation>您确定需要为钱包加密吗？</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>IMPORTANT: Any previous backups you have made of your wallet file should be replaced with the newly generated, encrypted wallet file. For security reasons, previous backups of the unencrypted wallet file will become useless as soon as you start using the new, encrypted wallet.</source>
        <translation>重要提示：您以前备份的钱包文件应该替换成最新生成的加密钱包文件（重新备份）。从安全性上考虑，您以前备份的未加密的钱包文件，在您使用新的加密钱包后将无效，请重新备份。</translation>
    </message>
    <message>
        <location line="+104"/>
        <location line="+29"/>
        <source>Warning: The Caps Lock key is on!</source>
        <translation>警告：大写锁定键处于打开状态！</translation>
    </message>
    <message>
        <location line="-140"/>
        <location line="+61"/>
        <source>Wallet encrypted</source>
        <translation>钱包已加密</translation>
    </message>
    <message>
        <location line="-138"/>
        <source>Enter the new passphrase to the wallet.&lt;br/&gt;Please use a passphrase of &lt;b&gt;ten or more random characters&lt;/b&gt;, or &lt;b&gt;eight or more words&lt;/b&gt;.</source>
        <translation>请输入新的钱包密码. &lt;br/&gt;密码须包含&lt;b&gt;10个以上随机字符&lt;/b&gt;,或&lt;b&gt;8个以上单词&lt;/b&gt;.</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Enter the old passphrase and new passphrase to the wallet.</source>
        <translation>请输入钱包的旧密码与新密码。</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>%1 will close now to finish the encryption process. Remember that encrypting your wallet cannot fully protect your coins from being stolen by malware infecting your computer.</source>
        <translation>%1 現在將關閉以完成加密過程。 請記住，加密你的錢包並不能完全保護你的硬幣不被感染你計算機的惡意軟件竊取。</translation>
    </message>
    <message>
        <location line="+14"/>
        <location line="+8"/>
        <location line="+42"/>
        <location line="+6"/>
        <source>Wallet encryption failed</source>
        <translation>钱包加密失败</translation>
    </message>
    <message>
        <location line="-55"/>
        <source>Wallet encryption failed due to an internal error. Your wallet was not encrypted.</source>
        <translation>由于一个本地错误，加密钱包的操作已经失败。您的钱包没能被加密。</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+48"/>
        <source>The supplied passphrases do not match.</source>
        <translation>密码不匹配。</translation>
    </message>
    <message>
        <location line="-36"/>
        <source>Wallet unlock failed</source>
        <translation>钱包解锁失败</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+11"/>
        <location line="+19"/>
        <source>The passphrase entered for the wallet decryption was incorrect.</source>
        <translation>用于解密钱包的密码不正确。</translation>
    </message>
    <message>
        <location line="-20"/>
        <source>Wallet decryption failed</source>
        <translation>钱包解密失败</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Wallet passphrase was successfully changed.</source>
        <translation>修改钱包密码成功。</translation>
    </message>
</context>
<context>
    <name>BanTableModel</name>
    <message>
        <location filename="../bantablemodel.cpp" line="+86"/>
        <source>IP/Netmask</source>
        <translation>IP/网络掩码</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Banned Until</source>
        <translation>禁止至</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>User Agent</source>
        <translation>用户代理</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Ban Reason</source>
        <translation>禁令原因</translation>
    </message>
</context>
<context>
    <name>BitcoinGUI</name>
    <message>
        <location filename="../nexagui.cpp" line="+318"/>
        <source>Sign &amp;message...</source>
        <translation>消息签名(&amp;M)...</translation>
    </message>
    <message>
        <location line="+411"/>
        <source>Synchronizing with network...</source>
        <translation>正在与网络同步...</translation>
    </message>
    <message>
        <location line="-500"/>
        <source>&amp;Overview</source>
        <translation>概况(&amp;O)</translation>
    </message>
    <message>
        <location line="-136"/>
        <source>Node</source>
        <translation>节点</translation>
    </message>
    <message>
        <location line="+137"/>
        <source>Show general overview of wallet</source>
        <translation>显示钱包概况</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>&amp;Transactions</source>
        <translation>交易记录(&amp;T)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Browse transaction history</source>
        <translation>查看交易历史</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>E&amp;xit</source>
        <translation>退出(&amp;X)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Quit application</source>
        <translation>退出程序</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;About %1</source>
        <translation>&amp;關於 %1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show information about %1</source>
        <translation>顯示有關 %1 的信息</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>About &amp;Qt</source>
        <translation>关于Qt(&amp;Q)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show information about Qt</source>
        <translation>显示 Qt 相关信息</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Options...</source>
        <translation>选项(&amp;O)...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Modify configuration options for %1</source>
        <translation>修改 %1 的配置選項</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Unlimited...</source>
        <translation>&amp;Unlimited...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Modify Bitcoin Unlimited Options</source>
        <translation>修改比特幣無限選項</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Encrypt Wallet...</source>
        <translation>加密钱包(&amp;E)...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Backup Wallet...</source>
        <translation>备份钱包(&amp;B)...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Change Passphrase...</source>
        <translation>更改密码(&amp;C)...</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Sending addresses...</source>
        <translation>正在发送地址(&amp;S)...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Receiving addresses...</source>
        <translation>正在接收地址(&amp;R)...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Open &amp;URI...</source>
        <translation>打开 &amp;URI...</translation>
    </message>
    <message>
        <location line="+397"/>
        <source>Importing blocks from disk...</source>
        <translation>正在从磁盘导入数据块...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Reindexing blocks on disk...</source>
        <translation>正在为数据块重建索引...</translation>
    </message>
    <message>
        <location line="-498"/>
        <source>Send coins to a Nexa address</source>
        <translation>向一个比特币地址发送比特币</translation>
    </message>
    <message>
        <location line="+77"/>
        <source>Backup wallet to another location</source>
        <translation>备份钱包到其他文件夹</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Change the passphrase used for wallet encryption</source>
        <translation>更改钱包加密口令</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Debug window</source>
        <translation>调试窗口(&amp;D)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Open debugging and diagnostic console</source>
        <translation>打开调试和诊断控制台</translation>
    </message>
    <message>
        <location line="-4"/>
        <source>&amp;Verify message...</source>
        <translation>验证消息(&amp;V)...</translation>
    </message>
    <message>
        <location line="+495"/>
        <source>Nexa</source>
        <translation>Nexa</translation>
    </message>
    <message>
        <location line="-726"/>
        <source>Wallet</source>
        <translation>钱包</translation>
    </message>
    <message>
        <location line="+147"/>
        <source>&amp;Send</source>
        <translation>发送(&amp;S)</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>&amp;Receive</source>
        <translation>接收(&amp;R)</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>&amp;Show / Hide</source>
        <translation>显示 / 隐藏(&amp;S)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show or hide the main Window</source>
        <translation>显示或隐藏主窗口</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Encrypt the private keys that belong to your wallet</source>
        <translation>对钱包中的私钥加密</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Sign messages with your Nexa addresses to prove you own them</source>
        <translation>用比特币地址关联的私钥为消息签名，以证明您拥有这个比特币地址</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Verify messages to ensure they were signed with specified Nexa addresses</source>
        <translation>校验消息，确保该消息是由指定的比特币地址所有者签名的</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>&amp;File</source>
        <translation>文件(&amp;F)</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>&amp;Settings</source>
        <translation>设置(&amp;S)</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Help</source>
        <translation>帮助(&amp;H)</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Tabs toolbar</source>
        <translation>分页工具栏</translation>
    </message>
    <message>
        <location line="-173"/>
        <source>Request payments (generates QR codes and %1: URIs)</source>
        <translation>请求支付 (生成二维码和 %1 URI)</translation>
    </message>
    <message>
        <location line="+80"/>
        <source>Show the list of used sending addresses and labels</source>
        <translation>显示用过的发送地址和标签的列表</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show the list of used receiving addresses and labels</source>
        <translation>显示用过的接收地址和标签的列表</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Command-line options</source>
        <translation>命令行选项(&amp;C)</translation>
    </message>
    <message numerus="yes">
        <location line="+366"/>
        <source>%n active connection(s) to Nexa network</source>
        <translation>
            <numerusform>%n 个到比特币网络的活动连接</numerusform>
        </translation>
    </message>
    <message>
        <location line="+34"/>
        <source>No block source available...</source>
        <translation>沒有可用的区块来源...</translation>
    </message>
    <message numerus="yes">
        <location line="+9"/>
        <source>Processed %n block(s) of transaction history.</source>
        <translation>
            <numerusform>已处理 %n 个交易历史数据块。</numerusform>
        </translation>
    </message>
    <message>
        <location line="+25"/>
        <source>%1 behind</source>
        <translation>落后 %1</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Last received block was generated %1 ago.</source>
        <translation>最新收到的区块产生于 %1。</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Transactions after this will not yet be visible.</source>
        <translation>此後的交易將不可見。</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Information</source>
        <translation>信息</translation>
    </message>
    <message>
        <location line="-85"/>
        <source>Up to date</source>
        <translation>已是最新</translation>
    </message>
    <message>
        <location line="-417"/>
        <source>Open a %1: URI or payment request</source>
        <translation>打開 %1: URI 或支付請求</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Show the %1 help message to get a list with possible Nexa command-line options</source>
        <translation>顯示 %1 幫助消息以獲取包含可能的 Nexa 命令行選項的列表</translation>
    </message>
    <message>
        <location line="+185"/>
        <source>%1 client</source>
        <translation>%1 個客戶</translation>
    </message>
    <message>
        <location line="+251"/>
        <source>Catching up...</source>
        <translation>更新中...</translation>
    </message>
    <message>
        <location line="+163"/>
        <source>Date: %1
</source>
        <translation>日期: %1
</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Amount: %1
</source>
        <translation>金额: %1
</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Type: %1
</source>
        <translation>类型: %1
</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Label: %1
</source>
        <translation>标签: %1
</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Address: %1
</source>
        <translation>地址: %1
</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sent transaction</source>
        <translation>发送交易</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Incoming transaction</source>
        <translation>流入交易</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>HD key generation is &lt;b&gt;enabled&lt;/b&gt;</source>
        <translation>HD 密鑰生成已&lt;b&gt;啟用&lt;/b&gt;</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>HD key generation is &lt;b&gt;disabled&lt;/b&gt;</source>
        <translation>HD 密鑰生成被&lt;b&gt;禁用&lt;/b&gt;</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Wallet is &lt;b&gt;encrypted&lt;/b&gt; and currently &lt;b&gt;unlocked&lt;/b&gt;</source>
        <translation>钱包已被&lt;b&gt;加密&lt;/b&gt;，当前为&lt;b&gt;解锁&lt;/b&gt;状态</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Wallet is &lt;b&gt;encrypted&lt;/b&gt; and currently &lt;b&gt;locked&lt;/b&gt;</source>
        <translation>钱包已被&lt;b&gt;加密&lt;/b&gt;，当前为&lt;b&gt;锁定&lt;/b&gt;状态</translation>
    </message>
</context>
<context>
    <name>CoinControlDialog</name>
    <message>
        <location filename="../forms/coincontroldialog.ui" line="+14"/>
        <source>Coin Selection</source>
        <translation>币源选择(Coin Selection)</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Quantity:</source>
        <translation>总量：</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Bytes:</source>
        <translation>字节：</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Amount:</source>
        <translation>金额：</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Priority:</source>
        <translation>优先级：</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Fee:</source>
        <translation>费用：</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Dust:</source>
        <translation>小额：</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>After Fee:</source>
        <translation>加上交易费用后:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Change:</source>
        <translation>变更 :</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>(un)select max inputs</source>
        <translation>(不)全选</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Tree mode</source>
        <translation>树状模式</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>List mode</source>
        <translation>列表模式</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Amount</source>
        <translation>金额</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Received with label</source>
        <translation>按标签收款</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Received with address</source>
        <translation>按地址收款</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Date</source>
        <translation>日期</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Confirmations</source>
        <translation>确认</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirmed</source>
        <translation>已确认</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Priority</source>
        <translation>优先级</translation>
    </message>
    <message>
        <location filename="../coincontroldialog.cpp" line="+45"/>
        <source>Copy address</source>
        <translation>复制地址</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy label</source>
        <translation>复制标签</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+29"/>
        <source>Copy amount</source>
        <translation>复制金额</translation>
    </message>
    <message>
        <location line="-27"/>
        <source>Copy transaction ID</source>
        <translation>复制交易编号</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Lock unspent</source>
        <translation>锁定未花费</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unlock unspent</source>
        <translation>解锁未花费</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Copy quantity</source>
        <translation>复制金额</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Copy fee</source>
        <translation>复制交易费</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy after fee</source>
        <translation>复制含交易费的金额</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy bytes</source>
        <translation>复制字节</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy priority</source>
        <translation>复制优先级</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy dust</source>
        <translation>复制小额</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy change</source>
        <translation>复制零钱</translation>
    </message>
    <message>
        <location line="+352"/>
        <source>highest</source>
        <translation>最高</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>higher</source>
        <translation>更高</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>high</source>
        <translation>高</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>medium-high</source>
        <translation>中高</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>medium</source>
        <translation>中等</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>low-medium</source>
        <translation>中低</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>low</source>
        <translation>低</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>lower</source>
        <translation>更低</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>lowest</source>
        <translation>最低</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>(%1 locked)</source>
        <translation>(%1 锁定)</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>none</source>
        <translation>无</translation>
    </message>
    <message>
        <location line="+167"/>
        <source>This label turns red if the transaction size is greater than 1000 bytes.</source>
        <translation>如果交易规模大于 1000 字节，此标签将变为红色。</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>This label turns red if the priority is smaller than &quot;medium&quot;.</source>
        <translation>如果优先级小于“中等”，此标签将变为红色。</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>This label turns red if any recipient receives an amount smaller than %1.</source>
        <translation>如果任何接收人收到的金额小于 %1，此标签将变为红色。</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Can vary +/- %1 satoshi(s) per input.</source>
        <translation>可能会有 正负 %1 聪(satoshi)的偏差</translation>
    </message>
    <message>
        <location line="-41"/>
        <source>yes</source>
        <translation>是</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>no</source>
        <translation>否</translation>
    </message>
    <message>
        <location line="+17"/>
        <location line="+8"/>
        <source>This means a fee of at least %1 per kB is required.</source>
        <translation>这意味着将对交易收取 %1/千字节 的交易费。</translation>
    </message>
    <message>
        <location line="-5"/>
        <source>Can vary +/- 1 byte per input.</source>
        <translation>每笔输入可能会有 正负1字节的偏差。</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Transactions with higher priority are more likely to get included into a block.</source>
        <translation>交易的优先级越高，被矿工收入数据块的速度也越快。</translation>
    </message>
    <message>
        <location line="+66"/>
        <location line="+64"/>
        <source>(no label)</source>
        <translation>(没有标签)</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>change from %1 (%2)</source>
        <translation>来自%1的零钱 (%2)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>(change)</source>
        <translation>(零钱)</translation>
    </message>
</context>
<context>
    <name>EditAddressDialog</name>
    <message>
        <location filename="../forms/editaddressdialog.ui" line="+14"/>
        <source>Edit Address</source>
        <translation>编辑地址</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Label</source>
        <translation>标签(&amp;L)</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>The label associated with this address list entry</source>
        <translation>与此地址相关的标签项</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>The address associated with this address list entry. This can only be modified for sending addresses.</source>
        <translation>该地址已与地址列表中的条目关联，只能被发送地址修改。</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>&amp;Address</source>
        <translation>地址(&amp;A)</translation>
    </message>
    <message>
        <location filename="../editaddressdialog.cpp" line="+25"/>
        <source>New receiving address</source>
        <translation>新建接收地址</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>New sending address</source>
        <translation>新建发送地址</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Edit receiving address</source>
        <translation>编辑接收地址</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Edit sending address</source>
        <translation>编辑发送地址</translation>
    </message>
    <message>
        <location line="+66"/>
        <source>The entered address &quot;%1&quot; is already in the address book.</source>
        <translation>输入的地址“%1”已经存在于地址簿中。</translation>
    </message>
    <message>
        <location line="-5"/>
        <source>The entered address &quot;%1&quot; is not a valid Nexa address.</source>
        <translation>您输入的“%1”不是有效的比特币地址。</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Could not unlock wallet.</source>
        <translation>无法解锁钱包</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>New key generation failed.</source>
        <translation>新的密钥生成失败。</translation>
    </message>
</context>
<context>
    <name>FreespaceChecker</name>
    <message>
        <location filename="../intro.cpp" line="+75"/>
        <source>A new data directory will be created.</source>
        <translation>一个新的数据目录将被创建。</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Directory already exists. Add %1 if you intend to create a new directory here.</source>
        <translation>目录已存在。如果您打算在这里创建一个新目录，添加 %1。</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Path already exists, and is not a directory.</source>
        <translation>路径已存在，并且不是一个目录。</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Cannot create data directory here.</source>
        <translation>无法在此创建数据目录。</translation>
    </message>
</context>
<context>
    <name>HelpMessageDialog</name>
    <message>
        <location filename="../utilitydialog.cpp" line="+39"/>
        <source>version</source>
        <translation>版本</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+2"/>
        <source>(%1-bit)</source>
        <translation>(%1 位)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>About %1</source>
        <translation>關於 %1</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Command-line options</source>
        <translation>命令行选项</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Usage:</source>
        <translation>使用：</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>command-line options</source>
        <translation>命令行选项</translation>
    </message>
</context>
<context>
    <name>Intro</name>
    <message>
        <location filename="../forms/intro.ui" line="+14"/>
        <source>Welcome</source>
        <translation>欢迎</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Welcome to %1.</source>
        <translation>歡迎來到 %1。</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>As this is the first time the program is launched, you can choose where %1 will store its data.</source>
        <translation>由於這是第一次啟動該程序，您可以選擇 %1 存儲其數據的位置。</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>%1 will download and store a copy of the Nexa block chain. At least %2GB of data will be stored in this directory, and it will grow over time. The wallet will also be stored in this directory.</source>
        <translation>%1 將下載並存儲 Nexa 區塊鏈的副本。 至少 %2GB 的數據將存儲在此目錄中，並且會隨著時間的推移而增長。 錢包也會存放在這個目錄下。</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Use the default data directory</source>
        <translation>使用默认的数据目录</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Use a custom data directory:</source>
        <translation>使用自定义的数据目录：</translation>
    </message>
    <message>
        <location filename="../intro.cpp" line="+81"/>
        <source>Error: Specified data directory &quot;%1&quot; cannot be created.</source>
        <translation>错误：无法创建 指定的数据目录 &quot;%1&quot;</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message numerus="yes">
        <location line="+11"/>
        <source>%n GB of free space available</source>
        <translation>
            <numerusform>有 %n GB 空闲空间</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+3"/>
        <source>(of %n GB needed)</source>
        <translation>
            <numerusform>(需要%n GB空间)</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>LessThanValidator</name>
    <message>
        <location filename="../unlimiteddialog.cpp" line="+486"/>
        <source>Upstream traffic shaping parameters can&apos;t be blank</source>
        <translation>上行流量整形參數不能為空</translation>
    </message>
</context>
<context>
    <name>ModalOverlay</name>
    <message>
        <location filename="../forms/modaloverlay.ui" line="+14"/>
        <source>Form</source>
        <translation>表单</translation>
    </message>
    <message>
        <location line="+119"/>
        <source>The displayed information may be out of date. Your wallet automatically synchronizes with the Nexa network after a connection is established, but this process has not completed yet. This means that recent transactions will not be visible, and the balance will not be up-to-date until this process has completed.</source>
        <translation>顯示的信息可能已過時。 建立連接後，您的錢包會自動與 Nexa 網絡同步，但此過程尚未完成。 這意味著最近的交易將不可見，並且在此過程完成之前餘額不會是最新的。</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Spending coins may not be possible during that phase!</source>
        <translation>在那個階段可能無法花費硬幣！</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Amount of blocks left</source>
        <translation>剩餘塊數</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+26"/>
        <source>unknown...</source>
        <translation>未知...</translation>
    </message>
    <message>
        <location line="-13"/>
        <source>Last block time</source>
        <translation>最後出塊時間</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Progress</source>
        <translation>進步</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>~</source>
        <translation>~</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Progress increase per Hour</source>
        <translation>每小時進度增加</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+20"/>
        <source>calculating...</source>
        <translation>計算...</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Estimated time left until synced</source>
        <translation>預計同步前的剩餘時間</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Hide</source>
        <translation>隐藏</translation>
    </message>
    <message>
        <location filename="../modaloverlay.cpp" line="+140"/>
        <source>Unknown. Reindexing (%1)...</source>
        <translation>未知。 正在重新編制索引（%1）...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unknown. Reindexing...</source>
        <translation>未知。 正在重新索引...</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unknown. Syncing Headers (%1)...</source>
        <translation>未知。 正在同步標頭 (%1)...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unknown. Syncing Headers...</source>
        <translation>未知。 同步標題...</translation>
    </message>
</context>
<context>
    <name>OpenURIDialog</name>
    <message>
        <location filename="../forms/openuridialog.ui" line="+14"/>
        <source>Open URI</source>
        <translation>打开 URI</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Open payment request from URI or file</source>
        <translation>打开来自URI或文件的付款请求</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>URI:</source>
        <translation>URI:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Select payment request file</source>
        <translation>选择付款请求文件</translation>
    </message>
    <message>
        <location filename="../openuridialog.cpp" line="+40"/>
        <source>Select payment request file to open</source>
        <translation>选择需要打开的付款请求文件</translation>
    </message>
</context>
<context>
    <name>OptionsDialog</name>
    <message>
        <location filename="../forms/optionsdialog.ui" line="+14"/>
        <source>Options</source>
        <translation>选项</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>&amp;Main</source>
        <translation>主要(&amp;M)</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Number of script &amp;verification threads</source>
        <translation>脚本验证线程数(&amp;V)</translation>
    </message>
    <message>
        <location line="+195"/>
        <source>Allow incoming connections</source>
        <translation>允许流入连接</translation>
    </message>
    <message>
        <location line="+44"/>
        <location line="+187"/>
        <source>IP address of the proxy (e.g. IPv4: 127.0.0.1 / IPv6: ::1)</source>
        <translation>代理的 IP 地址 (例如 IPv4: 127.0.0.1 / IPv6: ::1)</translation>
    </message>
    <message>
        <location line="+84"/>
        <source>Minimize instead of exit the application when the window is closed. When this option is enabled, the application will be closed only after selecting Exit in the menu.</source>
        <translation>窗口被关闭时最小化而不是退出应用程序。当此选项启用时，应用程序只会在菜单中选择退出时退出。</translation>
    </message>
    <message>
        <location line="+80"/>
        <location line="+13"/>
        <source>Third party URLs (e.g. a block explorer) that appear in the transactions tab as context menu items. %s in the URL is replaced by transaction hash. Multiple URLs are separated by vertical bar |.</source>
        <translation>出现在交易的选项卡的上下文菜单项的第三方网址 (例如：区块链接查询) 。 %s的URL被替换为交易哈希。多个的URL需要竖线 | 分隔。</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Third party transaction URLs</source>
        <translation>第三方交易网址</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Active command-line options that override above options:</source>
        <translation>有效的命令行参数覆盖上述选项:</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Reset all client options to default.</source>
        <translation>恢复客户端的缺省设置</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Reset Options</source>
        <translation>恢复缺省设置(&amp;R)</translation>
    </message>
    <message>
        <location line="-504"/>
        <source>&amp;Network</source>
        <translation>网络(&amp;N)</translation>
    </message>
    <message>
        <location line="-145"/>
        <source>(0 = auto, &lt;0 = leave that many cores free)</source>
        <translation>(0 = 自动, &lt;0 = 预留此数量的核心)</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>W&amp;allet</source>
        <translation>钱包(&amp;A)</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Expert</source>
        <translation>专家</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Enable coin &amp;control features</source>
        <translation>启动货币控制功能(&amp;C)</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>If you disable the spending of unconfirmed change, the change from a transaction cannot be used until that transaction has at least one confirmation. This also affects how your balance is computed.</source>
        <translation>如果禁用未确认的零钱，则零钱至少需要1个确认才能使用。同时账户余额计算会受到影响。</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Spend unconfirmed change</source>
        <translation>使用未经确认的零钱(&amp;S)</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When Instant Transactions is enabled you can spend unconfirmed transactions immediately.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;啟用即時交易後，您可以立即使用未確認的交易。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Instant Transactions</source>
        <translation>&amp;即時交易</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When creating and sending transactions, auto consolidate will, if required, automatically create a chain of transactions which have inputs no greater than the consensus input limit.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;在創建和發送交易時，如果需要，自動合併將自動創建一個輸入不大於共識輸入限制的交易鏈。&lt;/p&gt;&lt;/body &gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Auto Consolidate</source>
        <translation>自動合併</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>Automatically open the Nexa client port on the router. This only works when your router supports UPnP and it is enabled.</source>
        <translation>自动在路由器中打开比特币端口。只有当您的路由器开启了 UPnP 选项时此功能才有效。</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Map port using &amp;UPnP</source>
        <translation>使用 &amp;UPnP 映射端口</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Accept connections from outside.</source>
        <translation>接受來自外部的連接。</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Connect to the Nexa network through a SOCKS5 proxy.</source>
        <translation>通过 SOCKS5 代理连接比特币网络。</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Connect through SOCKS5 proxy (default proxy):</source>
        <translation>通过 SO&amp;CKS5 代理连接(默认代理)：</translation>
    </message>
    <message>
        <location line="+9"/>
        <location line="+187"/>
        <source>Proxy &amp;IP:</source>
        <translation>代理服务器 &amp;IP：</translation>
    </message>
    <message>
        <location line="-155"/>
        <location line="+187"/>
        <source>&amp;Port:</source>
        <translation>端口(&amp;P)：</translation>
    </message>
    <message>
        <location line="-162"/>
        <location line="+187"/>
        <source>Port of the proxy (e.g. 9050)</source>
        <translation>代理端口（例如 9050）</translation>
    </message>
    <message>
        <location line="-163"/>
        <source>Used for reaching peers via:</source>
        <translation>连接到同伴的方式：</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>IPv4</source>
        <translation>IPv4</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>IPv6</source>
        <translation>IPv6</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Tor</source>
        <translation>Tor</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Connect to the Nexa network through a separate SOCKS5 proxy for Tor hidden services.</source>
        <translation>在Tor匿名网络下通过不同的SOCKS5代理连接比特币网络</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Use separate SOCKS5 proxy to reach peers via Tor hidden services:</source>
        <translation>通过Tor隐藏服务连接节点时 使用不同的SOCKS5代理</translation>
    </message>
    <message>
        <location line="+102"/>
        <source>&amp;Window</source>
        <translation>窗口(&amp;W)</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Show only a tray icon after minimizing the window.</source>
        <translation>最小化窗口后仅显示托盘图标</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Minimize to the tray instead of the taskbar</source>
        <translation>最小化到托盘(&amp;M)</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>M&amp;inimize on close</source>
        <translation>单击关闭按钮最小化(&amp;I)</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>&amp;Display</source>
        <translation>显示(&amp;D)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>User Interface &amp;language:</source>
        <translation>用户界面语言(&amp;L)：</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>The user interface language can be set here. This setting will take effect after restarting %1.</source>
        <translation>用戶界面語言可以在這裡設置。 此設置將在重新啟動 %1 後生效。</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Unit to show amounts in:</source>
        <translation>比特币金额单位(&amp;U)：</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Choose the default subdivision unit to show in the interface and when sending coins.</source>
        <translation>选择比特币单位。</translation>
    </message>
    <message>
        <location line="-502"/>
        <source>Whether to show coin control features or not.</source>
        <translation>是否需要交易源地址控制功能。</translation>
    </message>
    <message>
        <location line="-121"/>
        <source>Automatically start %1 after logging in to the system.</source>
        <translation>登錄系統後自動啟動 %1。</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Start %1 on system login</source>
        <translation>在系統登錄時啟動 %1 (&amp;S)</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Automatically initiate a, one time only, full database reindex on the next startup.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;在下次啟動時自動啟動一次完整的數據庫重建索引。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Reindex on startup</source>
        <translation>啟動時重建索引</translation>
    </message>
    <message>
        <location line="+333"/>
        <location line="+23"/>
        <location line="+23"/>
        <source>Shows if the supplied default SOCKS5 proxy is used to reach peers via this network type.</source>
        <translation>顯示提供的默認 SOCKS5 代理是否用於通過此網絡類型到達對等點。</translation>
    </message>
    <message>
        <location line="+387"/>
        <source>&amp;OK</source>
        <translation>确定(&amp;O)</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Cancel</source>
        <translation>取消(&amp;C)</translation>
    </message>
    <message>
        <location filename="../optionsdialog.cpp" line="+118"/>
        <source>default</source>
        <translation>默认</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>none</source>
        <translation>无</translation>
    </message>
    <message>
        <location line="+73"/>
        <source>Confirm options reset</source>
        <translation>确认恢复缺省设置</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+27"/>
        <source>Client restart required to activate changes.</source>
        <translation>更改生效需要重启客户端。</translation>
    </message>
    <message>
        <location line="-26"/>
        <source>Client will be shut down. Do you want to proceed?</source>
        <translation>客户端即将关闭，您想继续吗？</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>This change would require a client restart.</source>
        <translation>此更改需要重启客户端。</translation>
    </message>
    <message>
        <location line="+82"/>
        <source>The supplied proxy address is invalid.</source>
        <translation>提供的代理服务器地址无效。</translation>
    </message>
</context>
<context>
    <name>OverviewPage</name>
    <message>
        <location filename="../forms/overviewpage.ui" line="+14"/>
        <source>Form</source>
        <translation>表单</translation>
    </message>
    <message>
        <location line="+59"/>
        <location line="+386"/>
        <source>The displayed information may be out of date. Your wallet automatically synchronizes with the Nexa network after a connection is established, but this process has not completed yet.</source>
        <translation>现在显示的消息可能是过期的. 在连接上比特币网络节点后，您的钱包将自动与网络同步，但是这个过程还没有完成。</translation>
    </message>
    <message>
        <location line="-139"/>
        <source>Watch-only:</source>
        <translation>查看-只有:</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Available:</source>
        <translation>可使用的余额：</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Your current spendable balance</source>
        <translation>您当前可使用的余额</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Pending:</source>
        <translation>等待中的余额：</translation>
    </message>
    <message>
        <location line="-236"/>
        <source>Total of transactions that have yet to be confirmed, and do not yet count toward the spendable balance</source>
        <translation>尚未确认的交易总额，未计入当前余额</translation>
    </message>
    <message>
        <location line="+112"/>
        <source>Immature:</source>
        <translation>未成熟的：</translation>
    </message>
    <message>
        <location line="-29"/>
        <source>Mined balance that has not yet matured</source>
        <translation>尚未成熟的挖矿收入余额</translation>
    </message>
    <message>
        <location line="-177"/>
        <source>Balances</source>
        <translation>余额</translation>
    </message>
    <message>
        <location line="+161"/>
        <source>Total:</source>
        <translation>总额：</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>Your current total balance</source>
        <translation>您当前的总余额</translation>
    </message>
    <message>
        <location line="+92"/>
        <source>Your current balance in watch-only addresses</source>
        <translation>您当前 观察地址(watch-only address)的余额</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Spendable:</source>
        <translation>可使用：</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Recent transactions</source>
        <translation>最近交易记录</translation>
    </message>
    <message>
        <location line="-317"/>
        <source>Unconfirmed transactions to watch-only addresses</source>
        <translation>观察地址(watch-only address)的未确认交易记录</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>Mined balance in watch-only addresses that has not yet matured</source>
        <translation>观察地址(watch-only address)中尚未成熟(matured)的挖矿收入余额：</translation>
    </message>
    <message>
        <location line="+128"/>
        <source>Current total balance in watch-only addresses</source>
        <translation>观察地址(watch-only address)中的当前总余额</translation>
    </message>
</context>
<context>
    <name>PaymentServer</name>
    <message>
        <location filename="../paymentserver.cpp" line="+470"/>
        <location line="+13"/>
        <location line="+10"/>
        <source>URI handling</source>
        <translation>URI 处理</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Invalid payment address %1</source>
        <translation>无效的付款地址 %1</translation>
    </message>
    <message>
        <location line="+113"/>
        <location line="+11"/>
        <location line="+35"/>
        <location line="+12"/>
        <location line="+21"/>
        <location line="+98"/>
        <source>Payment request rejected</source>
        <translation>支付请求被拒绝</translation>
    </message>
    <message>
        <location line="-177"/>
        <source>Payment request network doesn&apos;t match client network.</source>
        <translation>付款请求所在的网络与当前客户端所在的网络不匹配。</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Payment request is not initialized.</source>
        <translation>支付请求未成形。</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Requested payment amount of %1 is too small (considered dust).</source>
        <translation>请求支付的金额 %1 太小（就像尘埃）。</translation>
    </message>
    <message>
        <location line="-288"/>
        <location line="+240"/>
        <location line="+47"/>
        <location line="+126"/>
        <location line="+15"/>
        <location line="+15"/>
        <source>Payment request error</source>
        <translation>支付请求出错</translation>
    </message>
    <message>
        <location line="-443"/>
        <source>Cannot start click-to-pay handler</source>
        <translation>無法啟動點擊支付處理程序</translation>
    </message>
    <message>
        <location line="+95"/>
        <source>Payment request fetch URL is invalid: %1</source>
        <translation>付款请求URI链接非法: %1</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>URI cannot be parsed! This can be caused by an invalid Nexa address or malformed URI parameters.</source>
        <translation>URI无法解析！原因可能是比特币地址不正确，或者URI参数错误。</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Payment request file handling</source>
        <translation>付款请求文件处理</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Payment request file cannot be read! This can be caused by an invalid payment request file.</source>
        <translation>付款请求文件无法读取！可能是付款请求文件不合格。</translation>
    </message>
    <message>
        <location line="+76"/>
        <source>Payment request expired.</source>
        <translation>支付请求已过期。</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Unverified payment requests to custom payment scripts are unsupported.</source>
        <translation>不支持到自定义付款脚本的未验证付款请求。</translation>
    </message>
    <message>
        <location line="+11"/>
        <location line="+21"/>
        <source>Invalid payment request.</source>
        <translation>无效的支付请求。</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Refund from %1</source>
        <translation>退款来自 %1</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Payment request %1 is too large (%2 bytes, allowed %3 bytes).</source>
        <translation>支付请求 %1 太大 (%2 字节。只允许 %3 字节)。</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Error communicating with %1: %2</source>
        <translation>%1: %2 通讯出错</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Payment request cannot be parsed!</source>
        <translation>无法解析 付款请求！</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Bad response from server %1</source>
        <translation>来自 %1 服务器的错误响应</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Payment acknowledged</source>
        <translation>支付已到账</translation>
    </message>
    <message>
        <location line="-8"/>
        <source>Network request error</source>
        <translation>网络请求出错</translation>
    </message>
</context>
<context>
    <name>PeerTableModel</name>
    <message>
        <location filename="../peertablemodel.cpp" line="+106"/>
        <source>User Agent</source>
        <translation>用户代理</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Node/Service</source>
        <translation>节点/服务</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Ping Time</source>
        <translation>Ping 时间</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../nexaunits.cpp" line="+187"/>
        <source>Amount</source>
        <translation>金额</translation>
    </message>
    <message>
        <location filename="../guiutil.cpp" line="+135"/>
        <source>Enter a NEXA address (e.g. %1)</source>
        <translation>輸入 NEXA 地址（例如 %1）</translation>
    </message>
    <message>
        <location line="+832"/>
        <source>%1 d</source>
        <translation>%1 天</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 h</source>
        <translation>%1 小时</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 m</source>
        <translation>%1 分钟</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+68"/>
        <source>%1 s</source>
        <translation>%1 秒</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>None</source>
        <translation>无</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>N/A</source>
        <translation>不可用</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>%1 ms</source>
        <translation>%1 毫秒</translation>
    </message>
    <message numerus="yes">
        <location line="+19"/>
        <source>%n seconds(s)</source>
        <translation>
            <numerusform>%n 秒</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%n minutes(s)</source>
        <translation>
            <numerusform>%n 分鐘</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%n hour(s)</source>
        <translation>
            <numerusform>%n 小时</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%n day(s)</source>
        <translation>
            <numerusform>%n 天</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <location line="+8"/>
        <source>%n week(s)</source>
        <translation>
            <numerusform>%n 周</numerusform>
        </translation>
    </message>
    <message>
        <location line="-2"/>
        <source>%1 and %2</source>
        <translation>%1 和 %2</translation>
    </message>
    <message numerus="yes">
        <location line="+1"/>
        <source>%n year(s)</source>
        <translation>
            <numerusform>%n 年</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>QRImageWidget</name>
    <message>
        <location filename="../receiverequestdialog.cpp" line="+36"/>
        <source>&amp;Save Image...</source>
        <translation>保存图片(&amp;S)...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Copy Image</source>
        <translation>复制图片(&amp;C)</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Save QR Code</source>
        <translation>保存二维码</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>PNG Image (*.png)</source>
        <translation>PNG图片(*.png)</translation>
    </message>
</context>
<context>
    <name>RPCConsole</name>
    <message>
        <location filename="../forms/debugwindow.ui" line="+239"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+17"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+29"/>
        <location line="+16"/>
        <location line="+19"/>
        <location line="+16"/>
        <location line="+16"/>
        <location line="+1753"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+26"/>
        <location line="+26"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+26"/>
        <location line="+23"/>
        <source>N/A</source>
        <translation>不可用</translation>
    </message>
    <message>
        <location line="-2715"/>
        <source>Client version</source>
        <translation>客户端版本</translation>
    </message>
    <message>
        <location line="-6"/>
        <source>&amp;Information</source>
        <translation>信息</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Debug window</source>
        <translation>调试窗口</translation>
    </message>
    <message>
        <location line="+620"/>
        <source>General</source>
        <translation>常规</translation>
    </message>
    <message>
        <location line="-587"/>
        <source>Using BerkeleyDB version</source>
        <translation>使用的 BerkeleyDB 版本</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Startup time</source>
        <translation>启动时间</translation>
    </message>
    <message>
        <location line="+474"/>
        <source>Network</source>
        <translation>网络</translation>
    </message>
    <message>
        <location line="-467"/>
        <source>Name</source>
        <translation>姓名</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Number of connections</source>
        <translation>连接数</translation>
    </message>
    <message>
        <location line="+415"/>
        <source>Block chain</source>
        <translation>数据链</translation>
    </message>
    <message>
        <location line="-408"/>
        <source>Current number of blocks</source>
        <translation>当前数据块数量</translation>
    </message>
    <message>
        <location line="+884"/>
        <location line="+1694"/>
        <source>Received</source>
        <translation>收到</translation>
    </message>
    <message>
        <location line="-1614"/>
        <location line="+1591"/>
        <source>Sent</source>
        <translation>发送</translation>
    </message>
    <message>
        <location line="-417"/>
        <source>&amp;Peers</source>
        <translation>同伴(&amp;P)</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>Banned peers</source>
        <translation>节点黑名单</translation>
    </message>
    <message>
        <location line="+57"/>
        <location filename="../rpcconsole.cpp" line="+311"/>
        <location line="+855"/>
        <source>Select a peer to view detailed information.</source>
        <translation>选择节点查看详细信息。</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Whitelisted</source>
        <translation>白名单</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Direction</source>
        <translation>方向</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Version</source>
        <translation>版本</translation>
    </message>
    <message>
        <location line="+75"/>
        <source>Starting Block</source>
        <translation>正在启动数据块</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Synced Headers</source>
        <translation>同步区块头</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Synced Blocks</source>
        <translation>同步区块链</translation>
    </message>
    <message>
        <location line="-2488"/>
        <location line="+2390"/>
        <source>User Agent</source>
        <translation>用户代理</translation>
    </message>
    <message>
        <location line="-2370"/>
        <source>Datadir</source>
        <translation>數據目錄</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Last block time (time since)</source>
        <translation>上次出塊時間（時間自）</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Last block size</source>
        <translation>最後一個塊大小</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Transactions in Tx pool</source>
        <translation>Tx池中的交易</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Transactions in Orphan pool</source>
        <translation>孤兒池中的交易</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Messages in CAPD pool</source>
        <translation>CAPD 池中的消息</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Tx pool - usage</source>
        <translation>Tx 池 - 使用</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Tx pool - txns per second</source>
        <translation>Tx 池 - 每秒交易數</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>XThin (Totals)</source>
        <translation>XThin（總計）</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>XThin (24-Hour Averages)</source>
        <translation>XThin（24 小時平均值）</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Compact (Totals)</source>
        <translation>緊湊型（總計）</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Compact (24-Hour Averages)</source>
        <translation>緊湊型（24 小時平均值）</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Graphene (Totals)</source>
        <translation>石墨烯（總計）</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Graphene (24-Hour Averages)</source>
        <translation>石墨烯（24 小時平均值）</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Open the %1 debug log file from the current data directory. This can take a few seconds for large log files.</source>
        <translation>從當前數據目錄打開 %1 調試日誌文件。 對於大型日誌文件，這可能需要幾秒鐘。</translation>
    </message>
    <message>
        <location line="+130"/>
        <source>Block Propagation</source>
        <translation>塊傳播</translation>
    </message>
    <message>
        <location line="+94"/>
        <source>Transaction Pools</source>
        <translation>交易池</translation>
    </message>
    <message>
        <location line="+246"/>
        <source>Decrease font size</source>
        <translation>減小字體大小</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Increase font size</source>
        <translation>增加字體大小</translation>
    </message>
    <message>
        <location line="+380"/>
        <source>&amp;Transaction Rate</source>
        <translation>&amp;交易率</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>Instantaneous Rate (1s)</source>
        <translation>瞬時速率（1s）</translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+536"/>
        <source>Peak</source>
        <translation>頂峰</translation>
    </message>
    <message>
        <location line="-463"/>
        <location line="+257"/>
        <location line="+279"/>
        <location line="+257"/>
        <source>Runtime</source>
        <translation>運行</translation>
    </message>
    <message>
        <location line="-713"/>
        <location line="+257"/>
        <location line="+279"/>
        <location line="+257"/>
        <source>24-Hours</source>
        <translation>24小時</translation>
    </message>
    <message>
        <location line="-713"/>
        <location line="+257"/>
        <location line="+279"/>
        <location line="+257"/>
        <source>Displayed</source>
        <translation>顯示</translation>
    </message>
    <message>
        <location line="-769"/>
        <location line="+536"/>
        <source>Average</source>
        <translation>平均的</translation>
    </message>
    <message>
        <location line="-265"/>
        <source>Smoothed Rate (60s)</source>
        <translation>平滑速率（60 秒）</translation>
    </message>
    <message>
        <location line="+769"/>
        <source>Services</source>
        <translation>服务</translation>
    </message>
    <message>
        <location line="+95"/>
        <source>Ban Score</source>
        <translation>禁止得分</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Connection Time</source>
        <translation>连接时间</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Last Send</source>
        <translation>最后发送</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Last Receive</source>
        <translation>最后接收</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Ping Time</source>
        <translation>Ping 时间</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>The duration of a currently outstanding ping.</source>
        <translation>當前未完成的 ping 的持續時間。</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Ping Wait</source>
        <translation>Ping等待</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Time Offset</source>
        <translation>时间偏移</translation>
    </message>
    <message>
        <location line="-2524"/>
        <source>&amp;Open</source>
        <translation>打开(&amp;O)</translation>
    </message>
    <message>
        <location line="+431"/>
        <source>&amp;Console</source>
        <translation>控制台(&amp;C)</translation>
    </message>
    <message>
        <location line="+195"/>
        <source>&amp;Network Traffic</source>
        <translation>网络流量(&amp;N)</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>&amp;Clear</source>
        <translation>清除(&amp;C)</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Totals</source>
        <translation>总数</translation>
    </message>
    <message>
        <location filename="../rpcconsole.cpp" line="-530"/>
        <source>In:</source>
        <translation>输入：</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Out:</source>
        <translation>输出：</translation>
    </message>
    <message>
        <location filename="../forms/debugwindow.ui" line="-704"/>
        <source>Debug log file</source>
        <translation>调试日志文件</translation>
    </message>
    <message>
        <location line="+541"/>
        <source>Clear console</source>
        <translation>清空控制台</translation>
    </message>
    <message>
        <location filename="../rpcconsole.cpp" line="-207"/>
        <source>&amp;Disconnect Node</source>
        <translation>(&amp;D)断开节点连接</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+1"/>
        <location line="+1"/>
        <location line="+1"/>
        <source>Ban Node for</source>
        <translation>禁止节点连接时长：</translation>
    </message>
    <message>
        <location line="-3"/>
        <source>1 &amp;hour</source>
        <translation>1 小时(&amp;H)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>1 &amp;day</source>
        <translation>1 天(&amp;D)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>1 &amp;week</source>
        <translation>1 周(&amp;W)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>1 &amp;year</source>
        <translation>1 年(&amp;Y)</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>&amp;Unban Node</source>
        <translation>(&amp;U)允许节点连接</translation>
    </message>
    <message>
        <location line="+118"/>
        <source>Welcome to the %1 RPC console.</source>
        <translation>歡迎使用 %1 RPC 控制台。</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Use up and down arrows to navigate history, and &lt;b&gt;Ctrl-L&lt;/b&gt; to clear screen.</source>
        <translation>使用上下方向键浏览历史,  &lt;b&gt;Ctrl-L&lt;/b&gt;清除屏幕。</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Type &lt;b&gt;help&lt;/b&gt; for an overview of available commands.</source>
        <translation>使用 &lt;b&gt;help&lt;/b&gt; 命令显示帮助信息。</translation>
    </message>
    <message>
        <location line="+125"/>
        <location line="+1"/>
        <location line="+28"/>
        <location line="+1"/>
        <location line="+28"/>
        <location line="+1"/>
        <source>Disabled</source>
        <translation>殘疾人</translation>
    </message>
    <message>
        <location line="+110"/>
        <source>%1 B</source>
        <translation>%1 字节</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 KB</source>
        <translation>%1 KB</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 MB</source>
        <translation>%1 MB</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 GB</source>
        <translation>%1 GB</translation>
    </message>
    <message>
        <location line="+117"/>
        <source>(node id: %1)</source>
        <translation>(节点ID: %1)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>via %1</source>
        <translation>通过 %1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+2"/>
        <source>never</source>
        <translation>从未</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Inbound</source>
        <translation>传入</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Outbound</source>
        <translation>传出</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Yes</source>
        <translation>是</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>No</source>
        <translation>否</translation>
    </message>
    <message>
        <location line="+13"/>
        <location line="+6"/>
        <source>Unknown</source>
        <translation>未知</translation>
    </message>
</context>
<context>
    <name>ReceiveCoinsDialog</name>
    <message>
        <location filename="../forms/receivecoinsdialog.ui" line="+110"/>
        <source>&amp;Amount:</source>
        <translation>总额(&amp;A)：</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>&amp;Label:</source>
        <translation>标签(&amp;L)：</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>&amp;Message:</source>
        <translation>消息(&amp;M)：</translation>
    </message>
    <message>
        <location line="-27"/>
        <source>Reuse one of the previously used receiving addresses. Reusing addresses has security and privacy issues. Do not use this unless re-generating a payment request made before.</source>
        <translation>重复使用以前用过的接收地址。重用地址有安全和隐私方面的隐患。除非是为重复生成同一项支付请求，否则请不要这样做。</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>R&amp;euse an existing receiving address (not recommended)</source>
        <translation>&amp;重用现有的接收地址（不推荐）</translation>
    </message>
    <message>
        <location line="+21"/>
        <location line="+23"/>
        <source>An optional message to attach to the payment request, which will be displayed when the request is opened. Note: The message will not be sent with the payment over the Nexa network.</source>
        <translation>可在付款请求上备注一条信息，在打开付款请求时可以看到。注意：该消息不是通过比特币网络传送。</translation>
    </message>
    <message>
        <location line="-63"/>
        <location line="+56"/>
        <source>An optional label to associate with the new receiving address.</source>
        <translation>可为新建的收款地址添加一个标签。</translation>
    </message>
    <message>
        <location line="-23"/>
        <source>Use this form to request payments. All fields are &lt;b&gt;optional&lt;/b&gt;.</source>
        <translation>使用此表单要求付款。所有字段都是&lt;b&gt;可选&lt;/b&gt;。</translation>
    </message>
    <message>
        <location line="-56"/>
        <location line="+7"/>
        <source>An optional amount to request. Leave this empty or zero to not request a specific amount.</source>
        <translation>可选的请求金额。留空或填零为不要求具体金额。</translation>
    </message>
    <message>
        <location line="-45"/>
        <source>Clear all fields of the form.</source>
        <translation>清除此表单的所有字段。</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Clear</source>
        <translation>清除</translation>
    </message>
    <message>
        <location line="+177"/>
        <source>Requested payments history</source>
        <translation>请求付款的历史</translation>
    </message>
    <message>
        <location line="-197"/>
        <source>&amp;Request payment</source>
        <translation>请求付款(&amp;R)</translation>
    </message>
    <message>
        <location line="+148"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Coin freezing locks coins to make them temporarily unspendable. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;代幣凍結鎖定代幣，使它們暫時無法使用。 &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Coin &amp;Freeze</source>
        <translation>投幣&amp;凍結</translation>
    </message>
    <message>
        <location line="+71"/>
        <source>Show the selected request (does the same as double clicking an entry)</source>
        <translation>显示选中的请求 (双击也可以显示)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show</source>
        <translation>显示</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Remove the selected entries from the list</source>
        <translation>从列表中移除选中的条目</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Remove</source>
        <translation>移除</translation>
    </message>
    <message>
        <location filename="../receivecoinsdialog.cpp" line="+53"/>
        <source>Copy URI</source>
        <translation>複製 URI</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy label</source>
        <translation>复制标签</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy message</source>
        <translation>复制消息</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation>复制金额</translation>
    </message>
</context>
<context>
    <name>ReceiveFreezeDialog</name>
    <message>
        <location filename="../forms/receivefreezedialog.ui" line="+14"/>
        <source>Coin Freeze</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+24"/>
        <source>WARNING! Freezing coins means they will be UNSPENDABLE until the release date or block specified below.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Freeze until block :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+19"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Specify the block when coins are released from the freeze. Coins are UNSPENDABLE until the freeze block.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+99"/>
        <source>-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-57"/>
        <source>OR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Freeze until date and time :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Specify the future date and time when coins are released from the freeze. Coins are UNSPENDABLE until after the freeze date and time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+98"/>
        <source>&amp;Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Alt+R</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+25"/>
        <source>&amp;OK</source>
        <translation type="unfinished">确定(&amp;O)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Alt+O</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ReceiveRequestDialog</name>
    <message>
        <location filename="../forms/receiverequestdialog.ui" line="+29"/>
        <source>QR Code</source>
        <translation>二维码</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Copy &amp;URI</source>
        <translation>复制 URI(&amp;U)</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Copy &amp;Address</source>
        <translation>复制地址(&amp;A)</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Save Image...</source>
        <translation>保存图片(&amp;S)...</translation>
    </message>
    <message>
        <location filename="../receiverequestdialog.cpp" line="+78"/>
        <source>Request payment to %1</source>
        <translation>请求付款到 %1</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Payment information</source>
        <translation>付款信息</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>URI</source>
        <translation>URI</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Address</source>
        <translation>地址</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Amount</source>
        <translation>金额</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Label</source>
        <translation>标签</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Message</source>
        <translation>消息</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Freeze until</source>
        <translation>凍結直到</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Resulting URI too long, try to reduce the text for label / message.</source>
        <translation>URI 太长，请试着精简标签或消息文本。</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Error encoding URI into QR Code.</source>
        <translation>将 URI 转为二维码失败。</translation>
    </message>
</context>
<context>
    <name>RecentRequestsTableModel</name>
    <message>
        <location filename="../recentrequeststablemodel.cpp" line="+30"/>
        <source>Date</source>
        <translation>日期</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Label</source>
        <translation>标签</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Message</source>
        <translation>消息</translation>
    </message>
    <message>
        <location line="+97"/>
        <source>Amount</source>
        <translation>金额</translation>
    </message>
    <message>
        <location line="-57"/>
        <source>(no label)</source>
        <translation>(没有标签)</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>(no message)</source>
        <translation>(无消息)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>(no amount)</source>
        <translation>(无金额)</translation>
    </message>
</context>
<context>
    <name>SendCoinsDialog</name>
    <message>
        <location filename="../forms/sendcoinsdialog.ui" line="+14"/>
        <location filename="../sendcoinsdialog.cpp" line="+627"/>
        <source>Send Coins</source>
        <translation>发送比特币</translation>
    </message>
    <message>
        <location line="+76"/>
        <source>Coin Control Features</source>
        <translation>交易源地址控制功能</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Inputs...</source>
        <translation>输入...</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>automatically selected</source>
        <translation>自动选择</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Insufficient funds!</source>
        <translation>存款不足！</translation>
    </message>
    <message>
        <location line="+89"/>
        <source>Quantity:</source>
        <translation>总量：</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Bytes:</source>
        <translation>字节：</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Amount:</source>
        <translation>金额：</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Priority:</source>
        <translation>优先级：</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Fee:</source>
        <translation>费用：</translation>
    </message>
    <message>
        <location line="+80"/>
        <source>After Fee:</source>
        <translation>加上交易费用后:</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Change:</source>
        <translation>变更 :</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>If this is activated, but the change address is empty or invalid, change will be sent to a newly generated address.</source>
        <translation>如果激活该选项，但是零钱地址用光或者非法，将会新生成零钱地址，转入零钱。</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Custom change address</source>
        <translation>自定义零钱地址</translation>
    </message>
    <message>
        <location line="+206"/>
        <source>Transaction Fee:</source>
        <translation>交易费用:</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Choose...</source>
        <translation>选择...</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>collapse fee-settings</source>
        <translation>收起  费用设置</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>per kilobyte</source>
        <translation>每kb</translation>
    </message>
    <message>
        <location line="-3"/>
        <location line="+16"/>
        <source>If the custom fee is set to 1000 satoshis and the transaction is only 250 bytes, then &quot;per kilobyte&quot; only pays 250 satoshis in fee, while &quot;total at least&quot; pays 1000 satoshis. For transactions bigger than a kilobyte both pay by kilobyte.</source>
        <translation>如果自定义交易费设置为 1000聪而交易大小只有250字节，则“每千字节&quot; 模式只支付250聪交易费， 而&quot;最少&quot;模式则支付1000聪。 大于1000字节的交易按每千字节付费。</translation>
    </message>
    <message>
        <location line="-64"/>
        <source>Hide</source>
        <translation>隐藏</translation>
    </message>
    <message>
        <location line="+67"/>
        <source>total at least</source>
        <translation>最小额</translation>
    </message>
    <message>
        <location line="+30"/>
        <location line="+13"/>
        <source>Paying only the minimum fee is just fine as long as there is less transaction volume than space in the blocks. But be aware that this can end up in a never confirming transaction once there is more demand for nexa transactions than the network can process.</source>
        <translation>交易量小时只支付最小交易费是可以的。但是请注意，当交易量大到超出网络可处理时您的交易可能永远无法确认。</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>(read the tooltip)</source>
        <translation>(请注意提示信息)</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Recommended:</source>
        <translation>推荐：</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Custom:</source>
        <translation>自定义：</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>(Smart fee not initialized yet. This usually takes a few blocks...)</source>
        <translation>(智能交易费用 尚未初始化。 需要再下载一些数据块...)</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Confirmation time:</source>
        <translation>确认时间：</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>normal</source>
        <translation>一般</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>fast</source>
        <translation>快速</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Send as zero-fee transaction if possible</source>
        <translation>发送时尽可能 不支付交易费用</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>(confirmation may take longer)</source>
        <translation>(确认时间更长)</translation>
    </message>
    <message>
        <location line="+110"/>
        <source>Send to multiple recipients at once</source>
        <translation>一次发送给多个接收者</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Add &amp;Recipient</source>
        <translation>添加收款人(&amp;R)</translation>
    </message>
    <message>
        <location line="-20"/>
        <source>Clear all fields of the form.</source>
        <translation>清除此表单的所有字段。</translation>
    </message>
    <message>
        <location line="-858"/>
        <source>Dust:</source>
        <translation>小额：</translation>
    </message>
    <message>
        <location line="+861"/>
        <source>Clear &amp;All</source>
        <translation>清除所有(&amp;A)</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>Balance:</source>
        <translation>余额：</translation>
    </message>
    <message>
        <location line="-84"/>
        <source>Confirm the send action</source>
        <translation>确认发送货币</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>S&amp;end</source>
        <translation>发送(&amp;E)</translation>
    </message>
    <message>
        <location filename="../sendcoinsdialog.cpp" line="-231"/>
        <source>Confirm send coins</source>
        <translation>确认发送货币</translation>
    </message>
    <message>
        <location line="-59"/>
        <location line="+5"/>
        <location line="+5"/>
        <location line="+4"/>
        <source>%1 to %2</source>
        <translation>%1 到 %2</translation>
    </message>
    <message>
        <location line="-283"/>
        <source>Copy quantity</source>
        <translation>复制金额</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation>复制金额</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy fee</source>
        <translation>复制交易费</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy after fee</source>
        <translation>复制含交易费的金额</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy bytes</source>
        <translation>复制字节</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy priority</source>
        <translation>复制优先级</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Copy change</source>
        <translation>复制零钱</translation>
    </message>
    <message>
        <location line="+247"/>
        <source>&lt;b&gt;Public label:&lt;/b&gt; %1</source>
        <translation>&lt;b&gt;公共標籤：&lt;/b&gt; %1</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>&lt;br&gt;&lt;br&gt;&lt;b&gt;WARNING!!! DESTINATION IS A FREEZE ADDRESS&lt;br&gt;UNSPENDABLE UNTIL&lt;/b&gt; %1 &lt;br&gt;*************************************************&lt;br&gt;</source>
        <translation>&lt;br&gt;&lt;br&gt;&lt;b&gt;警告！！！ 目的地是一個凍結地址&lt;br&gt;直到&lt;/b&gt; %1 &lt;br&gt;********************************* *********************&lt;br&gt;</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Total Amount %1</source>
        <translation>总金额 %1</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>or</source>
        <translation>或</translation>
    </message>
    <message>
        <location line="+190"/>
        <source>The amount to pay must be larger than 0.</source>
        <translation>支付金额必须大于0。</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The amount exceeds your balance.</source>
        <translation>金额超出您的账上余额。</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The total exceeds your balance when the %1 transaction fee is included.</source>
        <translation>计入 %1 交易费后的金额超出您的账上余额。</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Transaction creation failed!</source>
        <translation>交易创建失败！</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>The transaction was rejected! This might happen if some of the coins in your wallet were already spent, such as if you used a copy of wallet.dat and coins were spent in the copy but not marked as spent here.</source>
        <translation>交易被拒絕！ 如果您錢包中的一些硬幣已經用完，則可能會發生這種情況，例如，如果您使用了 wallet.dat 的副本，並且硬幣在副本中花費但未在此處標記為已花費。</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>A fee higher than %1 is considered an absurdly high fee.</source>
        <translation>超过 %1 的交易费被认为是荒谬的高费率。</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Payment request expired.</source>
        <translation>支付请求已过期。</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Public Label exeeds limit of </source>
        <translation>公共標籤超出限制 </translation>
    </message>
    <message>
        <location line="+105"/>
        <source>Pay only the required fee of %1</source>
        <translation>只支付必要费用 %1</translation>
    </message>
    <message>
        <location line="-139"/>
        <source>The recipient address is not valid. Please recheck.</source>
        <translation>接收人地址无效。请重新检查。</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Duplicate address found: addresses should only be used once each.</source>
        <translation>发现重复地址：每个地址应该只使用一次。</translation>
    </message>
    <message>
        <location line="+255"/>
        <source>Warning: Invalid Nexa address</source>
        <translation>警告：无效的比特币地址</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Confirm custom change address</source>
        <translation>確認自定義更改地址</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The address you selected for change is not part of this wallet. Any or all funds in your wallet may be sent to this address. Are you sure?</source>
        <translation>您選擇的更改地址不屬於此錢包。 您錢包中的任何或所有資金都可以發送到該地址。 你確定嗎？</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>(no label)</source>
        <translation>(没有标签)</translation>
    </message>
    <message>
        <location line="-28"/>
        <source>Warning: Unknown change address</source>
        <translation>警告：未知的更改地址</translation>
    </message>
    <message>
        <location line="-781"/>
        <source>Copy dust</source>
        <translation>复制小额</translation>
    </message>
    <message>
        <location line="+292"/>
        <source>Are you sure you want to send?</source>
        <translation>您确定要发出吗？</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>added as transaction fee</source>
        <translation>已添加交易费</translation>
    </message>
</context>
<context>
    <name>SendCoinsEntry</name>
    <message>
        <location filename="../forms/sendcoinsentry.ui" line="+82"/>
        <location line="+650"/>
        <location line="+533"/>
        <source>A&amp;mount:</source>
        <translation>金额(&amp;M)</translation>
    </message>
    <message>
        <location line="-1170"/>
        <source>Pay &amp;To:</source>
        <translation>付给(&amp;T)：</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Choose previously used address</source>
        <translation>选择以前用过的地址</translation>
    </message>
    <message>
        <location line="-106"/>
        <source>This is a normal payment.</source>
        <translation>这是笔正常的支付。</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Private Description:</source>
        <translation>私人說明：</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Private &amp;Label:</source>
        <translation>私人標籤：</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>The Nexa address to send the payment to</source>
        <translation>付款目的地址</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Paste address from clipboard</source>
        <translation>从剪贴板粘贴地址</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Alt+P</source>
        <translation>Alt+P</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+526"/>
        <location line="+533"/>
        <source>Remove this entry</source>
        <translation>移除此项</translation>
    </message>
    <message>
        <location line="-1037"/>
        <source>A message that was attached to the coin: URI which will be stored with the transaction for your reference. Note: This message will not be sent over the Nexa network.</source>
        <translation>附加到硬幣的消息：URI 將與交易一起存儲供您參考。 注意：此消息不會通過 Nexa 網絡發送。</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Enter a private label for this address to add it to the list of used addresses</source>
        <translation>輸入此地址的私有標籤以將其添加到已用地址列表</translation>
    </message>
    <message>
        <location line="+11"/>
        <location filename="../sendcoinsentry.cpp" line="+43"/>
        <source>Enter a public label for this transaction</source>
        <translation>輸入此交易的公共標籤</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Public Label:</source>
        <translation>公共標籤：</translation>
    </message>
    <message>
        <location line="-180"/>
        <source>The fee will be deducted from the amount being sent. The recipient will receive less coins than you enter in the amount field. If multiple recipients are selected, the fee is split equally.</source>
        <translation>交易费将从发送总额中扣除。接收人将收到比您在金额框中输入的更少的比特币。如果选中了多个收件人，交易费平分。</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>S&amp;ubtract fee from amount</source>
        <translation>从金额中减去交易费(&amp;U)</translation>
    </message>
    <message>
        <location line="+616"/>
        <source>This is an unauthenticated payment request.</source>
        <translation>这是一个未经验证的支付请求。</translation>
    </message>
    <message>
        <location line="+529"/>
        <source>This is an authenticated payment request.</source>
        <translation>这是一个已经验证的支付请求。</translation>
    </message>
    <message>
        <location filename="../sendcoinsentry.cpp" line="-12"/>
        <source>A message that was attached to the %1 URI which will be stored with the transaction for your reference. Note: This message will not be sent over the Nexa network.</source>
        <translation>%1URI 附带的备注信息，将会和交易一起存储，备查。 注意：该消息不会通过比特币网络传输。</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Enter a private label for this address to add it to your address book</source>
        <translation>輸入此地址的私人標籤以將其添加到您的地址簿</translation>
    </message>
    <message>
        <location filename="../forms/sendcoinsentry.ui" line="-514"/>
        <location line="+529"/>
        <source>Pay To:</source>
        <translation>支付给:</translation>
    </message>
    <message>
        <location line="-495"/>
        <location line="+533"/>
        <source>Memo:</source>
        <translation>便条：</translation>
    </message>
</context>
<context>
    <name>ShutdownWindow</name>
    <message>
        <location filename="../utilitydialog.cpp" line="+76"/>
        <source>%1 is shutting down...</source>
        <translation>%1 正在關閉...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Do not shut down the computer until this window disappears.</source>
        <translation>在此窗口消失前不要关闭计算机。</translation>
    </message>
</context>
<context>
    <name>SignVerifyMessageDialog</name>
    <message>
        <location filename="../forms/signverifymessagedialog.ui" line="+14"/>
        <source>Signatures - Sign / Verify a Message</source>
        <translation>签名 - 为消息签名/验证签名消息</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Sign Message</source>
        <translation>签名消息(&amp;S)</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>You can sign messages/agreements with your addresses to prove you can receive coins sent to them. Be careful not to sign anything vague or random, as phishing attacks may try to trick you into signing your identity over to them. Only sign fully-detailed statements you agree to.</source>
        <translation>您可以用你的地址对消息/协议进行签名，以证明您可以接收发送到该地址的比特币。注意不要对任何模棱两可或者随机的消息进行签名，以免遭受钓鱼式攻击。请确保消息内容准确的表达了您的真实意愿。</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>The Nexa address to sign the message with</source>
        <translation>用来对消息签名的地址</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+210"/>
        <source>Choose previously used address</source>
        <translation>选择以前用过的地址</translation>
    </message>
    <message>
        <location line="-200"/>
        <location line="+210"/>
        <source>Alt+A</source>
        <translation>Alt+A</translation>
    </message>
    <message>
        <location line="-200"/>
        <source>Paste address from clipboard</source>
        <translation>从剪贴板粘贴地址</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Alt+P</source>
        <translation>Alt+P</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Enter the message you want to sign here</source>
        <translation>请输入您要发送的签名消息</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Signature</source>
        <translation>签名</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Copy the current signature to the system clipboard</source>
        <translation>复制当前签名至剪切板</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Sign the message to prove you own this Nexa address</source>
        <translation>签名消息，证明这个地址属于您。</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Sign &amp;Message</source>
        <translation>消息签名(&amp;M)</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Reset all sign message fields</source>
        <translation>清空所有签名消息栏</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+143"/>
        <source>Clear &amp;All</source>
        <translation>清除所有(&amp;A)</translation>
    </message>
    <message>
        <location line="-84"/>
        <source>&amp;Verify Message</source>
        <translation>验证消息(&amp;V)</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Enter the receiver&apos;s address, message (ensure you copy line breaks, spaces, tabs, etc. exactly) and signature below to verify the message. Be careful not to read more into the signature than what is in the signed message itself, to avoid being tricked by a man-in-the-middle attack. Note that this only proves the signing party receives with the address, it cannot prove sendership of any transaction!</source>
        <translation>请在下面输入接收者地址、消息（确保换行符、空格符、制表符等完全相同）和签名以验证消息。请仔细核对签名信息，以提防中间人攻击。请注意，这只是证明接收方签名的地址，它不能证明任何交易！</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>The Nexa address the message was signed with</source>
        <translation>消息使用的签名地址</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Verify the message to ensure it was signed with the specified Nexa address</source>
        <translation>验证消息，确保消息是由指定的比特币地址签名过的。</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Verify &amp;Message</source>
        <translation>验证消息签名(&amp;M)</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Reset all verify message fields</source>
        <translation>清空所有验证消息栏</translation>
    </message>
    <message>
        <location filename="../signverifymessagedialog.cpp" line="+40"/>
        <source>Click &quot;Sign Message&quot; to generate signature</source>
        <translation>单击“签名消息“产生签名。</translation>
    </message>
    <message>
        <location line="+68"/>
        <source>Wallet unlock was cancelled.</source>
        <translation>钱包解锁动作取消。</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Message signed.</source>
        <translation>消息已签名。</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Message verified.</source>
        <translation>消息验证成功。</translation>
    </message>
</context>
<context>
    <name>SplashScreen</name>
    <message>
        <location filename="../networkstyle.cpp" line="+19"/>
        <source>[testnet]</source>
        <translation>[测试网络]</translation>
    </message>
</context>
<context>
    <name>TrafficGraphWidget</name>
    <message>
        <location filename="../trafficgraphwidget.cpp" line="+74"/>
        <source>KB/s</source>
        <translation>KB/s</translation>
    </message>
</context>
<context>
    <name>TransactionDesc</name>
    <message>
        <location filename="../transactiondesc.cpp" line="+34"/>
        <source>Open until %1</source>
        <translation>至 %1 个数据块时开启</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>conflicted</source>
        <translation>发现冲突</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1/offline</source>
        <translation>%1 / 离线</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1/unconfirmed</source>
        <translation>%1/未确认</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 confirmations</source>
        <translation>%1 已确认</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Status</source>
        <translation>状态</translation>
    </message>
    <message numerus="yes">
        <location line="+7"/>
        <source>, broadcast through %n node(s)</source>
        <translation>
            <numerusform>, 通过 %n 个节点广播</numerusform>
        </translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Date</source>
        <translation>日期</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Source</source>
        <translation>源</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Generated</source>
        <translation>生成</translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+13"/>
        <location line="+128"/>
        <source>From</source>
        <translation>来自</translation>
    </message>
    <message>
        <location line="-148"/>
        <location line="+21"/>
        <location line="+35"/>
        <location line="+114"/>
        <source>To</source>
        <translation>到</translation>
    </message>
    <message>
        <location line="-215"/>
        <source>double spent</source>
        <translation>雙花</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>abandoned</source>
        <translation>棄</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>change address</source>
        <translation>更換地址</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>own address</source>
        <translation>自己的地址</translation>
    </message>
    <message>
        <location line="+0"/>
        <location line="+119"/>
        <source>watch-only</source>
        <translation>只看</translation>
    </message>
    <message>
        <location line="-116"/>
        <location line="+28"/>
        <location line="+113"/>
        <source>label</source>
        <translation>标签</translation>
    </message>
    <message>
        <location line="-122"/>
        <location line="+63"/>
        <location line="+48"/>
        <location line="+71"/>
        <source>Public label:</source>
        <translation>公共標籤：</translation>
    </message>
    <message>
        <location line="-167"/>
        <source>Freeze until</source>
        <translation>凍結直到</translation>
    </message>
    <message>
        <location line="+18"/>
        <location line="+41"/>
        <location line="+63"/>
        <location line="+47"/>
        <location line="+74"/>
        <source>Credit</source>
        <translation>收入</translation>
    </message>
    <message numerus="yes">
        <location line="-222"/>
        <source>matures in %n more block(s)</source>
        <translation>
            <numerusform>在 %n 個區塊後成熟</numerusform>
        </translation>
    </message>
    <message>
        <location line="+2"/>
        <source>not accepted</source>
        <translation>未被接受</translation>
    </message>
    <message>
        <location line="+102"/>
        <location line="+31"/>
        <location line="+79"/>
        <source>Debit</source>
        <translation>支出</translation>
    </message>
    <message>
        <location line="-98"/>
        <source>Total debit</source>
        <translation>总收入</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Total credit</source>
        <translation>总支出</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Transaction fee</source>
        <translation>交易费</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Net amount</source>
        <translation>净额</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+12"/>
        <source>Message</source>
        <translation>消息</translation>
    </message>
    <message>
        <location line="-9"/>
        <source>Comment</source>
        <translation>备注</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Transaction Idem</source>
        <translation>交易同上</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction size</source>
        <translation>交易規模</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Merchant</source>
        <translation>商店</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Generated coins must mature %1 blocks before they can be spent. When you generated this block, it was broadcast to the network to be added to the block chain. If it fails to get into the chain, its state will change to &quot;not accepted&quot; and it won&apos;t be spendable. This may occasionally happen if another node generates a block within a few seconds of yours.</source>
        <translation>生成的比特币在可以使用前必须有 %1 个成熟的区块。当您生成了此区块后，它将被广播到网络中以加入区块链。如果它未成功进入区块链，其状态将变更为“不接受”并且不可使用。这可能偶尔会发生，如果另一个节点比你早几秒钟成功生成一个区块。</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Debug information</source>
        <translation>调试信息</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Transaction</source>
        <translation>交易</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Inputs</source>
        <translation>输入</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Amount</source>
        <translation>金额</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+3"/>
        <source>true</source>
        <translation>正确</translation>
    </message>
    <message>
        <location line="-3"/>
        <location line="+3"/>
        <source>false</source>
        <translation>错误</translation>
    </message>
    <message>
        <location line="-357"/>
        <source>, has not been successfully broadcast yet</source>
        <translation>，未被成功广播</translation>
    </message>
    <message numerus="yes">
        <location line="-40"/>
        <source>Open for %n more block(s)</source>
        <translation>
            <numerusform>再打开 %n 个数据块</numerusform>
        </translation>
    </message>
    <message>
        <location line="+73"/>
        <source>unknown</source>
        <translation>未知</translation>
    </message>
</context>
<context>
    <name>TransactionDescDialog</name>
    <message>
        <location filename="../forms/transactiondescdialog.ui" line="+14"/>
        <source>Transaction details</source>
        <translation>交易细节</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>This pane shows a detailed description of the transaction</source>
        <translation>当前面板显示了交易的详细信息</translation>
    </message>
</context>
<context>
    <name>TransactionGraphWidget</name>
    <message>
        <location filename="../transactiongraphwidget.cpp" line="+148"/>
        <source>tps</source>
        <translation>每秒事務數</translation>
    </message>
</context>
<context>
    <name>TransactionTableModel</name>
    <message>
        <location filename="../transactiontablemodel.cpp" line="+239"/>
        <source>Date</source>
        <translation>日期</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Type</source>
        <translation>类别</translation>
    </message>
    <message>
        <location line="+89"/>
        <source>Immature (%1 confirmations, will be available after %2)</source>
        <translation>未成熟 (%1 个确认，将在 %2 个后可用)</translation>
    </message>
    <message numerus="yes">
        <location line="-29"/>
        <source>Open for %n more block(s)</source>
        <translation>
            <numerusform>再打开 %n 个数据块</numerusform>
        </translation>
    </message>
    <message>
        <location line="-60"/>
        <source>Address or Label</source>
        <translation>地址或標籤</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Open until %1</source>
        <translation>至 %1 个数据块时开启</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Confirmed (%1 confirmations)</source>
        <translation>已确认 (%1 条确认信息)</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>This block was not received by any other nodes and will probably not be accepted!</source>
        <translation>此数据块未被任何其他节点接收，可能不被接受！</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Generated but not accepted</source>
        <translation>已生成但未被接受</translation>
    </message>
    <message>
        <location line="-31"/>
        <source>Offline</source>
        <translation>掉线</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unconfirmed</source>
        <translation>未确认的</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirming (%1 of %2 recommended confirmations)</source>
        <translation>确认中 (推荐 %2个确认，已经有 %1个确认)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Conflicted</source>
        <translation>冲突的</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Double Spent</source>
        <translation>雙花</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Abandoned</source>
        <translation>棄</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Received with</source>
        <translation>接收于</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Received from</source>
        <translation>收款来自</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Sent to</source>
        <translation>发送给</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Payment to yourself</source>
        <translation>付款给自己</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Mined</source>
        <translation>挖矿所得</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Public label</source>
        <translation>公共標籤</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Other</source>
        <translation>其他</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>watch-only</source>
        <translation>只看</translation>
    </message>
    <message>
        <location line="+276"/>
        <source>Transaction status. Hover over this field to show number of confirmations.</source>
        <translation>交易状态。 鼠标移到此区域可显示确认项数量。</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Date and time that the transaction was received.</source>
        <translation>接收到交易的时间</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Type of transaction.</source>
        <translation>交易类别。</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Whether or not a watch-only address is involved in this transaction.</source>
        <translation>该交易中是否涉及  观察地址(watch-only address)。</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>User-defined intent/purpose of the transaction.</source>
        <translation>用户定义的该交易的意图/目的。</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Amount removed from or added to balance.</source>
        <translation>从余额添加或移除的金额。</translation>
    </message>
</context>
<context>
    <name>TransactionView</name>
    <message>
        <location filename="../transactionview.cpp" line="+77"/>
        <location line="+19"/>
        <source>All</source>
        <translation>全部</translation>
    </message>
    <message>
        <location line="-18"/>
        <source>Today</source>
        <translation>今天</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This week</source>
        <translation>本周</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This month</source>
        <translation>本月</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Last month</source>
        <translation>上月</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This year</source>
        <translation>今年</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Range...</source>
        <translation>范围...</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Received with</source>
        <translation>接收于</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sent to</source>
        <translation>发送给</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>To yourself</source>
        <translation>到自己</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mined</source>
        <translation>挖矿所得</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Other</source>
        <translation>其他</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Enter address or label to search</source>
        <translation>输入地址或标签进行搜索</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Min amount</source>
        <translation>最小金额</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Copy address</source>
        <translation>复制地址</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy label</source>
        <translation>复制标签</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation>复制金额</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Copy raw transaction</source>
        <translation>复制原始交易</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Edit label</source>
        <translation>编辑标签</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show transaction details</source>
        <translation>显示交易详情</translation>
    </message>
    <message>
        <location line="+172"/>
        <source>Export Transaction History</source>
        <translation>导出交易历史</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Watch-only</source>
        <translation>只看</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Exporting Failed</source>
        <translation>导出失败</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to save the transaction history to %1.</source>
        <translation>导出交易历史到 %1 时发生错误。</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Exporting Successful</source>
        <translation>导出成功</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The transaction history was successfully saved to %1.</source>
        <translation>交易历史已成功保存到 %1。</translation>
    </message>
    <message>
        <location line="-29"/>
        <source>Comma separated file (*.csv)</source>
        <translation>逗号分隔文件 (*.csv)</translation>
    </message>
    <message>
        <location line="-175"/>
        <source>Copy transaction idem</source>
        <translation>複製交易同上</translation>
    </message>
    <message>
        <location line="+184"/>
        <source>Confirmed</source>
        <translation>已确认</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Date</source>
        <translation>日期</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Type</source>
        <translation>类别</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Label</source>
        <translation>标签</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Address</source>
        <translation>地址</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location line="+108"/>
        <source>Range:</source>
        <translation>范围：</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>to</source>
        <translation>到</translation>
    </message>
</context>
<context>
    <name>UnitDisplayStatusBarControl</name>
    <message>
        <location filename="../nexagui.cpp" line="+117"/>
        <source>Unit to show amounts in. Click to select another unit.</source>
        <translation>金额单位。单击选择别的单位。</translation>
    </message>
</context>
<context>
    <name>UnlimitedDialog</name>
    <message>
        <location filename="../forms/unlimited.ui" line="+14"/>
        <source>Unlimited</source>
        <translation></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Mining</source>
        <translation>＆礦業</translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+10"/>
        <source>The largest block that will be mined</source>
        <translation>將被開采的最大區塊</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Maximum Generated Block Size (bytes) </source>
        <translation>最大生成塊大小（字節） </translation>
    </message>
    <message>
        <location line="+30"/>
        <source>&amp;Network</source>
        <translation>网络(&amp;N)</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Bandwidth Restrictions in KBytes/sec (check to enable):</source>
        <translation>以 KBytes/sec 為單位的帶寬限制（選中以啟用）：</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Send</source>
        <translation>發送</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+71"/>
        <source>Max</source>
        <translation>最大限度</translation>
    </message>
    <message>
        <location line="-35"/>
        <location line="+59"/>
        <source>Average</source>
        <translation>平均的</translation>
    </message>
    <message>
        <location line="-31"/>
        <source>Receive</source>
        <translation>收到</translation>
    </message>
    <message>
        <location line="+86"/>
        <source>Active command-line options that override above options:</source>
        <translation>有效的命令行参数覆盖上述选项:</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Reset all client options to default.</source>
        <translation>恢复客户端的缺省设置</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Reset Options</source>
        <translation>恢复缺省设置(&amp;R)</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>&amp;OK</source>
        <translation>确定(&amp;O)</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Cancel</source>
        <translation>取消(&amp;C)</translation>
    </message>
    <message>
        <location filename="../unlimiteddialog.cpp" line="-345"/>
        <source>Confirm options reset</source>
        <translation>确认恢复缺省设置</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This is a global reset of all settings!</source>
        <translation>這是所有設置的全局重置！</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Client restart required to activate changes.</source>
        <translation>更改生效需要重启客户端。</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Client will be shut down. Do you want to proceed?</source>
        <translation>客户端即将关闭，您想继续吗？</translation>
    </message>
    <message>
        <location line="+60"/>
        <location line="+39"/>
        <location line="+42"/>
        <location line="+42"/>
        <source>Upstream traffic shaping parameters can&apos;t be blank</source>
        <translation>上行流量整形參數不能為空</translation>
    </message>
    <message>
        <location line="-108"/>
        <location line="+36"/>
        <location line="+43"/>
        <location line="+41"/>
        <source>Traffic shaping parameters have to be greater than 0.</source>
        <translation>流量整形參數必須大於 0。</translation>
    </message>
</context>
<context>
    <name>WalletFrame</name>
    <message>
        <location filename="../walletframe.cpp" line="+26"/>
        <source>No wallet has been loaded.</source>
        <translation>没有载入钱包。</translation>
    </message>
</context>
<context>
    <name>WalletModel</name>
    <message>
        <location filename="../walletmodel.cpp" line="+287"/>
        <source>Send Coins</source>
        <translation>发送比特币</translation>
    </message>
</context>
<context>
    <name>WalletView</name>
    <message>
        <location filename="../walletview.cpp" line="+44"/>
        <source>&amp;Export</source>
        <translation>导出(&amp;E)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Export the data in the current tab to a file</source>
        <translation>导出当前数据到文件</translation>
    </message>
    <message>
        <location line="+187"/>
        <source>Backup Wallet</source>
        <translation>备份钱包</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Wallet Data (*.dat)</source>
        <translation>钱包文件(*.dat)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Backup Failed</source>
        <translation>备份失败</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to save the wallet data to %1.</source>
        <translation>尝试保存钱包数据至 %1 时发生错误。</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>The wallet data was successfully saved to %1.</source>
        <translation>钱包数据成功保存至 %1 。</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Backup Successful</source>
        <translation>备份成功</translation>
    </message>
</context>
<context>
    <name>nexa</name>
    <message>
        <location filename="../nexastrings.cpp" line="+56"/>
        <source>Prune configured below the minimum of %d MiB.  Please use a higher number.</source>
        <translation>修剪值被设置为低于最小值%d MiB，请使用更大的数值。</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Rescans are not possible in pruned mode. You will need to use -reindex which will download the whole blockchain again.</source>
        <translation>无法在开启修剪的状态下重扫描，请使用 -reindex重新下载完整的区块链。</translation>
    </message>
    <message>
        <location line="+90"/>
        <source>Error: A fatal internal error occurred, see debug.log for details</source>
        <translation>错误：发生了致命的内部错误，详情见 debug.log 文件</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Pruning blockstore...</source>
        <translation>正在修剪区块存储...</translation>
    </message>
    <message>
        <location line="-155"/>
        <source>Distributed under the MIT software license, see the accompanying file COPYING or &lt;http://www.opensource.org/licenses/mit-license.php&gt;.</source>
        <translation>Distributed under the MIT software license, see the accompanying file COPYING or &lt;http://www.opensource.org/licenses/mit-license.php&gt;.</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>The block database contains a block which appears to be from the future. This may be due to your computer&apos;s date and time being set incorrectly. Only rebuild the block database if you are sure that your computer&apos;s date and time are correct</source>
        <translation>区块数据库包含未来的交易，这可能是由本机错误的日期时间引起。若确认本机日期时间正确，请重新建立区块数据库。</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>This is a pre-release test build - use at your own risk - do not use for mining or merchant applications</source>
        <translation>这是测试用的预发布版本 - 请谨慎使用 - 不要用来挖矿，或者在正式商用环境下使用</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>WARNING: abnormally high number of blocks generated, %d blocks received in the last %d hours (%d expected)</source>
        <translation>警告：数据块生成数量异常，最近 %d 小时收到了 %d 个数据块（预期为 %d 个）</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>WARNING: check your network connection, %d blocks received in the last %d hours (%d expected)</source>
        <translation>警告：请检查您的网络连接，最近 %d 小时收到了 %d 个数据块（预期为 %d 个）</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Warning: The network does not appear to fully agree! Some miners appear to be experiencing issues.</source>
        <translation>警告：网络似乎并不完全同意！有些矿工似乎遇到了问题。</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Warning: We do not appear to fully agree with our peers! You may need to upgrade, or other nodes may need to upgrade.</source>
        <translation>警告：我们的同行似乎不完全同意！您可能需要升级，或者其他节点可能需要升级。</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Corrupted block database detected</source>
        <translation>检测发现数据块数据库损坏。请使用 -reindex参数重启客户端。</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Do you want to rebuild the block database now?</source>
        <translation>你想现在就重建块数据库吗？</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error initializing block database</source>
        <translation>初始化数据块数据库出错</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error initializing wallet database environment %s!</source>
        <translation>Error initializing wallet database environment %s!</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error opening block database</source>
        <translation>导入数据块数据库出错</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Error: Disk space is low!</source>
        <translation>错误：磁盘剩余空间低!</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Failed to listen on any port. Use -listen=0 if you want this.</source>
        <translation>监听端口失败。请使用 -listen=0 参数。</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Importing...</source>
        <translation>导入中...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Incorrect or no genesis block found. Wrong datadir for network?</source>
        <translation>不正确或没有找到起源区块。网络错误？</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Invalid -onion address: &apos;%s&apos;</source>
        <translation>无效的 -onion 地址：“%s”</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Not enough file descriptors available.</source>
        <translation>没有足够的文件描述符可用。</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Prune cannot be configured with a negative value.</source>
        <translation>修剪不能配置一个负数。</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Prune mode is incompatible with -txindex.</source>
        <translation>修剪模式与 -txindex 不兼容。</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Verifying blocks...</source>
        <translation>正在验证数据库的完整性...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Verifying wallet...</source>
        <translation>正在检测钱包的完整性...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Waiting for Genesis Block...</source>
        <translation>等待創世塊...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Wallet %s resides outside data directory %s</source>
        <translation>钱包 %s 在外部的数据目录 %s</translation>
    </message>
    <message>
        <location line="-166"/>
        <source>Error: Listening for incoming connections failed (listen returned error %s)</source>
        <translation>错误：监听外部连接失败 (监听返回错误 %s)</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>The transaction amount is too small to send after the fee has been deducted</source>
        <translation>在交易费被扣除后发送的交易金额太小</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>This product includes software developed by the OpenSSL Project for use in the OpenSSL Toolkit &lt;https://www.openssl.org/&gt; and cryptographic software written by Eric Young and UPnP software written by Thomas Bernard.</source>
        <translation>This product includes software developed by the OpenSSL Project for use in the OpenSSL Toolkit &lt;https://www.openssl.org/&gt; and cryptographic software written by Eric Young and UPnP software written by Thomas Bernard.</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>You need to rebuild the database using -reindex to go back to unpruned mode.  This will redownload the entire blockchain</source>
        <translation>您需要使用 -reindex 重新构建数据库以返回未修剪的模式。这将重新下载整个区块链</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Activating best chain...</source>
        <translation>正在激活最佳数据链...</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Cannot resolve -whitebind address: &apos;%s&apos;</source>
        <translation>无法解析 -whitebind 地址: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Information</source>
        <translation>信息</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Invalid netmask specified in -whitelist: &apos;%s&apos;</source>
        <translation>-whitelist: &apos;%s&apos; 指定的网络掩码无效</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Need to specify a port with -whitebind: &apos;%s&apos;</source>
        <translation>-whitebind: &apos;%s&apos; 需要指定一个端口</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Signing transaction failed</source>
        <translation>签署交易失败</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Starting txindex</source>
        <translation>起始 txindex</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The transaction amount is too small to pay the fee</source>
        <translation>交易金额太小，不足以支付交易费</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>This is experimental software.</source>
        <translation>这是实验性的软件。</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction amount too small</source>
        <translation>交易量太小</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction amounts must be positive</source>
        <translation>交易金额必须是积极的</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction has %d outputs. Maximum outputs allowed is %d</source>
        <translation>事務有 %d 個輸出。 允許的最大輸出是 %d</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction of %d bytes is too large. Maximum allowed is %d bytes</source>
        <translation>%d 字節的事務過大。 允許的最大值為 %d 字節</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction too large for fee policy</source>
        <translation>费用策略的交易太大</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unable to bind to %s on this computer (bind returned error %s)</source>
        <translation>无法在此计算机上绑定 %s (绑定返回错误 %s)</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location line="-39"/>
        <source>Loading addresses...</source>
        <translation>正在加载地址簿...</translation>
    </message>
    <message>
        <location line="-5"/>
        <source>Invalid -proxy address: &apos;%s&apos;</source>
        <translation>无效的代理地址：%s</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Unknown network specified in -onlynet: &apos;%s&apos;</source>
        <translation>-onlynet 指定的是未知网络：%s</translation>
    </message>
    <message>
        <location line="-61"/>
        <source>Cannot resolve -bind address: &apos;%s&apos;</source>
        <translation>无法解析 -bind 端口地址: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cannot resolve -externalip address: &apos;%s&apos;</source>
        <translation>无法解析 -externalip 地址: &apos;%s&apos;</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Loading block index...</source>
        <translation>正在加载数据块索引...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading wallet...</source>
        <translation>正在加载钱包...</translation>
    </message>
    <message>
        <location line="-36"/>
        <source>Cannot downgrade wallet</source>
        <translation>无法降级钱包</translation>
    </message>
    <message>
        <location line="-125"/>
        <source>Nexa</source>
        <translation>Nexa</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The %s developers</source>
        <translation>%s 開發人員</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bitcoin Bitcoin XT and Bitcoin Unlimited</source>
        <translation>比特幣 Bitcoin XT 和 Bitcoin Unlimited</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>-wallet.maxTxFee is set very high! Fees this large could be paid on a single transaction.</source>
        <translation>-wallet.maxTxFee 設置得非常高！ 這麼大的費用可以在單筆交易中支付。</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>-wallet.payTxFee is set very high! This is the transaction fee you will pay if you send a transaction.</source>
        <translation>-wallet.payTxFee 設置得非常高！ 這是您發送交易時將支付的交易費用。</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cannot obtain a lock on data directory %s. %s is probably already running.</source>
        <translation>無法獲得對數據目錄 %s 的鎖定。 %s 可能已經在運行。</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Could not locate RPC credentials. No authentication cookie could be found, and no rpcpassword is set in the configuration file (%s)</source>
        <translation>找不到 RPC 憑據。 找不到身份驗證 cookie，並且配置文件中沒有設置 rpcpassword (%s)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Deployment configuration file &apos;%s&apos; contained invalid data - see debug.log</source>
        <translation>部署配置文件“%s”包含無效數據 - 請參閱 debug.log</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error loading %s: You can&apos;t enable HD on a already existing non-HD wallet</source>
        <translation>加載 %s 時出錯：您無法在現有的非 HD 錢包上啟用 HD</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error reading %s! All keys read correctly, but transaction data or address book entries might be missing or incorrect.</source>
        <translation>讀取 %s 時出錯！ 所有密鑰均正確讀取，但交易數據或地址簿條目可能丟失或不正確。</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Error reading from the coin database.
Details: %s

Do you want to reindex on the next restart?</source>
        <translation>從硬幣數據庫讀取錯誤。
詳細信息：%s

您想在下次重新啟動時重建索引嗎？</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Failed to listen on all P2P ports. Failing as requested by -bindallorfail.</source>
        <translation>無法偵聽所有 P2P 端口。 按照 -bindallorfail 的要求失敗。</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Fee: %ld is larger than configured maximum allowed fee of : %ld.  To change, set &apos;wallet.maxTxFee&apos;.</source>
        <translation>費用：%ld 大於配置的最大允許費用：%ld。 要更改，請設置“wallet.maxTxFee”。</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Invalid amount for -wallet.maxTxFee=&lt;amount&gt;: &apos;%u&apos; (must be at least the minrelay fee of %s to prevent stuck transactions)</source>
        <translation>-wallet.maxTxFee=&lt;amount&gt; 的無效金額：&apos;%u&apos;（必須至少為 %s 的最小中繼費用以防止交易卡住）</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Invalid amount for -wallet.payTxFee=&lt;amount&gt;: &apos;%u&apos; (must be at least %s)</source>
        <translation>-wallet.payTxFee=&lt;amount&gt; 的無效金額：“%u”（必須至少為 %s）</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Please check that your computer&apos;s date and time are correct! If your clock is wrong, %s will not work properly.</source>
        <translation>請檢查您計算機的日期和時間是否正確！ 如果您的時鐘有誤，%s 將無法正常工作。</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Prune: last wallet synchronisation goes beyond pruned data. You need to -reindex (download the whole blockchain again in case of pruned node)</source>
        <translation>修剪：最後的錢包同步超出了修剪後的數據。 您需要 -reindex （在修剪節點的情況下再次下載整個區塊鏈）</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Reducing -maxconnections from %d to %d because of file descriptor limitations (unix) or winsocket fd_set limitations (windows). If you are a windows user there is a hard upper limit of 1024 which cannot be changed by adjusting the node&apos;s configuration.</source>
        <translation>由於文件描述符限制 (unix) 或 winsocket fd_set 限制 (windows)，將 -maxconnections 從 %d 減少到 %d。 如果您是 Windows 用戶，則硬性上限為 1024，無法通過調整節點配置來更改。</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Total length of network version string with uacomments added exceeded the maximum length (%i) and have been truncated.  Reduce the number or size of uacomments to avoid truncation.</source>
        <translation>添加了 uacomments 的網絡版本字符串的總長度超過了最大長度 (%i)，並且已被截斷。 減少 uacomments 的數量或大小以避免截斷。</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Transaction has %d inputs and %d outputs. Maximum inputs allowed are %d and maximum outputs are %d</source>
        <translation>交易有 %d 個輸入和 %d 個輸出。 允許的最大輸入為 %d，最大輸出為 %d</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Transaction has %d inputs. Maximum inputs allowed is %d. Try reducing inputs by transferring a smaller amount.</source>
        <translation>事務有 %d 個輸入。 允許的最大輸入是 %d。 嘗試通過轉移較小的金額來減少投入。</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Wallet is not password protected. Your funds may be at risk! Goto &quot;Settings&quot; and then select &quot;Encrypt Wallet&quot; to create a password.</source>
        <translation>錢包不受密碼保護。 您的資金可能有風險！ 轉到“設置”，然後選擇“加密錢包”以創建密碼。</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Warning: Could not open deployment configuration CSV file &apos;%s&apos; for reading</source>
        <translation>警告：無法打開部署配置 CSV 文件“%s”進行讀取</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Warning: Unknown block versions being mined! It&apos;s possible unknown rules are in effect</source>
        <translation>警告：正在挖掘未知的塊版本！ 可能有未知規則生效</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Warning: Wallet file corrupt, data salvaged! Original %s saved as %s in %s; if your balance or transactions are incorrect you should restore from a backup.</source>
        <translation>警告：錢包文件損壞，數據已搶救！ 原始 %s 在 %s 中另存為 %s； 如果您的餘額或交易不正確，您應該從備份中恢復。</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>You are trying to use -wallet.auto but neither -spendzeroconfchange nor -wallet.instant is turned on</source>
        <translation>您正在嘗試使用 -wallet.auto 但 -spendzeroconfchange 和 -wallet.instant 都沒有打開</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>You are trying to use wallet.fallbackFee which has been deprecated an no longer in  use - use wallet.payTxFee instead </source>
        <translation>您正在嘗試使用已棄用且不再使用的 wallet.fallbackFee - 請改用 wallet.payTxFee </translation>
    </message>
    <message>
        <location line="+3"/>
        <source>You are trying to use wallet.minTxFee which has been deprecated an no longer in  use - use wallet.payTxFee instead </source>
        <translation>您正在嘗試使用已棄用且不再使用的 wallet.minTxFee - 請改用 wallet.payTxFee </translation>
    </message>
    <message>
        <location line="+3"/>
        <source>You can not send free transactions if you have configured a -relay.limitFreeRelay of zero</source>
        <translation>如果您將 -relay.limitFreeRelay 配置為零，則無法發送免費交易</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>%s </source>
        <translation>%s </translation>
    </message>
    <message>
        <location line="+1"/>
        <source>%s corrupt, salvage failed</source>
        <translation>%s 損壞，挽救失敗</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>-maxtxpool must be at least %d MB</source>
        <translation>-cache.maxTxPool 必須至少為 %d MB</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>-xthinbloomfiltersize must be at least %d Bytes</source>
        <translation>-xthinbloomfiltersize 必須至少為 %d 字節</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Cannot write default address</source>
        <translation>无法写入默认地址</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>CommitTransaction failed.</source>
        <translation>提交交易失敗。</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copyright (C) 2015-%i The Bitcoin Unlimited Developers</source>
        <translation>版權所有 (C) 2015-%i The Bitcoin Unlimited Developers</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Deployment configuration file &apos;%s&apos; not found</source>
        <translation>未找到部署配置文件“%s”</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error loading %s</source>
        <translation>加載 %s 時出錯</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error loading %s: Wallet corrupted</source>
        <translation>加載 %s 時出錯：錢包已損壞</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error loading %s: Wallet requires newer version of %s</source>
        <translation>加載 %s 時出錯：電子錢包需要較新版本的 %s</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error loading %s: You can&apos;t disable HD on a already existing HD wallet</source>
        <translation>加載 %s 時出錯：您無法在現有的 HD 錢包上禁用 HD</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Initialization sanity check failed. %s is shutting down.</source>
        <translation>初始化完整性檢查失敗。 %s 正在關閉。</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Insufficient funds or funds not confirmed</source>
        <translation>資金不足或資金未確認</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Keypool ran out, please call keypoolrefill first</source>
        <translation>keypool用完了，請先調用keypoolrefill</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading Orphanpool</source>
        <translation>加載孤兒池</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading TxPool</source>
        <translation>加載交易池</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Loading banlist...</source>
        <translation>載入黑名單...</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Opening Block database...</source>
        <translation>正在打開塊數據庫...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Opening Coins Cache database...</source>
        <translation>正在打開硬幣緩存數據庫...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Opening UTXO database...</source>
        <translation>正在打開 UTXO 數據庫...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Portions Copyright (C) 2009-%i The Bitcoin Core Developers</source>
        <translation>部分版權所有 (C) 2009-%i The Bitcoin Core Developers</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Portions Copyright (C) 2014-%i The Bitcoin XT Developers</source>
        <translation>部分版權 (C) 2014-%i The Bitcoin XT Developers</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Reaccepting Wallet Transactions</source>
        <translation>重新接受錢包交易</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Rescanning...</source>
        <translation>正在重新扫描...</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Turn off auto consolidate and try sending again.</source>
        <translation>關閉自動合併並嘗試再次發送。</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unable to bind to %s on this computer. %s is probably already running.</source>
        <translation>無法綁定到此計算機上的 %s。 %s 可能已經在運行。</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unable to start RPC services. See debug log for details.</source>
        <translation>無法啟動 RPC 服務。 有關詳細信息，請參閱調試日誌。</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Upgrading block database...This could take a while.</source>
        <translation>升級塊數據庫...這可能需要一段時間。</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Upgrading txindex database </source>
        <translation>升級 txindex 數據庫 </translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Upgrading txindex database...</source>
        <translation>正在升級 txindex 數據庫...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>User Agent comment (%s) contains unsafe characters.</source>
        <translation>用戶代理註釋 (%s) 包含不安全字符。</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Wallet needed to be rewritten: restart %s to complete</source>
        <translation>需要重寫錢包：重新啟動 %s 以完成</translation>
    </message>
    <message>
        <location line="-61"/>
        <source>Done loading</source>
        <translation>加载完成</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
</context>
</TS>
