Release Notes for Nexa 1.3.0.0
======================================================

Nexa version 1.3.0.0 is now available from:

  <https://gitlab.com/nexa/nexa/-/releases>

Please report bugs using the issue tracker at github:

  <https://gitlab.com/nexa/nexa/-/issues>

This is minor release of Nexa, for more information about Nexa see:

- https://nexa.org
- https://spec.nexa.org

Main changes in 1.3.0.0
-----------------------

This is list of the main changes that have been merged in this release:

- fine tune criteria to determine non standard transaction
- UTXO auto consolidation (Qt) ([details](#utxo-auto-consolidation))
- new `consolidate` RPC command to reduce own wallet utxo size ([details](#new-consolidate-rpc-command))
- new and updated translations

###### UTXO auto consolidation:

This feature automates the consolidation of small inputs, such as that produced by mining rewards, by generating transactions that combine inputs into larger quantities, and then including those quantities in a final transaction that sends to the user's chosen destination.  This feature is on by default for QT users, but can be turned off.  Also, it is never activated if coin control is selected.

###### New `consolidate` RPC command:

It is recommended that exchanges and other heavy non-QT users periodically call this command to clean up large amounts of small UTXOs caused by customers mining directly to the service.

Commit details
--------------

- `7b336949a` cast nHeight to int32_t to match the VarIntMode expected type/value (Griffith)
- `b25ec9a30` make unspendable template args hash sizes nonstandard (Andrew Stone)
- `19aa0c02a` Handle exceptions when blockindex or txindex fail to open (Peter Tschipper)
- `23a15a263` Create a mapping for missing native langauge names (Peter Tschipper)
- `3f91309bf` Fix script template argsHash validation (vgrunner)
- `1ff1d2cc7` Update translation files for the Phillipines (Peter Tschipper)
- `4c8ef1c29` make P2SH outputs nonstandard, reorder a file in cashlib to resolve undefined symbol (Andrew Stone)
- `13070e3db` Update portuguese translation files (Peter TSchipper)
- `35bbba1d7` Add auto consolidate feature and turn it on by default for QT users (Peter Tschipper)
- `c7cb659e3` Update REST interface documentation (Andrea Suisani)
- `837b749f8` this logic reopens the log file if its been deleted, so the proper check is if the log name exists (not empty). (Andrew Stone)
- `404e03e9e` remove unneeded param in genesis & add decimal op_return field (Andrew Stone)
- `c06587d20` Update all ts files with current translatable strings (Peter Tschipper)
- `e79897d11` Fix bug in disable sendfreetransactions for QT users only (Peter Tschipper)
- `f2359149c` Use nexa URL as string to test TestSplitHost() (Andrea Suisani)
- `f8b39bf09` Use nexad default port in unit tests (Andrea Suisani)
- `3d5bce684` Use default nexad port in rpc nexa-cli help text (Andrea Suisani)
- `23ae4c1b9` On startup check if the nexa.conf file exists and if not then create one (Peter TSchipper)
- `7a7872750` Use default nexa main net rpc port in curl help text (Andrea Suisani)
- `0bb088138` Don't return any watch only coins from AvailableCoins() - fixes #30 and #32 (Peter Tschipper)
- `f0713623f` Add more CheckBlock() tests (Peter Tschipper)
- `56cd33fcd` Simplify the m4 macro to search for Berkeley db header files (Andrea Suisani)
- `16e68737a` Fix issue #2 (Andrea Suisani)
- `e3b4a3fd1` Fix txindex.py (Peter Tschipper)
- `f59c4d37e` Add logging to keep track of median block size and next max block size candidate (Andrea Suisani)
- `df2021078` Set a reindex on restart when a coinscatcher exception is thrown (Peter Tschipper)
- `8de2e2790` Fix getmininginfo help text (Andrea Suisani)
- `eb2c13e35` Show a "removed" icon in the transaction view if abandoned. (Peter TSchipper)
- `f67ee2602` Fix lock order issue in rpc consolidate() (Peter Tschipper)
- `b2416c34f` wrap some uses of wallet-specific stuff in ifdefs (Andrew Stone)
- `bf5a57feb` Add a consolidate rpc which will consolidate coins in the wallet (Peter Tschipper)
- `99bbc0561` Add QT checkbox to main options in order to initiate a reindex on next startup (Peter TSchipper)

Credits
-------

Thanks to everyone who directly contributed to this release:

- Andrea Suisani
- Andrew Stone
- Griffith
- Peter Tschipper
- vgrunner
