Release Notes for Nexa 1.2.0
======================================================

Nexa version 1.2.0 is now available from:

  <https://gitlab.com/nexa/nexa/-/releases>

Please report bugs using the issue tracker at github:

  <https://gitlab.com/nexa/nexa/-/issues>

This is minor release of Nexa, for more information about Nexa see:

- https://nexa.org
- https://spec.nexa.org

Upgrading
---------

Main changes in 1.2.0
-----------------------

This is list of the main changes that have been merged in this release:

- add instant transactions in Qt wallet
- new rpc command to dump utxo data on a csv file
- fix libnexa.dylib for osx
- fix rare bug happening during IBD
- various improvements and fixes to libnexakotlin
- add support for DSproof to p2pkt
- enable UPnP by default in Qt client
- enable getrawtransaction to retrieve transaction by outpoint (if txindex=1)

Commit details
--------------

- `3bbadb230` Fix QT wallet balance bug for instant transactions (Peter TSchipper)
- `81c3e854b` [rpc] add dumputxoset command (Andrea Suisani)
- `713440521` Instant Transactions (Peter Tschipper)
- `a78225a98` Fix macOS M1 build & libnexa (Andrey Moiseev)
- `dc4ba7c84` Warn mainnet users if their wallets are unnecrypted (Peter Tschipper)
- `08b065f70` Add a few timing points and log printts to txindex.py and grouptokens.py (Peter TSchipper)
- `3662c5611` CI: Upload macOS build artifacts (Andrey Moiseev)
- `5a604b2c3` Update formatting tools to use clang-format ver 15 (Andrea Suisani)
- `b9aa8ba1a` Fix for rare sync bug (Peter Tschipper)
- `70e535456` Cleanup and simplify CommitTransaction() (Peter Tschipper)
- `06940b08c` expand txindex state parameter in getinfo rpc to return 'trailing' if the txindex is not caught up to the tip height specified in getinfo (Andrew Stone)
- `93e530fc8` port libnexakotlin header verification native function to cashlib temporarily during the parallel existence (Andrew Stone)
- `c93236bf7` Fix gitian bianries compilation (Andrea Suisani)
- `402d7cb58` When resetting maxConnections lower use a graceful disconnect (Peter Tschipper)
- `5c1742e34` allow p2pkt in dsproofs (Peter Tschipper)
- `3c548a2ae` Only allow 16 inbound connections when doing IBD (Peter Tschipper)
- `3defc4148` Fix an assorted collection of warnings spotted by clang (Andrea Suisani)
- `eea6dd70a` Hide maxReorgDepth which is in the list of command line options (Peter Tschipper)
- `6967ed9a2` The next time QT launces set UPnP to be "ON" (Peter Tschipper)
- `603cb9ccb` Fix spurious failures in mining_adaptive_blocksize.py (Peter TSchipper)
- `9cee5c812` Prevent the msg threads from spinning when network traffic is light (Peter Tschipper)
- `159f4664b` Update the README_windows.txt which goes with each release (Peter TSchipper)
- `838506d3f` Increase the default transaction fees for the wallet (Peter Tschipper)
- `fe9b33f53` Only allow a max of 256 inputs to be selected from coincontrol (Peter Tschipper)
- `524f0e2e0` Add rostrum and src/rostrum to .gitignore (Peter TSchipper)
- `576a3c450` Add text to getrawtransaction help text which shows we can search by outpoint (Peter TSchipper)
- `70135eaf6` enable getrawtransaction to access transactions by created outpoints (Andrew Stone)
- `a964510d4` Update gettxout usage text to reflect that it works with txidem not txid (Andrea Suisani)
- `3e6cd0b8b` Remove fOnlyConfirmed in the wallet (Peter Tschipper)
- `46821fb7b` Instead of returning Error , return ACTION REQUIRED (Peter TSchipper)
- `d85302b41` Only resend the wallet transaction once. (Peter TSchipper)
- `d4e1244fd` Add extra text to error message when creating a new transaction (Peter TSchipper)
- `eda973e07` Remove 2 references to P2WPKH (Andrea Suisani)
- `de132d647` Set the fee coin amount needed correctly when looking for a fee coin (Peter TSchipper)
- `efb75a99e` add header count to getinfo (Griffith)
- `5f93fa411` Further docker related enhancements (Andrea Suisani)
- `80f7e2538` move the already-used check into the filtercoins selector (Andrew Stone)
- `86f496f8f` When adding extra fees for a token make sure to check for duplicates (ptschip)
- `4fd548797` Remove the MAX_FEELER_CONNECTIONS variable (ptschip)
- `7c1e41890` Add falcon to Makefile.am (ptschip)
- `214fbe9fa` Add falcon512 files and folder (Peter Tschipper)
- `4703d8e91` Change GetArg(-dbcache,0) to dbcacheTweak.Value() (Peter Tschipper)
- `38bc0f4a5` Fix rare mempool_tests.cpp one off error (Peter Tschipper)
- `91171f14f` If net.maxConnections is reduced then drop connections right away (Peter Tschipper)
- `abcc88df7` Add lock on the corral when we return the corral region (ptschip)
- `9fc5ee3ef` ci: Run the rostrum tests that match release (Dagur Valberg Johannsson)
- `32f9051ba` Fix output of "token info" showing ticker and name reversed (Peter Tschipper)
- `3b4d8c54a` make the QT wallet pickup the wallet.payTxFee tweak (Peter Tschipper)
- `a569a4d5e` Fix some remaining txindex.py timing issues (ptschip)
- `f53edd003` Update rostrum to v8.0.1 tag (Andrea Suisani)
- `141215f92` Fix electrum.{port,ws.port} default values in the helper text (Andrea Suisani)
- `34888422a` Fix a comment in GetASERTAnchorBlock() (Andrea Suisani)
- `02b9345ac` Fix documentation to build gitian binaries (Andrea Suisani)

Credits
=======

Thanks to everyone who directly contributed to this release:

- Andrea Suisani
- Andrew Stone
- Andrey Moiseev
- Dagur Valberg Johannsson
- Griffith
- Peter Tschipper

